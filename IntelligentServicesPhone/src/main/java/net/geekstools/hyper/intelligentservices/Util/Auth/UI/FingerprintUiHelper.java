/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Auth.UI;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.widget.ImageView;
import android.widget.TextView;

import net.geekstools.hyper.intelligentservices.R;

public class FingerprintUiHelper extends FingerprintManager.AuthenticationCallback {

    private static final long ERROR_TIMEOUT_MILLIS = 1600;
    private static final long SUCCESS_DELAY_MILLIS = 1300;

    Context context;

    private final FingerprintManager fingerprintManager;

    private final ImageView imageView;
    private final TextView errortextview;

    private final Callback callback;

    int initHintTextColor = 0;

    private CancellationSignal cancellationSignal;
    private boolean selfCancelled;
    private Runnable resetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            errortextview.setTextColor(initHintTextColor);
            errortextview.setText(errortextview.getResources().getString(R.string.fingerprint_hint));
            imageView.setImageResource(R.drawable.draw_finger_print);
        }
    };

    /**
     * Constructor for {@link FingerprintUiHelper}.
     */
    public FingerprintUiHelper(Context context, FingerprintManager fingerprintManager, ImageView icon, TextView errorTextView, Callback callback) {
        this.context = context;

        this.fingerprintManager = fingerprintManager;

        imageView = icon;
        errortextview = errorTextView;

        this.callback = callback;

        initHintTextColor = errorTextView.getCurrentTextColor();
    }

    public boolean isFingerprintAuthAvailable() {
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        return fingerprintManager.isHardwareDetected()
                && fingerprintManager.hasEnrolledFingerprints();
    }

    public void startListening(FingerprintManager.CryptoObject cryptoObject) {
        if (!isFingerprintAuthAvailable()) {
            return;
        }
        cancellationSignal = new CancellationSignal();
        selfCancelled = false;
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0 /* flags */, this, null);

        imageView.setImageResource(R.drawable.draw_finger_print);
    }

    public void stopListening() {
        if (cancellationSignal != null) {
            selfCancelled = true;
            cancellationSignal.cancel();
            cancellationSignal = null;
        }
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        if (!selfCancelled) {
            showError(errString);
            imageView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    callback.onError();
                }
            }, ERROR_TIMEOUT_MILLIS);
        }
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        showError(helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        showError(imageView.getResources().getString(R.string.fingerprint_not_recognized));
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        errortextview.removeCallbacks(resetErrorTextRunnable);
        imageView.setImageResource(R.drawable.draw_finger_print_success);
        errortextview.setTextColor(errortextview.getResources().getColor(R.color.success_color, null));
        errortextview.setText(errortextview.getResources().getString(R.string.fingerprint_success));
        imageView.postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onAuthenticated();
            }
        }, SUCCESS_DELAY_MILLIS);
    }

    private void showError(CharSequence error) {
        imageView.setImageResource(R.drawable.draw_finger_print_error);
        errortextview.setText(error);
        errortextview.setTextColor(errortextview.getResources().getColor(R.color.warning_color, null));
        errortextview.removeCallbacks(resetErrorTextRunnable);
        errortextview.postDelayed(resetErrorTextRunnable, ERROR_TIMEOUT_MILLIS);
    }

    public interface Callback {
        void onAuthenticated();

        void onError();
    }
}
