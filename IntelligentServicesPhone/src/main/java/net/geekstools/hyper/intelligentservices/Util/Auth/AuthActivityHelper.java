/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Auth;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.security.keystore.KeyProperties;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

public class AuthActivityHelper extends Activity {

    static final String DEFAULT_KEY_NAME = "default_key";
    private static final String KEY_NAME_NOT_INVALIDATED = "key_not_invalidated";
    FunctionsClass functionsClass;
    RelativeLayout authBackground;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.auth_transparent);
        functionsClass = new FunctionsClass(getApplicationContext(), AuthActivityHelper.this);

        authBackground = (RelativeLayout) findViewById(R.id.authBackground);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        try {
            window.setStatusBarColor(
                    functionsClass.setColorAlpha(functionsClass.extractVibrantColor(functionsClass.appIcon(getPackageManager().getActivityInfo(new ComponentName(FunctionsClass.AuthOpenAppValues.authPackageName, FunctionsClass.AuthOpenAppValues.authClassName), 0))),
                            0.50f)
            );
            window.setNavigationBarColor(
                    functionsClass.setColorAlpha(functionsClass.extractVibrantColor(functionsClass.appIcon(getPackageManager().getActivityInfo(new ComponentName(FunctionsClass.AuthOpenAppValues.authPackageName, FunctionsClass.AuthOpenAppValues.authClassName), 0))),
                            0.50f)
            );

            authBackground.setBackgroundColor(
                    functionsClass.setColorAlpha(functionsClass.extractVibrantColor(functionsClass.appIcon(getPackageManager().getActivityInfo(new ComponentName(FunctionsClass.AuthOpenAppValues.authPackageName, FunctionsClass.AuthOpenAppValues.authClassName), 0))),
                            0.50f)
            );
        } catch (Exception e) {
            e.printStackTrace();
            authBackground.setBackgroundColor(
                    functionsClass.setColorAlpha(functionsClass.extractVibrantColor(functionsClass.appIcon(FunctionsClass.AuthOpenAppValues.authPackageName)),
                            0.50f)
            );
        }

        /*Finger-Print Authentication Invocation*/
        try {
            KeyguardManager keyguardManager =
                    (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager =
                    (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            if (fingerprintManager.isHardwareDetected()) {
                if (fingerprintManager.hasEnrolledFingerprints()) {
                    if (keyguardManager.isKeyguardSecure()) {

                        try {
                            FunctionsClass.AuthOpenAppValues.keyStore = KeyStore.getInstance("AndroidKeyStore");
                        } catch (KeyStoreException e) {
                            throw new RuntimeException("Failed to get an instance of KeyStore", e);
                        }
                        try {
                            FunctionsClass.AuthOpenAppValues.keyGenerator = KeyGenerator
                                    .getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
                        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                            throw new RuntimeException("Failed to get an instance of KeyGenerator", e);
                        }
                        final Cipher defaultCipher;
                        Cipher cipherNotInvalidated;
                        try {
                            defaultCipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                                    + KeyProperties.BLOCK_MODE_CBC + "/"
                                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
                            cipherNotInvalidated = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                                    + KeyProperties.BLOCK_MODE_CBC + "/"
                                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
                        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                            throw new RuntimeException("Failed to get an instance of Cipher", e);
                        }

                        functionsClass.createKey(DEFAULT_KEY_NAME, true);
                        functionsClass.createKey(KEY_NAME_NOT_INVALIDATED, false);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                functionsClass.new InvokeAuth(defaultCipher, DEFAULT_KEY_NAME);
                            }
                        }, 333);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*Finger-Print Authentication Invocation*/
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
