/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.UI.ActionCenterUI;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;

import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.UI.SimpleGestureFilter;

public class ActionCenter extends Button
        implements SimpleGestureFilter.SimpleGestureListener {

    FunctionsClass functionsClass;
    Activity activity;
    Context context;

    SimpleGestureFilter detector;
    BroadcastReceiver visibilityReceiver;

    public ActionCenter(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = ((Activity) getContext());
        this.context = context;
        functionsClass = new FunctionsClass(context, (Activity) getContext());
        initConfirmButton();
    }

    public ActionCenter(Context context) {
        super(context);
    }

    public void initConfirmButton() {

        RippleDrawable drawPrefAction = (RippleDrawable) context.getResources().getDrawable(R.drawable.ripple_effect_action_center_no_bound);
        Drawable iconPref = drawPrefAction.findDrawableByLayerId(R.id.iconPref).mutate();
        GradientDrawable backPrefAction = (GradientDrawable) drawPrefAction.findDrawableByLayerId(R.id.backAction);
        drawPrefAction.setColor(ColorStateList.valueOf(PublicVariable.primaryColorOpposite));
        backPrefAction.setColor(PublicVariable.primaryColor);
        iconPref.setTint(functionsClass.mixColors(PublicVariable.colorLightDarkOpposite, PublicVariable.primaryColor, 0.77f));
        this.setBackground(drawPrefAction);

        detector = new SimpleGestureFilter(context, this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(context.getString(R.string.resetActionColor));
        visibilityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(context.getString(R.string.resetActionColor))) {
                    functionsClass.loadSavedColor();
                    RippleDrawable drawPrefAction = (RippleDrawable) context.getResources().getDrawable(R.drawable.ripple_effect_action_center_no_bound);
                    Drawable iconPref = drawPrefAction.findDrawableByLayerId(R.id.iconPref).mutate();
                    GradientDrawable backPrefAction = (GradientDrawable) drawPrefAction.findDrawableByLayerId(R.id.backAction);
                    drawPrefAction.setColor(ColorStateList.valueOf(PublicVariable.primaryColorOpposite));
                    backPrefAction.setColor(PublicVariable.primaryColor);
                    iconPref.setTint(functionsClass.mixColors(PublicVariable.colorLightDarkOpposite, PublicVariable.primaryColor, 0.77f));
                    ActionCenter.this.setBackground(drawPrefAction);
                }
            }
        };
        context.registerReceiver(visibilityReceiver, intentFilter);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getContext().unregisterReceiver(visibilityReceiver);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        this.detector.onTouchEvent(me);

        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {
            case SimpleGestureFilter.SWIPE_DOWN:
                System.out.println("SWIPE_DOWN");
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                System.out.println("SWIPE_LEFT");
                break;
            case SimpleGestureFilter.SWIPE_RIGHT:
                System.out.println("SWIPE_RIGHT");
                break;
            case SimpleGestureFilter.SWIPE_UP:
                System.out.println("SWIPE_UP");
                break;
        }
    }

    @Override
    public void onSingleTapUp() {
        functionsClass.Preferences(ActionCenter.this);
    }

    @Override
    public void onLongPress() {
        try {
            functionsClass.openPrimaryApplications(
                    ((int) PublicVariable.primaryAppObjects.get(0)),
                    String.valueOf((PublicVariable.primaryAppObjects.get(1))),
                    String.valueOf((PublicVariable.primaryAppObjects.get(2))),
                    ((int) PublicVariable.primaryAppObjects.get(3)),
                    ((int) PublicVariable.primaryAppObjects.get(4)),
                    ((int) PublicVariable.primaryAppObjects.get(5))
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
