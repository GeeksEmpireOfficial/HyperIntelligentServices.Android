/*
 * Copyright © 2020 By Geeks Empire.
 *
 * Created by Elias Fazel on 4/14/20 9:44 PM
 * Last modified 4/14/20 9:44 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.LicenseValidator;
import net.geekstools.hyper.intelligentservices.Util.NavAdapter.CardHybridAdapter;
import net.geekstools.hyper.intelligentservices.Util.NavAdapter.NavDrawerItem;
import net.geekstools.hyper.intelligentservices.Util.NavAdapter.RecycleViewSmoothLayout;
import net.geekstools.hyper.intelligentservices.Util.NavAdapter.SectionedGridRecyclerViewAdapter;
import net.geekstools.hyper.intelligentservices.Util.Notification.NotificationListener;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseAppInfo;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseIntelligentServices;
import net.geekstools.hyper.intelligentservices.Util.UI.SimpleGestureFilterSwitch;
import net.geekstools.imageview.customshapes.ShapesImage;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

public class HybridIntelligentStaticPrimary extends Activity implements View.OnClickListener, View.OnLongClickListener, View.OnTouchListener, SimpleGestureFilterSwitch.SimpleGestureListener {

    FunctionsClass functionsClass;

    RecyclerView loadView;
    ScrollView nestedScrollView;
    HorizontalScrollView secondaryScrollView;
    RelativeLayout listWhole, primaryPick;
    LinearLayout indexView, secondaryView;

    RelativeLayout primaryView;
    ShapesImage primaryPickApp, primaryNotificationDot;

    List<String> appData, appNames;
    Map<String, Integer> mapIndex, mapPackageNamePosition;
    NavigableMap<String, Integer> indexItems;
    ArrayList<NavDrawerItem> navDrawerItems;

    List<String> indexList, primaryClasses;
    List<SectionedGridRecyclerViewAdapter.Section> sections;
    RecyclerView.Adapter recyclerViewAdapter;
    GridLayoutManager recyclerViewLayoutManager;

    SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices;
    SqLiteDataBaseAppInfo sqLiteDataBaseAppInfo;

    String PackageName, AppName = "Application", ClassName, Number;
    Drawable AppIcon;

    RelativeLayout[] secondaryItems;
    String[] notificationPackages;
    ShapesImage[] primaryPicks, secondaryNotificationDot;

    int limitedCountLine, yScroll, secondaryCount;

    SimpleGestureFilterSwitch simpleGestureFilterSwitch;

    BroadcastReceiver broadcastReceiverDataSet;

    FirebaseRemoteConfig firebaseRemoteConfig;
    ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    public Resources.Theme getTheme() {
        Resources.Theme theme = super.getTheme();
        functionsClass = new FunctionsClass(getApplicationContext(), this);

        SharedPreferences primeColor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!functionsClass.wallpaperStaticLive()) {
            theme.applyStyle(android.R.style.Theme_DeviceDefault_Wallpaper_NoTitleBar, true);
        } else if (primeColor.getString("LightDark", "3").equals("1")) {//Light
            theme.applyStyle(R.style.GeeksEmpire_AppCompat_Light, true);
        } else if (primeColor.getString("LightDark", "3").equals("2")) {//Dark
            theme.applyStyle(R.style.GeeksEmpire_AppCompat_Dark, true);
        } else if (primeColor.getString("LightDark", "3").equals("3")) {
            if (functionsClass.colorLightDarkWallpaper()) {//light
                theme.applyStyle(R.style.GeeksEmpire_AppCompat_Light, true);
            } else if (!functionsClass.colorLightDarkWallpaper()) {//dark
                theme.applyStyle(R.style.GeeksEmpire_AppCompat_Dark, true);
            }
        } else if (primeColor.getString("LightDark", "3").equals("4")) {
            int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (timeHours >= 18 || timeHours < 6) {//Night
                theme.applyStyle(R.style.GeeksEmpire_AppCompat_Dark, true);
            } else {//Day
                theme.applyStyle(R.style.GeeksEmpire_AppCompat_Light, true);
            }
        }
        return theme;
    }

    @Override
    protected void onCreate(Bundle Saved) {
        super.onCreate(Saved);
        functionsClass = new FunctionsClass(getApplicationContext(), this);
        if (functionsClass.RightLeft()) {
            setContentView(R.layout.hybrid_intelligent_static_right);
            loadView = (RecyclerView) findViewById(R.id.list);
        } else if (!functionsClass.RightLeft()) {
            setContentView(R.layout.hybrid_intelligent_static_left);
            loadView = (RecyclerView) findViewById(R.id.list);
            loadView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        PublicVariable.HybridIntelligent = this;
        PublicVariable.contextStatic = getApplicationContext();
        if (functionsClass.returnAPI() >= 25) {
            functionsClass.setAppShortcutsScheduler();
        }
        TypedValue typedValue = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)) {
            PublicVariable.actionBarHeight = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        }
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            PublicVariable.statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        indexView = (LinearLayout) findViewById(R.id.side_index);
        listWhole = (RelativeLayout) findViewById(R.id.listWhole);

        nestedScrollView = (ScrollView) findViewById(R.id.nestedScrollView);
        nestedScrollView.setSmoothScrollingEnabled(true);

        primaryView = (RelativeLayout) findViewById(R.id.primaryView);
        primaryPick = (RelativeLayout) findViewById(R.id.primaryPick);
        primaryPickApp = (ShapesImage) findViewById(R.id.primaryPickApp);
        primaryNotificationDot = (ShapesImage) findViewById(R.id.primaryNotificationDot);
        primaryPickApp.setShapeDrawable(functionsClass.shapesDrawables());
        primaryNotificationDot.setShapeDrawable(functionsClass.shapesDrawables());

        secondaryScrollView = (HorizontalScrollView) findViewById(R.id.secondaryScrollView);
        secondaryView = (LinearLayout) findViewById(R.id.secondaryView);

        primaryClasses = new ArrayList<String>();
        secondaryCount = (functionsClass.columnCount(105)) + 3;
        secondaryItems = new RelativeLayout[secondaryCount + 1];
        primaryPicks = new ShapesImage[secondaryCount + 1];
        secondaryNotificationDot = new ShapesImage[secondaryCount + 1];

        simpleGestureFilterSwitch = new SimpleGestureFilterSwitch(getApplicationContext(), this);

        String DATABASE_FILE_NAME = functionsClass.sqLiteIntelligentServiceFileName();
        PublicVariable.databaseOldFile = DATABASE_FILE_NAME;
        sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(getApplicationContext(), DATABASE_FILE_NAME, 1);
        sqLiteDataBaseAppInfo = new SqLiteDataBaseAppInfo(getApplicationContext(), 1);

        recyclerViewLayoutManager = new RecycleViewSmoothLayout(getApplicationContext(), functionsClass.columnCount(105), OrientationHelper.VERTICAL, false);
        loadView.setLayoutManager(recyclerViewLayoutManager);

        indexList = new ArrayList<String>();
        sections = new ArrayList<SectionedGridRecyclerViewAdapter.Section>();
        indexItems = new TreeMap<String, Integer>();

        functionsClass.setThemeColor(listWhole);

        navDrawerItems = new ArrayList<NavDrawerItem>();
        mapIndex = new LinkedHashMap<String, Integer>();
        mapPackageNamePosition = new LinkedHashMap<String, Integer>();

        IntelligentServices();

        /*try {
            Intent goHome = getIntent();
            if (goHome.hasExtra("goHome")){
                if (goHome.getBooleanExtra("goHome", false)) {
                    Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                    homeScreen.addCategory(Intent.CATEGORY_HOME);
                    homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(homeScreen);
                }
            }
        }
        catch (Exception e){e.printStackTrace();}
        finally { }*/

        IntentFilter intentFilterNewDataSet = new IntentFilter();
        intentFilterNewDataSet.addAction(getString(R.string.newNotificationData));
        intentFilterNewDataSet.addAction(getString(R.string.notificationDot));
        intentFilterNewDataSet.addAction(getString(R.string.notificationDotNo));
        intentFilterNewDataSet.addAction(getString(R.string.splitAppsSingle));
        broadcastReceiverDataSet = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(getString(R.string.newNotificationData))) {
                    LoadNotificationDots loadNotificationDots = new LoadNotificationDots();
                    loadNotificationDots.execute();
                } else if (intent.getAction().equals(context.getString(R.string.notificationDot))) {
                    try {
                        String notificationPackage = intent.getStringExtra("NotificationPackage");
                        if (functionsClass.databaseAvailable(notificationPackage)) {
                            Drawable dotDrawable = null;
                            switch (functionsClass.shapesImageId()) {
                                case 1:
                                    dotDrawable = context.getDrawable(R.drawable.dot_droplet_icon).mutate();
                                    break;
                                case 2:
                                    dotDrawable = context.getDrawable(R.drawable.dot_circle_icon).mutate();
                                    break;
                                case 3:
                                    dotDrawable = context.getDrawable(R.drawable.dot_square_icon).mutate();
                                    break;
                                case 4:
                                    dotDrawable = context.getDrawable(R.drawable.dot_squircle_icon).mutate();
                                    break;
                                case 0:
                                    dotDrawable = functionsClass.appIcon(notificationPackage).mutate();
                                    break;
                            }
                            if (functionsClass.LightDark()) {
                                dotDrawable.setTint(functionsClass.manipulateColor(functionsClass.extractVibrantColor(functionsClass.appIcon(notificationPackage)), 1.30f));
                            } else {
                                dotDrawable.setTint(functionsClass.manipulateColor(functionsClass.extractVibrantColor(functionsClass.appIcon(notificationPackage)), 0.50f));
                            }
                            try {
                                primaryNotificationDot.setVisibility(View.INVISIBLE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            for (int i = 0; i < secondaryNotificationDot.length; i++) {
                                try {
                                    secondaryNotificationDot[i].setVisibility(View.INVISIBLE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (notificationPackage.equals(functionsClass.getPackageName(primaryPicks[0].getTag()))) {
                                primaryNotificationDot.setImageDrawable(dotDrawable);
                                primaryNotificationDot.setVisibility(View.VISIBLE);
                            } else {
                                int notificationPosition = mapPackageNamePosition.get(notificationPackage);
                                secondaryNotificationDot[notificationPosition].setImageDrawable(dotDrawable);
                                secondaryNotificationDot[notificationPosition].setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (intent.getAction().equals(context.getString(R.string.notificationDotNo))) {
                    try {
                        String notificationPackage = intent.getStringExtra("NotificationPackage");
                        if (notificationPackage.equals(functionsClass.getPackageName(primaryPicks[0].getTag()))) {
                            primaryNotificationDot.setVisibility(View.INVISIBLE);
                        } else {
                            int notificationPosition = mapPackageNamePosition.get(notificationPackage);
                            secondaryNotificationDot[notificationPosition].setVisibility(View.INVISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (intent.getAction().equals(getString(R.string.splitAppsSingle))) {
                    try {
                        String packageName = intent.getStringExtra("PackageName");
                        String className = intent.getStringExtra("ClassName");

                        Intent splitSingle = new Intent();
                        splitSingle.setClassName(packageName, className);
                        splitSingle.setFlags(
                                Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT |
                                        Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        context.startActivity(splitSingle);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        registerReceiver(broadcastReceiverDataSet, intentFilterNewDataSet);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            nestedScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    yScroll = scrollY;
                }
            });
        }
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        functionsClass.loadSavedColor();
        PublicVariable.inFront = true;
        if (functionsClass.NotificationAccess() && !functionsClass.SettingServiceRunning(NotificationListener.class)) {
            startService(new Intent(getApplicationContext(), NotificationListener.class));
        }
        CheckInstalledApps checkInstalledApps = new CheckInstalledApps();
        checkInstalledApps.execute();

        if (PublicVariable.newIcon) {
            PublicVariable.newIcon = false;
            primaryClasses.clear();
            startActivity(new Intent(getApplicationContext(), Configurations.class), ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        } else if (!PublicVariable.databaseOldFile.equals(functionsClass.sqLiteIntelligentServiceFileName())) {
            //HybridIntelligentStaticPrimary.this.finish();
            if (!functionsClass.databaseAvailableIntelligentService()) {
                functionsClass.LoadSaveAppInfoIntelligentService(HybridIntelligentStaticPrimary.class);
            } else {
                PublicVariable.forceReload = true;
                startActivity(new Intent(getApplicationContext(), HybridIntelligentStaticPrimary.class),
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
            }
        } else if (PublicVariable.inMemory) {
            IntelligentServices();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    LoadIndex();
                }
            }, 113);
        }
        if (functionsClass.ActivityRecognitionEnabled()) {
            functionsClass.enableActivityRecognition(PublicVariable.HybridIntelligent);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(getString(R.string.license));
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(getString(R.string.license))) {
                    functionsClass.DialogueLicense(HybridIntelligentStaticPrimary.this);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            stopService(new Intent(getApplicationContext(), LicenseValidator.class));
                        }
                    }, 1000);
                }
            }
        };
        registerReceiver(broadcastReceiver, intentFilter);
        try {
            if (!getFileStreamPath(".License").exists() && functionsClass.networkConnection() == true) {
                if (!BuildConfig.DEBUG || !functionsClass.appVersionName(getPackageName()).contains("BETA")) {
                    startService(new Intent(getApplicationContext(), LicenseValidator.class));
                }
            } else {
                try {
                    unregisterReceiver(broadcastReceiver);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        primaryPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (functionsClass.isAppLocked(functionsClass.getPackageName(primaryPicks[0].getTag()))) {
                        FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices = sqLiteDataBaseIntelligentServices;

                        FunctionsClass.AuthOpenAppValues.authPackageName = functionsClass.getPackageName(primaryPicks[0].getTag());
                        FunctionsClass.AuthOpenAppValues.authClassName = primaryClasses.get(0);
                        FunctionsClass.AuthOpenAppValues.authOrderNumber = Integer.parseInt(functionsClass.getOrderNumber(primaryPicks[0].getTag()));
                        FunctionsClass.AuthOpenAppValues.authPositionX = (int) (primaryPickApp.getX());
                        FunctionsClass.AuthOpenAppValues.authPositionY = (int) (primaryPickApp.getHeight() - functionsClass.DpToPixel(121));
                        FunctionsClass.AuthOpenAppValues.authHW = primaryPickApp.getWidth();

                        FunctionsClass.AuthOpenAppValues.authUnlockIt = false;
                        FunctionsClass.AuthOpenAppValues.authPopupOption = false;

                        functionsClass.openAuthInvocation();
                    } else {
                        functionsClass.openPrimaryApplications(Integer.parseInt(functionsClass.getOrderNumber(primaryPicks[0].getTag())), primaryClasses.get(0), functionsClass.getPackageName(primaryPicks[0].getTag()),
                                (int) (primaryPickApp.getX()), (int) (primaryPickApp.getHeight() - functionsClass.DpToPixel(121)), primaryPickApp.getWidth());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        primaryPick.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                try {
                    String packageName = functionsClass.getPackageName(primaryPicks[0].getTag());
                    String className = functionsClass.getClassName(primaryPicks[0].getTag());
                    int orderNumber = Integer.parseInt(functionsClass.getClassName(primaryPicks[0].getTag()));

                    FunctionsClass.AuthOpenAppValues.authPackageName = packageName;
                    FunctionsClass.AuthOpenAppValues.authClassName = className;

                    if (functionsClass.isAppLocked(packageName)) {
                        FunctionsClass.AuthOpenAppValues.authPopupOption = true;
                        FunctionsClass.AuthOpenAppValues.authAnchorView = view;

                        functionsClass.openAuthInvocation();
                    } else {
                        functionsClass.popupOptions(getApplicationContext(), view, packageName, className, orderNumber);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        primaryPickApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (functionsClass.isAppLocked(functionsClass.getPackageName(primaryPicks[0].getTag()))) {
                        FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices = sqLiteDataBaseIntelligentServices;

                        FunctionsClass.AuthOpenAppValues.authPackageName = functionsClass.getPackageName(primaryPicks[0].getTag());
                        FunctionsClass.AuthOpenAppValues.authClassName = primaryClasses.get(0);
                        FunctionsClass.AuthOpenAppValues.authOrderNumber = Integer.parseInt(functionsClass.getOrderNumber(primaryPicks[0].getTag()));
                        FunctionsClass.AuthOpenAppValues.authPositionX = (int) (primaryPickApp.getX());
                        FunctionsClass.AuthOpenAppValues.authPositionY = (int) (primaryPickApp.getHeight() - functionsClass.DpToPixel(121));
                        FunctionsClass.AuthOpenAppValues.authHW = primaryPickApp.getWidth();

                        FunctionsClass.AuthOpenAppValues.authUnlockIt = false;
                        FunctionsClass.AuthOpenAppValues.authPopupOption = false;

                        functionsClass.openAuthInvocation();
                    } else {
                        functionsClass.openPrimaryApplications(Integer.parseInt(functionsClass.getOrderNumber(primaryPicks[0].getTag())), primaryClasses.get(0), functionsClass.getPackageName(primaryPicks[0].getTag()),
                                (int) (primaryPickApp.getX()), (int) (primaryPickApp.getHeight() - functionsClass.DpToPixel(121)), primaryPickApp.getWidth());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        primaryPickApp.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                try {
                    String packageName = functionsClass.getPackageName(primaryPicks[0].getTag());
                    String className = functionsClass.getClassName(primaryPicks[0].getTag());
                    int orderNumber = Integer.parseInt(functionsClass.getOrderNumber(primaryPicks[0].getTag()));

                    FunctionsClass.AuthOpenAppValues.authPackageName = packageName;
                    FunctionsClass.AuthOpenAppValues.authClassName = className;

                    if (functionsClass.isAppLocked(packageName)) {
                        FunctionsClass.AuthOpenAppValues.authPopupOption = true;
                        FunctionsClass.AuthOpenAppValues.authAnchorView = view;

                        functionsClass.openAuthInvocation();
                    } else {
                        functionsClass.popupOptions(getApplicationContext(), view, packageName, className, orderNumber);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        primaryNotificationDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String notificationPackage = functionsClass.getPackageName(primaryPicks[0].getTag());
                    String notificationClassName = primaryClasses.get(0);
                    functionsClass.PopupNotificationShortcuts(
                            view,
                            notificationPackage,
                            notificationClassName,
                            0
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        primaryNotificationDot.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                try {
                    Object sbservice = getSystemService("statusbar");
                    Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                    Method showsb = statusbarManager.getMethod("expandNotificationsPanel");//expandNotificationsPanel
                    showsb.invoke(sbservice);
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        Object sbservice = getSystemService("statusbar");
                        Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                        Method showsb = statusbarManager.getMethod("expand");//expandNotificationsPanel
                        showsb.invoke(sbservice);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                return true;
            }
        });

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_default);

        firebaseRemoteConfig.fetch(0)
                .addOnCompleteListener(HybridIntelligentStaticPrimary.this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            firebaseRemoteConfig.activateFetched();
                            if (firebaseRemoteConfig.getLong(functionsClass.versionCodeRemoteConfigKey()) > functionsClass.appVersionCode(getPackageName())) {
                                functionsClass.notificationCreator(
                                        functionsClass.isBetaTester() ? getString(R.string.updateAvailableBeta) : getString(R.string.updateAvailable),
                                        firebaseRemoteConfig.getString(functionsClass.upcomingChangeLogSummaryConfigKey()),
                                        (int) firebaseRemoteConfig.getLong(functionsClass.versionCodeRemoteConfigKey()),
                                        Integer.MAX_VALUE
                                );
                            } else {
                            }
                        } else {
                        }
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            primaryClasses.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        PublicVariable.inFront = false;
        PublicVariable.inMemory = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PublicVariable.inMemory = false;
        try {
            sqLiteDataBaseAppInfo.close();
            sqLiteDataBaseIntelligentServices.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent homeScreen = new Intent(Intent.ACTION_MAIN);
        homeScreen.addCategory(Intent.CATEGORY_HOME);
        homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeScreen, ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
    }

    @Override
    public void onClick(View view) {
        if (view instanceof ShapesImage) {
            switch (view.getId()) {
                case R.id.secondaryApps: {
                    int orderNumber = Integer.parseInt(functionsClass.getOrderNumber(view.getTag()));
                    String packageName = functionsClass.getPackageName(view.getTag());
                    String className = functionsClass.getClassName(view.getTag());

                    if (functionsClass.isAppLocked(packageName)) {
                        FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices = sqLiteDataBaseIntelligentServices;

                        FunctionsClass.AuthOpenAppValues.authPackageName = packageName;
                        FunctionsClass.AuthOpenAppValues.authClassName = className;
                        FunctionsClass.AuthOpenAppValues.authOrderNumber = orderNumber;
                        FunctionsClass.AuthOpenAppValues.authPositionX = (int) (primaryPickApp.getX());
                        FunctionsClass.AuthOpenAppValues.authPositionY = (int) (primaryPickApp.getHeight() / 2);
                        FunctionsClass.AuthOpenAppValues.authHW = primaryPickApp.getWidth();

                        FunctionsClass.AuthOpenAppValues.authUnlockIt = false;
                        FunctionsClass.AuthOpenAppValues.authPopupOption = false;

                        functionsClass.openAuthInvocation();
                    } else {
                        functionsClass.openPrimaryApplications(orderNumber, className, packageName,
                                (int) (primaryPickApp.getX()), (int) (primaryPickApp.getHeight() / 2), primaryPickApp.getWidth());
                    }

                    break;
                }
                case R.id.secondaryNotificationDot: {
                    String notificationPackage = view.getTag().toString();
                    Cursor cursor = sqLiteDataBaseIntelligentServices.getAppInfoDataFromPackageName(notificationPackage);
                    cursor.moveToFirst();
                    String notificationClassName = cursor.getString(cursor.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_CLASS_NAME));
                    functionsClass.PopupNotificationShortcuts(
                            view,
                            notificationPackage,
                            notificationClassName,
                            0
                    );
                    if (cursor != null) {
                        cursor.close();
                    }
                    break;
                }
            }
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (view instanceof ShapesImage) {
            switch (view.getId()) {
                case R.id.secondaryNotificationDot: {
                    try {
                        Object sbservice = getSystemService("statusbar");
                        Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                        Method showsb = statusbarManager.getMethod("expandNotificationsPanel");//expandNotificationsPanel
                        showsb.invoke(sbservice);
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            Object sbservice = getSystemService("statusbar");
                            Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                            Method showsb = statusbarManager.getMethod("expand");//expandNotificationsPanel
                            showsb.invoke(sbservice);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    break;
                }
                case R.id.secondaryApps: {
                    String packageName = functionsClass.getPackageName(view.getTag());
                    String className = functionsClass.getClassName(view.getTag());
                    int orderNumber = Integer.parseInt(functionsClass.getOrderNumber(view.getTag()));

                    FunctionsClass.AuthOpenAppValues.authPackageName = packageName;
                    FunctionsClass.AuthOpenAppValues.authClassName = className;

                    if (functionsClass.isAppLocked(packageName)) {
                        FunctionsClass.AuthOpenAppValues.authPopupOption = true;
                        FunctionsClass.AuthOpenAppValues.authAnchorView = view;

                        functionsClass.openAuthInvocation();
                    } else {
                        functionsClass.popupOptions(getApplicationContext(), view, packageName, className, orderNumber);
                    }

                    break;
                }
            }
        }
        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view instanceof TextView) {
            final TextView selectedIndex = (TextView) view;
            nestedScrollView.smoothScrollTo(
                    0,
                    ((int) loadView.getChildAt(mapIndex.get(selectedIndex.getText().toString())).getY() + functionsClass.DpToInteger(23 * 13))
            );
        }
        return true;
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {
            case SimpleGestureFilterSwitch.SWIPE_RIGHT: {


                break;
            }
            case SimpleGestureFilterSwitch.SWIPE_LEFT: {


                break;
            }
            case SimpleGestureFilterSwitch.SWIPE_UP: {


                break;
            }
            case SimpleGestureFilterSwitch.SWIPE_DOWN: {
                if (yScroll == 0) {
                    Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                    homeScreen.addCategory(Intent.CATEGORY_HOME);
                    homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeScreen, ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, R.anim.go_down).toBundle());
                }

                break;
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        this.simpleGestureFilterSwitch.onTouchEvent(motionEvent);

        return super.dispatchTouchEvent(motionEvent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 666) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount googleSignInAccount = task.getResult(ApiException.class);
                AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
                firebaseAuth.signInWithCredential(authCredential)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                                    if (firebaseUser != null) {
                                        functionsClass.savePreference(".BETA", "testerEmail", firebaseUser.getEmail());

                                        System.out.println("Firebase Activities Done Successfully");
                                        functionsClass.Toast(getString(R.string.alphaTitle), Gravity.TOP);
                                        try {
                                            progressDialog.dismiss();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        try {
                                            progressDialog.setMessage(Html.fromHtml(
                                                    "<big><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + getString(R.string.error) + "</font></big>"
                                                            + "<br/>" +
                                                            "<small><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + task.getException().getMessage() + "</font></small>"));
                                            progressDialog.setCancelable(true);
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progressDialog.dismiss();
                                                }
                                            }, 777);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    try {
                                        progressDialog.setMessage(Html.fromHtml(
                                                "<big><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + getString(R.string.error) + "</font></big>"
                                                        + "<br/>" +
                                                        "<small><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + task.getException().getMessage() + "</font></small>"));
                                        progressDialog.setCancelable(true);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressDialog.dismiss();
                                            }
                                        }, 777);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }

    private void IntelligentServices() {
        try {
            secondaryView = (LinearLayout) findViewById(R.id.secondaryView);
            secondaryView.removeAllViews();
            primaryClasses.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LoadIntelligentServices loadIntelligentServices = new LoadIntelligentServices();
        loadIntelligentServices.execute();
    }

    private void LoadIndex() {
        try {
            indexView.removeAllViews();
            mapIndex.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LoadApplicationsIndex loadApplicationsIndex = new LoadApplicationsIndex();
        loadApplicationsIndex.execute();
    }

    private class LoadIntelligentServices extends AsyncTask<Void, Void, Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                secondaryView.removeAllViews();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            List<Integer> minusList = new ArrayList<Integer>();
            try {
                minusList.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                int timeNow = Integer.parseInt(String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond));

                int maxValueSame = secondaryCount + 1;
                List<Integer> allAppLastUsed = sqLiteDataBaseIntelligentServices.getAllAppLastUsed("DESC", maxValueSame);
                List<Integer> allAppOrderNumber = sqLiteDataBaseIntelligentServices.getAllAppInfoOfOrderNumber(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, "DESC", maxValueSame);

                Map<Integer, Integer> mapOrderNumberMinusTime = new LinkedHashMap<Integer, Integer>();
                try {
                    mapOrderNumberMinusTime.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < allAppLastUsed.size(); i++) {
                    int minusTime = Math.abs(timeNow - allAppLastUsed.get(i));

                    sqLiteDataBaseIntelligentServices.updateAppInfoFromLastUsed(String.valueOf(allAppOrderNumber.get(i)), String.valueOf(allAppLastUsed.get(i)), minusTime);
                    minusList.add(minusTime);
                    mapOrderNumberMinusTime.put(allAppOrderNumber.get(i), allAppLastUsed.get(i));
                }
                Collections.sort(minusList);

                Set<Integer> duplicatedRemoved = new LinkedHashSet<Integer>(minusList);
                minusList.clear();
                minusList.addAll(duplicatedRemoved);

                Cursor cursorPrimary = null;
                for (int i = 0; i < minusList.size(); i++) {
                    cursorPrimary = sqLiteDataBaseIntelligentServices.getMinusData(minusList.get(i));
                    cursorPrimary.moveToFirst();

                    RelativeLayout secondaryItem = (RelativeLayout) getLayoutInflater()
                            .inflate(R.layout.secondary_item, null);
                    ShapesImage secondaryShapes = (ShapesImage) secondaryItem.findViewById(R.id.secondaryApps);
                    ShapesImage secondaryShapesNotificationDot = (ShapesImage) secondaryItem.findViewById(R.id.secondaryNotificationDot);
                    secondaryShapes.setShapeDrawable(functionsClass.shapesDrawables());
                    secondaryShapesNotificationDot.setShapeDrawable(functionsClass.shapesDrawables());
                    secondaryItems[i] = secondaryItem;
                    primaryPicks[i] = secondaryShapes;
                    secondaryNotificationDot[i] = secondaryShapesNotificationDot;

                    int Number = cursorPrimary.getInt(cursorPrimary.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_NUMBER));
                    String primaryPackages = cursorPrimary.getString(cursorPrimary.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_PACKAGES));
                    String primaryClassName = cursorPrimary.getString(cursorPrimary.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_CLASS_NAME));
                    primaryClasses.add(i, primaryClassName);

                    String[] tagObjects = new String[]{String.valueOf(Number), primaryPackages, primaryClassName};
                    primaryPicks[i].setTag(tagObjects);

                    secondaryNotificationDot[i].setTag(primaryPackages);
                    mapPackageNamePosition.put(primaryPackages, i);
                }
                if (cursorPrimary != null) {
                    cursorPrimary.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (BuildConfig.DEBUG) {
                    this.cancel(true);
                    finish();
                    return null;
                }
                functionsClass.LoadSaveAppInfo();
            }
            return minusList.size();
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (!PublicVariable.inMemory || PublicVariable.forceReload) {
                PublicVariable.forceReload = false;
                LoadApplicationsOffLimited loadApplicationsOffLimited = new LoadApplicationsOffLimited();
                loadApplicationsOffLimited.execute();
            }

            try {
                final int secondaryAmount = result - 1;
                try {
                    Drawable primaryAppIcon = functionsClass.shapedAppIcon(getPackageManager().getActivityInfo(new ComponentName(functionsClass.getPackageName(primaryPicks[0].getTag()), primaryClasses.get(0)), 0));
                    primaryPickApp.setImageDrawable(primaryAppIcon);
                } catch (PackageManager.NameNotFoundException nameNotFoundException) {
                    Drawable primaryAppIcon = functionsClass.shapedAppIcon(functionsClass.getPackageName(primaryPicks[0].getTag()));
                    primaryPickApp.setImageDrawable(primaryAppIcon);
                }

                if (functionsClass.RightLeft()) {
                    for (int i = secondaryAmount; i >= 1; i--) {
                        try {
                            Drawable appIcon = functionsClass.shapedAppIcon(getPackageManager().getActivityInfo(new ComponentName(functionsClass.getPackageName(primaryPicks[i].getTag()), primaryClasses.get(i)), 0));
                            primaryPicks[i].setImageDrawable(appIcon);
                            primaryPicks[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                            primaryPicks[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                            secondaryNotificationDot[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                            secondaryNotificationDot[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                            secondaryView.addView(secondaryItems[i]);
                        } catch (PackageManager.NameNotFoundException nameNotFoundException) {
                            if (primaryPicks[i].getTag().toString().equals(primaryClasses.get(i))) {
                                Drawable appIcon = functionsClass.shapedAppIcon(functionsClass.getPackageName(primaryPicks[i].getTag()));
                                primaryPicks[i].setImageDrawable(appIcon);
                                primaryPicks[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                                primaryPicks[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                                secondaryNotificationDot[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                                secondaryNotificationDot[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                                secondaryView.addView(secondaryItems[i]);
                            }
                        }
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                secondaryScrollView.scrollTo(
                                        ((int) secondaryView.getChildAt(secondaryAmount - 1).getX()),
                                        0
                                );
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    }, 100);
                } else if (!functionsClass.RightLeft()) {
                    for (int i = 1; i <= secondaryAmount; i++) {
                        try {
                            Drawable appIcon = functionsClass.shapedAppIcon(getPackageManager().getActivityInfo(new ComponentName(functionsClass.getPackageName(primaryPicks[i].getTag()), primaryClasses.get(i)), 0));
                            primaryPicks[i].setImageDrawable(appIcon);
                            primaryPicks[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                            primaryPicks[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                            secondaryNotificationDot[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                            secondaryNotificationDot[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                            secondaryView.addView(secondaryItems[i]);
                        } catch (PackageManager.NameNotFoundException nameNotFoundException) {
                            if (primaryPicks[i].getTag().toString().equals(primaryClasses.get(i))) {
                                Drawable appIcon = functionsClass.shapedAppIcon(functionsClass.getPackageName(primaryPicks[i].getTag()));
                                primaryPicks[i].setImageDrawable(appIcon);
                                primaryPicks[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                                primaryPicks[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                                secondaryNotificationDot[i].setOnClickListener(HybridIntelligentStaticPrimary.this);
                                secondaryNotificationDot[i].setOnLongClickListener(HybridIntelligentStaticPrimary.this);
                                secondaryView.addView(secondaryItems[i]);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            PublicVariable.primaryAppObjects.add(Integer.parseInt(functionsClass.getOrderNumber(primaryPicks[0].getTag())));
                            PublicVariable.primaryAppObjects.add(primaryClasses.get(0));
                            PublicVariable.primaryAppObjects.add(functionsClass.getPackageName(primaryPicks[0].getTag()));
                            PublicVariable.primaryAppObjects.add((int) (primaryPickApp.getX()));
                            PublicVariable.primaryAppObjects.add((int) (primaryPickApp.getHeight() - functionsClass.DpToPixel(121)));
                            PublicVariable.primaryAppObjects.add(primaryPickApp.getWidth());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            if (functionsClass.returnAPI() >= 25) {
                functionsClass.setAppShortcuts();
            }
        }
    }

    private class CheckInstalledApps extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean newData = false;
            try {
                if (functionsClass.countInstalledApplication() != sqLiteDataBaseAppInfo.numberOfRows()) {
                    newData = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                this.cancel(true);
            }
            return newData;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                PublicVariable.allApps = null;
                functionsClass.LoadSaveAppInfo();
            }
        }
    }

    private class LoadApplicationsOffLimited extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            File file = getApplicationContext().getDatabasePath(SqLiteDataBaseAppInfo.DATABASE_FILE_NAME);
            if (!file.exists()) {
                functionsClass.LoadSaveAppInfo();

                finish();
                this.cancel(true);
            }

            indexView.removeAllViews();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (PublicVariable.allApps != null) {
                    appData = PublicVariable.allApps;
                } else {
                    appData = sqLiteDataBaseAppInfo.getAllAppInfo(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_APP_NAME, "ASC");
                }
                limitedCountLine = ((int) appData.size() / 5);
                appNames = sqLiteDataBaseAppInfo.getAllAppInfoAppName(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_APP_NAME, "ASC");

                int itemOfIndex = 1;
                Cursor cursor = null;
                for (int navItem = 0; navItem < limitedCountLine; navItem++) {
                    try {
                        String newChar = appNames.get(navItem).substring(0, 1).toUpperCase();
                        if (navItem == 0) {
                            sections.add(new SectionedGridRecyclerViewAdapter.Section(navItem, newChar));
                        } else {
                            String oldChar = appNames.get(navItem - 1).substring(0, 1).toUpperCase();
                            if (!oldChar.equals(newChar)) {
                                sections.add(new SectionedGridRecyclerViewAdapter.Section(navItem, newChar));
                                indexList.add(newChar);
                                itemOfIndex = 1;
                            }
                        }

                        Number = appData.get(navItem);
                        cursor = sqLiteDataBaseAppInfo.getAppInfoDataRowFromNumber(Number);
                        cursor.moveToFirst();

                        PackageName = cursor.getString(cursor.getColumnIndex(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_PACKAGE_NAME));
                        ClassName = cursor.getString(cursor.getColumnIndex(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_CLASS_NAME));

                        try {
                            ComponentName componentName = new ComponentName(PackageName, ClassName);
                            ActivityInfo activityInfo = getPackageManager().getActivityInfo(componentName, 0);

                            AppName = activityInfo.loadLabel(getPackageManager()).toString();
                            AppIcon = functionsClass.shapedAppIcon(activityInfo);
                        } catch (PackageManager.NameNotFoundException nameNotFoundException) {
                            if (ClassName.equals(PackageName)) {
                                AppName = functionsClass.appName(PackageName);
                                AppIcon = functionsClass.shapedAppIcon(PackageName);
                            }
                        }

                        navDrawerItems.add(new NavDrawerItem(
                                HybridIntelligentStaticPrimary.class,
                                PackageName,
                                ClassName,
                                AppName,
                                AppIcon,
                                Integer.valueOf(Number)
                        ));

                        indexList.add(newChar);
                        indexItems.put(newChar, itemOfIndex++);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                recyclerViewAdapter = new CardHybridAdapter(HybridIntelligentStaticPrimary.this, getApplicationContext(), navDrawerItems, sqLiteDataBaseIntelligentServices);
            } catch (Exception e) {
                e.printStackTrace();
                this.cancel(true);
                finish();
            } finally {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            SectionedGridRecyclerViewAdapter.Section[] sectionsData = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
            SectionedGridRecyclerViewAdapter mSectionedAdapter = new SectionedGridRecyclerViewAdapter(
                    getApplicationContext(),
                    R.layout.section,
                    R.id.section_text,
                    loadView,
                    recyclerViewAdapter
            );
            mSectionedAdapter.setSections(sections.toArray(sectionsData));
            loadView.setAdapter(mSectionedAdapter);

            LoadApplicationsOff loadApplicationsOff = new LoadApplicationsOff();
            loadApplicationsOff.execute();

            try {
                functionsClass.ChangeLog(HybridIntelligentStaticPrimary.this, false);
                if (!functionsClass.AccessibilityServiceEnabled() && !functionsClass.readPreference("InteractionObserver", "Later", false)) {
                    functionsClass.AccessibilityServiceEntry(HybridIntelligentStaticPrimary.this);
                }
                if (!BuildConfig.DEBUG && functionsClass.networkConnection()) {
                    firebaseAuth.addAuthStateListener(
                            new FirebaseAuth.AuthStateListener() {
                                @Override
                                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                    FirebaseUser user = firebaseAuth.getCurrentUser();
                                    if (user == null) {
                                        functionsClass.savePreference(".BETA", "testerEmail", null);

                                    } else {
                                    }
                                }
                            }
                    );

                    if (functionsClass.readPreference(".BETA", "isBetaTester", false) && functionsClass.readPreference(".BETA", "testerEmail", null) == null) {
                        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(getString(R.string.webClientId))
                                .requestEmail()
                                .build();

                        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(HybridIntelligentStaticPrimary.this, googleSignInOptions);
                        try {
                            googleSignInClient.signOut();
                            googleSignInClient.revokeAccess();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent signInIntent = googleSignInClient.getSignInIntent();
                        startActivityForResult(signInIntent, 666);

                        progressDialog = new ProgressDialog(HybridIntelligentStaticPrimary.this,
                                functionsClass.LightDark() ? R.style.GeeksEmpire_Dialogue_Light : R.style.GeeksEmpire_Dialogue_Dark);
                        progressDialog.setMessage(Html.fromHtml(
                                "<big><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + getString(R.string.wait) + "</font></big>"
                                        + "<br/>" +
                                        "<small><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + getString(R.string.alphaTitle) + "</font></small>"));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class LoadApplicationsOff extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                int itemOfIndex = 1;
                Cursor cursor = null;
                for (int navItem = limitedCountLine; navItem < appData.size(); navItem++) {
                    try {
                        String newChar = appNames.get(navItem).substring(0, 1).toUpperCase();
                        if (navItem == 0) {
                        } else {
                            String oldChar = appNames.get(navItem - 1).substring(0, 1).toUpperCase();
                            if (!oldChar.equals(newChar)) {
                                sections.add(new SectionedGridRecyclerViewAdapter.Section(navItem, newChar));
                                indexList.add(newChar);
                                itemOfIndex = 1;
                            }
                        }

                        Number = appData.get(navItem);
                        cursor = sqLiteDataBaseAppInfo.getAppInfoDataRowFromNumber(Number);
                        cursor.moveToFirst();

                        PackageName = cursor.getString(cursor.getColumnIndex(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_PACKAGE_NAME));
                        ClassName = cursor.getString(cursor.getColumnIndex(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_CLASS_NAME));

                        try {
                            ComponentName componentName = new ComponentName(PackageName, ClassName);
                            ActivityInfo activityInfo = getPackageManager().getActivityInfo(componentName, 0);

                            AppName = activityInfo.loadLabel(getPackageManager()).toString();
                            AppIcon = functionsClass.shapedAppIcon(activityInfo);
                        } catch (PackageManager.NameNotFoundException nameNotFoundException) {
                            if (ClassName.equals(PackageName)) {
                                AppName = functionsClass.appName(PackageName);
                                AppIcon = functionsClass.shapedAppIcon(PackageName);
                            }
                        }

                        navDrawerItems.add(new NavDrawerItem(
                                HybridIntelligentStaticPrimary.class,
                                PackageName,
                                ClassName,
                                AppName,
                                AppIcon,
                                Integer.valueOf(Number)
                        ));

                        indexList.add(newChar);
                        indexItems.put(newChar, itemOfIndex++);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                for (int i = 1; i < 9; i++) {
                    sections.add(new SectionedGridRecyclerViewAdapter.Section(appData.size(), ""));
                }

                recyclerViewAdapter = new CardHybridAdapter(HybridIntelligentStaticPrimary.this, getApplicationContext(), navDrawerItems, sqLiteDataBaseIntelligentServices);
            } catch (Exception e) {
                e.printStackTrace();
                this.cancel(true);
                finish();
            } finally {
                PublicVariable.allApps = appData;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
            SectionedGridRecyclerViewAdapter mSectionedAdapter = new SectionedGridRecyclerViewAdapter(
                    getApplicationContext(),
                    R.layout.section,
                    R.id.section_text,
                    loadView,
                    recyclerViewAdapter
            );
            mSectionedAdapter.setSections(sections.toArray(dummy));
            loadView.setAdapter(mSectionedAdapter);

            LoadIndex();
        }
    }

    private class LoadApplicationsIndex extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                indexView.removeAllViews();
                mapIndex.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            int indexCount = indexList.size();
            for (int navItem = 0; navItem < indexCount; navItem++) {
                try {
                    String indexText = indexList.get(navItem);
                    if (mapIndex.get(indexText) == null/*avoid duplication*/) {
                        mapIndex.put(indexText, navItem);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            TextView textView = null;
            List<String> indexList = new ArrayList<String>(mapIndex.keySet());
            for (String index : indexList) {
                textView = (TextView) getLayoutInflater()
                        .inflate(R.layout.side_index_item_right, null);
                textView.setText(index.toUpperCase());
                textView.setTextColor(functionsClass.mixColors(PublicVariable.colorLightDarkOpposite, PublicVariable.primaryColor, 0.77f));
                textView.setOnTouchListener(HybridIntelligentStaticPrimary.this);
                indexView.addView(textView);
            }

            if (functionsClass.NotificationAccess() && functionsClass.SettingServiceRunning(NotificationListener.class)) {
                LoadNotificationDots loadNotificationDots = new LoadNotificationDots();
                loadNotificationDots.execute();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    PublicVariable.countAppInfo = functionsClass.countInstalledApplication();
                }
            }, 1000);
        }
    }

    private class LoadNotificationDots extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            notificationPackages = functionsClass.readFileLine(".notificationPackages");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (notificationPackages != null) {
                if (notificationPackages.length > 0) {
                    for (String notificationPackage : notificationPackages) {
                        if (functionsClass.databaseAvailable(notificationPackage)) {
                            sendBroadcast(new Intent("Notification_Dot").putExtra("NotificationPackage", notificationPackage));
                        } else {
                            functionsClass.removeLine(".notificationPackages", notificationPackage);
                        }
                    }
                }
            }
        }
    }
}
