/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.InteractionObserver;

import android.accessibilityservice.AccessibilityService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.view.accessibility.AccessibilityEvent;

import net.geekstools.hyper.intelligentservices.BuildConfig;
import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseIntelligentServices;

import java.util.Calendar;
import java.util.List;

public class InteractionObserver extends AccessibilityService {

    public static String PackageNameSplit, ClassNameSplit;
    FunctionsClass functionsClass;
    boolean doSplitSingle = false;

    public static void setPackageNameClassName(String PackageName, String ClassName) {
        PackageNameSplit = PackageName;
        ClassNameSplit = ClassName;
    }

    @Override
    protected void onServiceConnected() {
    }

    @Override
    public void onAccessibilityEvent(final AccessibilityEvent event) {
        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_TOUCH_INTERACTION_END: {
                if (event.getAction() == 10296) {
                    if (BuildConfig.DEBUG) {
                        System.out.println("*** PackageName: " + PackageNameSplit + " | " + "ClassName: " + ClassNameSplit);
                    }

                    if (doSplitSingle == false) {
                        doSplitSingle = true;
                        startActivity(new Intent(getApplicationContext(), SplitTransparentSingle.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }

                    IntentFilter intentFilterTest = new IntentFilter();
                    intentFilterTest.addAction("perform_split_single");
                    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            if (intent.getAction().equals("perform_split_single") && doSplitSingle == true) {
                                doSplitSingle = false;
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_TOGGLE_SPLIT_SCREEN);
                                sendBroadcast(new Intent(context.getString(R.string.splitAppsSingle))
                                        .putExtra("PackageName", PackageNameSplit)
                                        .putExtra("ClassName", ClassNameSplit));
                            }
                        }
                    };
                    registerReceiver(broadcastReceiver, intentFilterTest);
                } else if (event.getAction() == 66666) {
                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_NOTIFICATIONS);
                }
                break;
            }
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED: {
                String PackageName = (String) event.getPackageName();
                String ClassName = (String) event.getClassName();
                int previousCounter = 0;

                if (functionsClass.databaseAvailableIntelligentService()) {
                    SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(getApplicationContext(), functionsClass.sqLiteIntelligentServiceFileName(), 1);
                    try {
                        int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                        int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                        int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                        int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                        String lastUsed = String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond);

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        intent.setPackage(PackageName);
                        List<ResolveInfo> activityAliases = getPackageManager().queryIntentActivities(intent, 0);
                        if (activityAliases.size() > 1) {
                            Cursor cursor = null;
                            for (ResolveInfo resolveInfo : activityAliases) {
                                ClassName = resolveInfo.activityInfo.name;

                                cursor = sqLiteDataBaseIntelligentServices.getAppInfoDataFromClassName(ClassName);
                                cursor.moveToFirst();

                                previousCounter = cursor.getInt(cursor.getColumnIndexOrThrow(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER));
                                sqLiteDataBaseIntelligentServices.updateAppInfoAccessibility(
                                        ClassName,
                                        previousCounter + 1,
                                        Integer.parseInt(lastUsed)
                                );
                            }
                            if (!cursor.isClosed()) {
                                cursor.close();
                            }
                        } else {
                            Cursor cursor = sqLiteDataBaseIntelligentServices.getAppInfoDataFromPackageName(PackageName);
                            cursor.moveToFirst();
                            ClassName = cursor.getString(cursor.getColumnIndexOrThrow(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_CLASS_NAME));
                            previousCounter = cursor.getInt(cursor.getColumnIndexOrThrow(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER));
                            sqLiteDataBaseIntelligentServices.updateAppInfoAccessibility(
                                    ClassName,
                                    previousCounter + 1,
                                    Integer.parseInt(lastUsed)
                            );
                            if (!cursor.isClosed()) {
                                cursor.close();
                            }
                        }

                        if (BuildConfig.DEBUG) {
                            System.out.println("# " + previousCounter + " | " + PackageName + " | " + ClassName);
                        }
                    } catch (Exception cursorIndexOutOfBoundsException) {
                        cursorIndexOutOfBoundsException.printStackTrace();
                        try {
                            if (!PackageName.equals(getPackageName())
                                    && !functionsClass.ifDefaultLauncher(PackageName) && !functionsClass.ifSystem(PackageName) && functionsClass.canLaunch(PackageName)) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                try {
                                    if (getPackageManager().getLaunchIntentForPackage(PackageName) != null) {
                                        try {
                                            intent.setPackage(PackageName);
                                            List<ResolveInfo> activityAliases = getPackageManager().queryIntentActivities(intent, 0);
                                            if (activityAliases.size() > 1) {
                                                for (ResolveInfo resolveInfo : activityAliases) {
                                                    sqLiteDataBaseIntelligentServices.insertAppInfo(
                                                            String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                                            PackageName,
                                                            resolveInfo.activityInfo.name,
                                                            1,
                                                            0
                                                    );
                                                }
                                            } else {
                                                try {
                                                    if (activityAliases.size() == 1) {
                                                        sqLiteDataBaseIntelligentServices.insertAppInfo(
                                                                String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                                                PackageName,
                                                                activityAliases.get(0).activityInfo.name,
                                                                1,
                                                                0
                                                        );
                                                    } else if (activityAliases.size() == 0) {
                                                        sqLiteDataBaseIntelligentServices.insertAppInfo(
                                                                String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                                                PackageName,
                                                                PackageName,
                                                                1,
                                                                0
                                                        );
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } finally {
                        sqLiteDataBaseIntelligentServices.close();
                    }
                }
                break;
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        return START_STICKY;
    }

    @Override
    public void onInterrupt() {
        startService(new Intent(getApplicationContext(), InteractionObserver.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        functionsClass = new FunctionsClass(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
