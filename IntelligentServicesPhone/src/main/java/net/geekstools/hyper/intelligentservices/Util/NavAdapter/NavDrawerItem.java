/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.NavAdapter;

import android.graphics.drawable.Drawable;

public class NavDrawerItem {

    Class classNameCommand;
    String appName, appPackageName, className,
            notificationTime, notificationPackage, notificationAppName, notificationTitle, notificationText, notificationId;
    CharSequence title;
    Drawable appIcon, icon, notificationAppIcon, notificationLargeIcon;
    int orderNumber;

    public NavDrawerItem(Class classNameCommand, String appPackageName, String className, String appName, Drawable appIcon, int orderNumber) {
        this.classNameCommand = classNameCommand;
        this.appPackageName = appPackageName;
        this.className = className;
        this.appName = appName;
        this.appIcon = appIcon;
        this.orderNumber = orderNumber;
    }

    public NavDrawerItem(CharSequence title, Drawable icon) {
        this.title = title;
        this.icon = icon;
    }

    public NavDrawerItem(String notificationTime, String notificationPackage,
                         String notificationAppName, String notificationTitle, String notificationText, Drawable notificationAppIcon, Drawable notificationLargeIcon,
                         String notificationId) {
        this.notificationTime = notificationTime;
        this.notificationPackage = notificationPackage;
        this.notificationAppName = notificationAppName;
        this.notificationTitle = notificationTitle;
        this.notificationText = notificationText;
        this.notificationAppIcon = notificationAppIcon;
        this.notificationLargeIcon = notificationLargeIcon;
        this.notificationId = notificationId;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getAppPackageName() {
        return this.appPackageName;
    }

    public String getClassName() {
        return this.className;
    }

    public String getNotificationTime() {
        return this.notificationTime;
    }

    public String getNotificationPackage() {
        return this.notificationPackage;
    }

    public String getNotificationAppName() {
        return this.notificationAppName;
    }

    public String getNotificationTitle() {
        return this.notificationTitle;
    }

    public String getNotificationText() {
        return this.notificationText;
    }

    public String getNotificationId() {
        return this.notificationId;
    }

    public CharSequence getCharTitle() {
        return this.title;
    }

    public Drawable getAppIcon() {
        return this.appIcon;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public Drawable getNotificationAppIcon() {
        return this.notificationAppIcon;
    }

    public Drawable getNotificationLargeIcon() {
        return this.notificationLargeIcon;
    }

    public int getOrderNumber() {
        return this.orderNumber;
    }
}
