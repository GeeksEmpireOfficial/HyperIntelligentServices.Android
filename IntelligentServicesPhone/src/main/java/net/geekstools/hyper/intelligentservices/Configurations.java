/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.firebase.FirebaseApp;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.PermissionsCheckPoint;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseIntelligentServices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

public class Configurations extends Activity {

    public static boolean alertDialogueShow = false;
    FunctionsClass functionsClass;
    boolean wentForward = false;

    @Override
    protected void onCreate(Bundle Saved) {
        super.onCreate(Saved);
        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder()
                .disabled(BuildConfig.DEBUG)
                .build();
        Fabric.with(this, new Crashlytics.Builder().core(crashlyticsCore).build());
        FirebaseApp.initializeApp(getApplicationContext());

        functionsClass = new FunctionsClass(getApplicationContext(), Configurations.this);
        if (functionsClass.UsageStatsEnabled()) {
            UsageStatsManager usageStatsManager = (UsageStatsManager) getSystemService(USAGE_STATS_SERVICE);
            List<UsageStats> queryUsageStats = usageStatsManager
                    .queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                            System.currentTimeMillis() - 1000 * 60,            //begin
                            System.currentTimeMillis());                    //end
            Collections.sort(queryUsageStats, new LastTimeLaunchedComparator());
            String previousAppPack = queryUsageStats.get(1).getPackageName();
            if (previousAppPack.contains("com.google.android.googlequicksearchbox")) {
                System.out.println("*** GOOGLE");
                LoadIntelligentServices loadIntelligentServices = new LoadIntelligentServices();
                loadIntelligentServices.execute();

                return;
            }
        }
        functionsClass.extractWallpaperColor();
        try {
            if (!BuildConfig.DEBUG) {
                if (functionsClass.appVersionName(getPackageName()).contains("[BETA]")) {
                    functionsClass.savePreference(".BETA", "isBetaTester", true);
                    functionsClass.savePreference(".BETA", "installedVersionCode", functionsClass.appVersionCode(getPackageName()));
                    functionsClass.savePreference(".BETA", "installedVersionName", functionsClass.appVersionName(getPackageName()));
                    functionsClass.savePreference(".BETA", "deviceModel", functionsClass.getDeviceName());
                    functionsClass.savePreference(".BETA", "userRegion", functionsClass.getCountryIso());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences sharedPreferences = getSharedPreferences(".Configurations", MODE_PRIVATE);
        if (sharedPreferences.getBoolean("Permissions", false)) {
            if (functionsClass.UsageStatsEnabled()) {
                setContentView(R.layout.entry_point);

                functionsClass.setThemeColor(findViewById(R.id.entryPoint));

                if (!functionsClass.databaseAvailableIntelligentService()) {
                    PublicVariable.inMemory = false;
                    wentForward = true;

                    functionsClass.LoadSaveAppInfoIntelligentService(Configurations.class);
                    //finish();
                } else if (!functionsClass.databaseAvailable("AppInfo")) {
                    PublicVariable.inMemory = false;
                    wentForward = true;

                    functionsClass.LoadSaveAppInfo();
                    //finish();
                } else {
                    PublicVariable.inMemory = false;
                    wentForward = true;

                    Intent hybrid = new Intent();
                    if (functionsClass.StaticActive()) {
                        hybrid.setClass(getApplicationContext(), HybridIntelligentStaticPrimary.class);
                    } else if (!functionsClass.StaticActive()) {
                        hybrid.setClass(getApplicationContext(), HybridIntelligentActivePrimary.class);
                    }
                    hybrid.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
                    startActivity(hybrid, ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                    finish();
                    return;
                }
            } else {
                setContentView(R.layout.entry_point);

                functionsClass.Toast(getString(R.string.wait), Gravity.BOTTOM);
                functionsClass.setThemeColor(findViewById(R.id.entryPoint));

                WebView webView = (WebView) findViewById(R.id.configurationWaiting);
                webView.setVisibility(View.VISIBLE);
                webView.getSettings();
                webView.setBackgroundColor(Color.TRANSPARENT);
                webView.loadUrl("file:///android_asset/gifLoader.html");

                functionsClass.UsageAccess(Configurations.this);
            }
        } else if (functionsClass.returnAPI() >= 23) {
            Intent checkpoint = new Intent(getApplicationContext(), PermissionsCheckPoint.class);
            checkpoint.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(checkpoint);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!wentForward) {
            SharedPreferences sharedPreferences = getSharedPreferences(".Configurations", MODE_PRIVATE);
            if (sharedPreferences.getBoolean("Permissions", false)) {
                if (functionsClass.UsageStatsEnabled()) {
                    setContentView(R.layout.entry_point);

                    functionsClass.setThemeColor(findViewById(R.id.entryPoint));

                    WebView webView = (WebView) findViewById(R.id.configurationWaiting);
                    webView.setVisibility(View.VISIBLE);
                    webView.getSettings();
                    webView.setBackgroundColor(Color.TRANSPARENT);
                    webView.loadUrl("file:///android_asset/gifLoader.html");
                    if (!functionsClass.databaseAvailableIntelligentService()) {
                        PublicVariable.inMemory = false;

                        functionsClass.LoadSaveAppInfoIntelligentService(Configurations.class);
                        //finish();
                    } else if (!functionsClass.databaseAvailable("AppInfo")) {
                        PublicVariable.inMemory = false;

                        functionsClass.LoadSaveAppInfo();
                        //finish();
                    }
                } else {
                    if (!Configurations.alertDialogueShow) {
                        functionsClass.UsageAccess(Configurations.this);
                    }
                }
            }
        }
    }

    private class LoadIntelligentServices extends AsyncTask<Void, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Void... voids) {
            List<Integer> minusList = new ArrayList<Integer>();
            try {
                minusList.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
            String[] primaryObjects = new String[]{getPackageName(), getPackageName()};

            try {
                int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                int timeNow = Integer.parseInt(String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond));

                int secondaryCount = (functionsClass.columnCount(105)) + 3;
                int maxValueSame = secondaryCount + 1;
                String DATABASE_FILE_NAME = functionsClass.sqLiteIntelligentServiceFileName();
                SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(getApplicationContext(), DATABASE_FILE_NAME, 1);
                List<Integer> allAppLastUsed = sqLiteDataBaseIntelligentServices.getAllAppLastUsed("DESC", maxValueSame);
                List<Integer> allAppOrderNumber = sqLiteDataBaseIntelligentServices.getAllAppInfoOfOrderNumber(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, "DESC", maxValueSame);

                for (int i = 0; i < allAppLastUsed.size(); i++) {
                    int minusTime = Math.abs(timeNow - allAppLastUsed.get(i));

                    sqLiteDataBaseIntelligentServices.updateAppInfoFromLastUsed(
                            String.valueOf(allAppOrderNumber.get(i)),
                            String.valueOf(allAppLastUsed.get(i)),
                            minusTime);
                    minusList.add(minusTime);
                }
                Collections.sort(minusList);

                Set<Integer> duplicatedRemoved = new LinkedHashSet<Integer>(minusList);
                minusList.clear();
                minusList.addAll(duplicatedRemoved);

                Cursor cursorPrimary = sqLiteDataBaseIntelligentServices.getMinusData(minusList.get(0));
                cursorPrimary.moveToFirst();

                String primaryPackages = cursorPrimary.getString(cursorPrimary.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_PACKAGES));
                String primaryClassName = cursorPrimary.getString(cursorPrimary.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_CLASS_NAME));
                primaryObjects = new String[]{primaryPackages, primaryClassName};
            } catch (Exception e) {
                e.printStackTrace();
                primaryObjects = null;
            }
            return primaryObjects;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    Intent intent = new Intent(getApplicationContext(), LaunchPad.class);
                    intent.setAction("App_Shortcuts_Action");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.putExtra("packageName", result[0]);
                    intent.putExtra("className", result[1]);
                    intent.putExtra("positionX", ((functionsClass.displayX() / 2) - (functionsClass.DpToInteger(77) / 2)));
                    intent.putExtra("positionY", ((functionsClass.displayY() / 2) - (functionsClass.DpToInteger(77) / 2)));
                    intent.putExtra("HW", functionsClass.DpToInteger(77));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (!functionsClass.databaseAvailableIntelligentService()) {
                    functionsClass.LoadSaveAppInfoIntelligentService(Configurations.class);
                    finish();
                } else if (!functionsClass.databaseAvailable("AppInfo")) {
                    functionsClass.LoadSaveAppInfo();
                    finish();
                } else {
                    PublicVariable.inMemory = false;
                    Intent hybrid = new Intent();
                    if (functionsClass.StaticActive()) {
                        hybrid.setClass(getApplicationContext(), HybridIntelligentStaticPrimary.class);
                    } else if (!functionsClass.StaticActive()) {
                        hybrid.setClass(getApplicationContext(), HybridIntelligentActivePrimary.class);
                    }
                    hybrid.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
                    startActivity(hybrid, ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                    finish();
                    return;
                }
            }
        }
    }

    private class LastTimeLaunchedComparator implements Comparator<UsageStats> {
        @Override
        public int compare(UsageStats left, UsageStats right) {
            return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
        }
    }
}
