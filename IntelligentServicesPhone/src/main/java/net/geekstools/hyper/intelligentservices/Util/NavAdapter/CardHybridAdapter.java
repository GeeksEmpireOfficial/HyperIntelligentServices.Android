/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.NavAdapter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import net.geekstools.hyper.intelligentservices.BuildConfig;
import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseIntelligentServices;
import net.geekstools.imageview.customshapes.ShapesImage;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CardHybridAdapter extends RecyclerView.Adapter<CardHybridAdapter.ViewHolder> {

    FunctionsClass functionsClass;
    Activity activity;
    Context context;
    ArrayList<NavDrawerItem> navDrawerItems;
    SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices;

    ShapesImage[] notificationDot;

    int layoutInflater, idRippleShape;

    View view;
    ViewHolder viewHolder;

    RippleDrawable drawItem;

    Map<String, Integer> mapPackageNamePosition;

    int previousCounter;

    public CardHybridAdapter(Activity activity, Context context, ArrayList<NavDrawerItem> navDrawerItems, SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices) {
        this.context = context;
        this.activity = activity;
        this.navDrawerItems = navDrawerItems;
        this.sqLiteDataBaseIntelligentServices = sqLiteDataBaseIntelligentServices;

        functionsClass = new FunctionsClass(context, activity);

        notificationDot = new ShapesImage[navDrawerItems.size()];
        mapPackageNamePosition = new HashMap<String, Integer>();

        switch (functionsClass.shapesImageId()) {
            case 1:
                layoutInflater = R.layout.item_card_hybrid_droplet;
                idRippleShape = R.drawable.ripple_effect_shape_droplet;
                break;
            case 2:
                layoutInflater = R.layout.item_card_hybrid_circle;
                idRippleShape = R.drawable.ripple_effect_shape_circle;
                break;
            case 3:
                layoutInflater = R.layout.item_card_hybrid_square;
                idRippleShape = R.drawable.ripple_effect_shape_square;
                break;
            case 4:
                layoutInflater = R.layout.item_card_hybrid_squircle;
                idRippleShape = R.drawable.ripple_effect_shape_squircle;
                break;
            case 0:
                layoutInflater = R.layout.item_card_hybrid_noshape;
                idRippleShape = R.drawable.ripple_effect_no_bound;
                break;
            default:
                layoutInflater = R.layout.item_card_hybrid_noshape;
                idRippleShape = R.drawable.ripple_effect_no_bound;
                break;
        }
    }

    @Override
    public CardHybridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(layoutInflater, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolderBinder, final int position) {

        viewHolderBinder.shapedIcon.setImageDrawable(navDrawerItems.get(position).getAppIcon());
        viewHolderBinder.appName.setText(navDrawerItems.get(position).getAppName());
        viewHolderBinder.appName.setTextColor(functionsClass.mixColors(PublicVariable.colorLightDarkOpposite, PublicVariable.primaryColor, 0.77f));

        try {
            notificationDot[position] = viewHolderBinder.notificationDot;
            notificationDot[position].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String notificationPackage = navDrawerItems.get(position).getAppPackageName();
                    String notificationClassName = navDrawerItems.get(position).getClassName();
                    functionsClass.PopupNotificationShortcuts(
                            view,
                            notificationPackage,
                            notificationClassName,
                            position
                    );
                }
            });
            notificationDot[position].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (functionsClass.AccessibilityServiceEnabled()) {
                        functionsClass.sendInteractionObserverEvent(view, navDrawerItems.get(position).getAppPackageName(), navDrawerItems.get(position).getClassName(), AccessibilityEvent.TYPE_TOUCH_INTERACTION_END, 66666);
                    } else {
                        try {
                            Object sbservice = context.getSystemService("statusbar");
                            Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                            Method showsb = statusbarManager.getMethod("expandNotificationsPanel");//expandNotificationsPanel
                            showsb.invoke(sbservice);
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                Object sbservice = context.getSystemService("statusbar");
                                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                                Method showsb = statusbarManager.getMethod("expand");//expandNotificationsPanel
                                showsb.invoke(sbservice);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                    return true;
                }
            });

            mapPackageNamePosition.put(navDrawerItems.get(position).getAppPackageName(), position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolderBinder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String PackageName = navDrawerItems.get(position).getAppPackageName();
                final String ClassName = navDrawerItems.get(position).getClassName();
                final int orderNumber = navDrawerItems.get(position).getOrderNumber();

                int[] positionXY = new int[2];
                view.getLocationInWindow(positionXY);
                if (BuildConfig.DEBUG) {
                    System.out.println("X Position *** " + positionXY[0]);
                    System.out.println("Y Position *** " + positionXY[1]);
                    System.out.println(orderNumber + "# " + previousCounter + " | " + PackageName + " | " + ClassName);
                }

                /*Locked*/
                if (functionsClass.isAppLocked(PackageName)) {
                    FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices = sqLiteDataBaseIntelligentServices;

                    FunctionsClass.AuthOpenAppValues.authPackageName = PackageName;
                    FunctionsClass.AuthOpenAppValues.authClassName = ClassName;
                    FunctionsClass.AuthOpenAppValues.authOrderNumber = orderNumber;
                    FunctionsClass.AuthOpenAppValues.authPositionX = positionXY[0];
                    FunctionsClass.AuthOpenAppValues.authPositionY = positionXY[1];
                    FunctionsClass.AuthOpenAppValues.authHW = view.getWidth();

                    FunctionsClass.AuthOpenAppValues.authUnlockIt = false;
                    FunctionsClass.AuthOpenAppValues.authPopupOption = false;

                    functionsClass.openAuthInvocation();
                } else {
                    functionsClass.appsLaunchPad(PackageName, ClassName, positionXY[0], positionXY[1], view.getWidth(), functionsClass.FreeForm());

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!functionsClass.databaseAvailableIntelligentService()) {
                                SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context.getApplicationContext(), functionsClass.sqLiteIntelligentServiceFileName(), 1);
                                sqLiteDataBaseIntelligentServices.insertAppInfo(
                                        String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                        PackageName,
                                        ClassName,
                                        1,
                                        0
                                );
                                sqLiteDataBaseIntelligentServices.close();
                            } else {
                                try {
                                    int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                                    int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                                    int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                                    int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                                    String lastUsed = String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond);

                                    Cursor cursor = sqLiteDataBaseIntelligentServices.getAppInfoDataFromOrderNumber(orderNumber);
                                    cursor.moveToFirst();
                                    previousCounter = cursor.getInt(cursor.getColumnIndexOrThrow(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER));
                                    sqLiteDataBaseIntelligentServices.updateAppInfoFromNumber(
                                            String.valueOf(orderNumber),
                                            previousCounter + 1,
                                            Integer.parseInt(lastUsed)
                                    );
                                    if (!cursor.isClosed()) {
                                        cursor.close();
                                    }
                                } catch (Exception cursorIndexOutOfBoundsException) {
                                    cursorIndexOutOfBoundsException.printStackTrace();
                                    sqLiteDataBaseIntelligentServices.insertAppInfo(
                                            String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                            PackageName,
                                            ClassName,
                                            1,
                                            0
                                    );
                                }
                            }
                        }
                    }, 333);
                }
            }
        });
        viewHolderBinder.item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String PackageName = navDrawerItems.get(position).getAppPackageName();
                String ClassName = navDrawerItems.get(position).getClassName();
                final int orderNumber = navDrawerItems.get(position).getOrderNumber();

                FunctionsClass.AuthOpenAppValues.authPackageName = PackageName;
                FunctionsClass.AuthOpenAppValues.authClassName = ClassName;

                if (functionsClass.isAppLocked(PackageName)) {
                    FunctionsClass.AuthOpenAppValues.authPopupOption = true;
                    FunctionsClass.AuthOpenAppValues.authAnchorView = view;

                    functionsClass.openAuthInvocation();
                } else {
                    functionsClass.popupOptions(context, view, PackageName, ClassName, orderNumber);
                }

                return true;
            }
        });

        drawItem = (RippleDrawable) context.getResources().getDrawable(idRippleShape);
        drawItem.setColor(ColorStateList.valueOf(functionsClass.extractDominantColor(navDrawerItems.get(position).getAppIcon())));
        viewHolderBinder.item.setBackground(drawItem);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(context.getString(R.string.resetTextColor));
        intentFilter.addAction(context.getString(R.string.notificationDot));
        intentFilter.addAction(context.getString(R.string.notificationDotNo));
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(context.getString(R.string.resetTextColor))) {
                    viewHolderBinder.appName.setTextColor(PublicVariable.colorLightDarkOpposite);
                } else if (intent.getAction().equals(context.getString(R.string.notificationDot))) {
                    try {
                        String notificationPackage = intent.getStringExtra("NotificationPackage");
                        if (functionsClass.databaseAvailable(notificationPackage)) {
                            int notificationPosition = mapPackageNamePosition.get(notificationPackage);
                            Drawable dotDrawable = null;
                            switch (functionsClass.shapesImageId()) {
                                case 1:
                                    dotDrawable = context.getDrawable(R.drawable.dot_droplet_icon).mutate();
                                    break;
                                case 2:
                                    dotDrawable = context.getDrawable(R.drawable.dot_circle_icon).mutate();
                                    break;
                                case 3:
                                    dotDrawable = context.getDrawable(R.drawable.dot_square_icon).mutate();
                                    break;
                                case 4:
                                    dotDrawable = context.getDrawable(R.drawable.dot_squircle_icon).mutate();
                                    break;
                                case 0:
                                    dotDrawable = functionsClass.appIcon(navDrawerItems.get(notificationPosition).getAppPackageName()).mutate();
                                    break;
                            }
                            if (functionsClass.LightDark()) {
                                dotDrawable.setTint(functionsClass.manipulateColor(functionsClass.extractVibrantColor(functionsClass.appIcon(navDrawerItems.get(notificationPosition).getAppPackageName())), 1.30f));
                            } else {
                                dotDrawable.setTint(functionsClass.manipulateColor(functionsClass.extractVibrantColor(functionsClass.appIcon(navDrawerItems.get(notificationPosition).getAppPackageName())), 0.50f));
                            }
                            notificationDot[notificationPosition].setImageDrawable(dotDrawable);
                            notificationDot[notificationPosition].setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (intent.getAction().equals(context.getString(R.string.notificationDotNo))) {
                    try {
                        String notificationPackage = intent.getStringExtra("NotificationPackage");
                        int notificationPosition = mapPackageNamePosition.get(notificationPackage);
                        notificationDot[notificationPosition].setVisibility(View.INVISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        context.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public int getItemCount() {
        return navDrawerItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout item;
        ShapesImage shapedIcon, notificationDot;
        TextView appName;

        public ViewHolder(View view) {
            super(view);
            item = (RelativeLayout) view.findViewById(R.id.item);
            shapedIcon = (ShapesImage) view.findViewById(R.id.icon);
            appName = (TextView) view.findViewById(R.id.desc);
            notificationDot = (ShapesImage) view.findViewById(R.id.notificationDot);
        }
    }
}
