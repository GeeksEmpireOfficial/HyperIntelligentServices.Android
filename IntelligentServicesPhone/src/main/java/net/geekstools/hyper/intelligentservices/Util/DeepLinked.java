/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import net.geekstools.hyper.intelligentservices.Configurations;

public class DeepLinked extends Activity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        startActivity(new Intent(getApplicationContext(), Configurations.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
