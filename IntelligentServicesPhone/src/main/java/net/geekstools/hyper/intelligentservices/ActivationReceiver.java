/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseAppInfo;

public class ActivationReceiver extends Service {

    Context context;
    FunctionsClass functionsClass;
    SqLiteDataBaseAppInfo sqLiteDataBaseAppInfo;

    BroadcastReceiver broadcastReceiverONOFF;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        if (intent != null) {
            if (intent.hasExtra("Ready")) {
                if (intent.getBooleanExtra("Ready", true)) {
                    startForeground(666, functionsClass.notificationCreator(context.getString(R.string.app_name), context.getString(R.string.app_name), 666, Integer.MIN_VALUE));
                }
            }
        }

        if (functionsClass.alwaysReady()) {
            IntentFilter intentFilterNewDataSet = new IntentFilter();
            intentFilterNewDataSet.addAction(Intent.ACTION_SCREEN_ON);
            intentFilterNewDataSet.addAction(Intent.ACTION_SCREEN_OFF);
            broadcastReceiverONOFF = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                        PublicVariable.allApps = sqLiteDataBaseAppInfo.getAllAppInfo(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_APP_NAME, "ASC");

                        stopForeground(true);
                        Intent openBackground = new Intent(getApplicationContext(), functionsClass.StaticActive() ? HybridIntelligentStaticPrimary.class : HybridIntelligentActivePrimary.class);
                        openBackground.putExtra("goHome", true);
                        openBackground.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(openBackground);
                    } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                        startForeground(666, functionsClass.notificationCreator(context.getString(R.string.app_name), context.getString(R.string.app_name), 666, Integer.MIN_VALUE));
                    }
                }
            };
            registerReceiver(broadcastReceiverONOFF, intentFilterNewDataSet);
        }
        return functionsClass.alwaysReadyMode();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        functionsClass = new FunctionsClass(context);
        sqLiteDataBaseAppInfo = new SqLiteDataBaseAppInfo(context, 1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!functionsClass.alwaysReady()) {
            try {
                unregisterReceiver(broadcastReceiverONOFF);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            startService(new Intent(getApplicationContext(), ActivationReceiver.class).putExtra("Ready", true));
        }
    }
}
