/*
 * Copyright © 2020 By Geeks Empire.
 *
 * Created by Elias Fazel on 2/7/20 9:46 AM
 * Last modified 2/7/20 9:41 AM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Functions;

import android.Manifest;
import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.WallpaperManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.Icon;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.VectorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ListPopupWindow;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.ColorUtils;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
import androidx.palette.graphics.Palette;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import net.geekstools.hyper.intelligentservices.BuildConfig;
import net.geekstools.hyper.intelligentservices.Configurations;
import net.geekstools.hyper.intelligentservices.HybridIntelligentActivePrimary;
import net.geekstools.hyper.intelligentservices.HybridIntelligentStaticPrimary;
import net.geekstools.hyper.intelligentservices.LaunchPad;
import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.ActivityRecognition.ActivityRecognitionService;
import net.geekstools.hyper.intelligentservices.Util.AppShortcuts.SetAppShortcuts;
import net.geekstools.hyper.intelligentservices.Util.Auth.AuthActivityHelper;
import net.geekstools.hyper.intelligentservices.Util.Auth.AuthShortcuts;
import net.geekstools.hyper.intelligentservices.Util.Auth.FingerprintAuthenticationDialogFragment;
import net.geekstools.hyper.intelligentservices.Util.InteractionObserver.InteractionObserver;
import net.geekstools.hyper.intelligentservices.Util.NavAdapter.NavDrawerItem;
import net.geekstools.hyper.intelligentservices.Util.Notification.Adapter.PopupShortcutsNotification;
import net.geekstools.hyper.intelligentservices.Util.Notification.SqLiteDataBaseNotification;
import net.geekstools.hyper.intelligentservices.Util.SettingGUI.SettingGUIDark;
import net.geekstools.hyper.intelligentservices.Util.SettingGUI.SettingGUILight;
import net.geekstools.hyper.intelligentservices.Util.SharingService;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseAppInfo;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseIntelligentServices;
import net.geekstools.hyper.intelligentservices.Util.UI.Splash.FloatingSplash;
import net.geekstools.imageview.customshapes.ShapesImage;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import static android.content.Context.ACCESSIBILITY_SERVICE;
import static android.content.Context.VIBRATOR_SERVICE;

public class FunctionsClass {

    public static final String KEY_NAME_NOT_INVALIDATED = "key_not_invalidated";
    public static final String DEFAULT_KEY_NAME = "default_key";

    Activity activity;
    Context context;
    int API;

    public FunctionsClass(Context context) {
        this.context = context;
        API = Build.VERSION.SDK_INT;

        loadSavedColor();
    }

    public FunctionsClass(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        API = Build.VERSION.SDK_INT;

        loadSavedColor();
    }

    /*Floating Shortcuts Integration*/
    public void sendFloatingShortcuts(Activity activity, String packageName, String className) {
        try {
            Intent floatingIntent = new Intent();
            floatingIntent.setAction(context.getString(R.string.create_floating_shortcuts_action));
            floatingIntent.putExtra("PackageName", packageName);
            floatingIntent.putExtra("ClassName", className);
            if (appInstalledOrNot((context.getString(R.string.floating_shortcuts_pro_package)))) {
                floatingIntent.setClassName(context.getString(R.string.floating_shortcuts_pro_package),
                        context.getString(R.string.create_floating_shortcuts_class));
                floatingIntent.addCategory(Intent.CATEGORY_DEFAULT);
            } else if (appInstalledOrNot(context.getString(R.string.floating_shortcuts_free_package))) {
                floatingIntent.setClassName(context.getString(R.string.floating_shortcuts_free_package),
                        context.getString(R.string.create_floating_shortcuts_class));
                floatingIntent.addCategory(Intent.CATEGORY_DEFAULT);
            } else {
                floatingIntent.setAction(Intent.ACTION_VIEW);
                floatingIntent.setData(Uri.parse(context.getString(R.string.play_store_link) + context.getString(R.string.floating_shortcuts_pro_package)));
            }
            activity.startActivity(floatingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Info File Functions*/
    public void LoadSaveAppInfo() {
        LoadApplications loadApplications = new LoadApplications();
        loadApplications.execute();
    }

    public void LoadSaveAppInfoIntelligentService(Class aClass) {
        LoadApplicationsIntelligentService loadApplicationsIntelligentService = new LoadApplicationsIntelligentService();
        loadApplicationsIntelligentService.execute(aClass.getName());
    }

    public void UpdateSqLiteDatabaseInfo(String packageNameToDelete) {
        UpdateSqLiteDatabase updateSqLiteDatabase = new UpdateSqLiteDatabase();
        updateSqLiteDatabase.execute(packageNameToDelete);
    }

    private void filterInstalledApplications(List<ResolveInfo> resolveInfos) throws Exception {
        Collections.sort(resolveInfos, new ResolveInfo.DisplayNameComparator(context.getPackageManager()));

        try {
            context.deleteDatabase(SqLiteDataBaseAppInfo.DATABASE_FILE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SqLiteDataBaseAppInfo sqLiteDataBaseAppInfo = new SqLiteDataBaseAppInfo(context, 1);

        int rowNumber = 0;
        for (ResolveInfo resolveInfo : resolveInfos) {
            try {
                if (context.getPackageManager().getLaunchIntentForPackage(resolveInfo.activityInfo.packageName) != null) {
                    try {
                        sqLiteDataBaseAppInfo.insertAppInfo(
                                rowNumber,
                                resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name,
                                CapitalizeFirstChar(String.valueOf(resolveInfo.loadLabel(context.getPackageManager())))
                        );
                        rowNumber++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            sqLiteDataBaseAppInfo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!databaseAvailableIntelligentService() || !(databaseIntelligentServiceCountRow() > 0)) {
            filterInstalledApplicationsIntelligentService(resolveInfos);
        }
    }

    private SqLiteDataBaseIntelligentServices filterInstalledApplicationsIntelligentService(List<ResolveInfo> resolveInfos) throws Exception {
        Collections.sort(resolveInfos, new ResolveInfo.DisplayNameComparator(context.getPackageManager()));

        SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context, sqLiteIntelligentServiceFileName(), 1);
        if (!databaseAvailableIntelligentService()) {
            int rowNumber = 0;
            for (ResolveInfo resolveInfo : resolveInfos) {
                try {
                    if (context.getPackageManager().getLaunchIntentForPackage(resolveInfo.activityInfo.packageName) != null) {
                        try {
                            sqLiteDataBaseIntelligentServices.insertAppInfo(
                                    String.valueOf(rowNumber),
                                    resolveInfo.activityInfo.packageName,
                                    resolveInfo.activityInfo.name,
                                    0,
                                    0
                            );
                            rowNumber++;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            sqLiteDataBaseIntelligentServices.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!databaseAvailable("AppInfo") || !(databaseAppInfoCountRow() > 0)) {
            Intent appInfoIntent = new Intent();
            appInfoIntent.setAction(Intent.ACTION_MAIN);
            appInfoIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            filterInstalledApplications(context.getPackageManager().queryIntentActivities(appInfoIntent, 0));
        }
        return sqLiteDataBaseIntelligentServices;
    }

    /*Option Functions*/
    public void popupOptions(final Context context, View anchorView, final String PackageName, final String ClassName, int orderNumber) {
        PopupMenu popupMenu = new PopupMenu(context, anchorView, Gravity.CENTER);
        if (LightDark()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                popupMenu = new PopupMenu(context, anchorView, Gravity.CENTER, 0, R.style.GeeksEmpire_Dialogue_Light);
            }
        } else if (!LightDark()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                popupMenu = new PopupMenu(context, anchorView, Gravity.CENTER, 0, R.style.GeeksEmpire_Dialogue_Dark);
            }
        }

        Drawable backgroundDrawable = shapesDrawables();
        Drawable foregroundDrawable = context.getDrawable(R.drawable.w_pref_popup).mutate();

        if (shapesDrawables() == null) {
            backgroundDrawable = context.getDrawable(R.drawable.ic_launcher).mutate();
            backgroundDrawable.setAlpha(0);

            foregroundDrawable.setTint(extractVibrantColor(appIcon(PackageName)));
        } else {
            backgroundDrawable = shapesDrawables().mutate();
            backgroundDrawable.setTint(extractVibrantColor(appIcon(PackageName)));
        }

        LayerDrawable popupItemIcon = new LayerDrawable(
                new Drawable[]{
                        backgroundDrawable,
                        foregroundDrawable
                });

        String[] menuItems = context.getResources().getStringArray(R.array.ContextMenuLock);
        if (isAppLocked(PackageName)) {
            menuItems = context.getResources().getStringArray(R.array.ContextMenuUnlock);
        } else {
            menuItems = context.getResources().getStringArray(R.array.ContextMenuLock);
        }
        for (int itemId = 0; itemId < menuItems.length; itemId++) {
            popupMenu.getMenu().add(Menu.NONE, itemId, itemId,
                    Html.fromHtml("<font color='" + PublicVariable.colorLightDarkOpposite + "'>" + menuItems[itemId] + "</font>"))
                    .setIcon(popupItemIcon);
        }

        Drawable backgroundDrawableIcon = shapesDrawables();
        if (shapesDrawables() == null) {
            backgroundDrawableIcon = context.getDrawable(R.drawable.ic_launcher).mutate();
            backgroundDrawableIcon.setAlpha(0);
        } else {
            backgroundDrawableIcon = shapesDrawables().mutate();
            backgroundDrawableIcon.setTint(extractVibrantColor(appIcon(PackageName)));
        }

        Drawable foregroundDrawableIcon = appIcon(PackageName);
        try {
            foregroundDrawableIcon = appIcon(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            foregroundDrawableIcon = appIcon(PackageName);
        }
        LayerDrawable popupItemAppIcon = new LayerDrawable(
                new Drawable[]{
                        backgroundDrawableIcon,
                        foregroundDrawableIcon
                });
        popupItemAppIcon.setLayerInset(1, 35, 35, 35, 35);
        try {
            popupMenu.getMenu().add(Menu.NONE, (menuItems.length + 1), (menuItems.length + 1),
                    Html.fromHtml("<big><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + activityLabel(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0)) + "</font></big>"))
                    .setIcon(popupItemAppIcon);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            popupMenu.getMenu().add(Menu.NONE, (menuItems.length + 1), (menuItems.length + 1),
                    Html.fromHtml("<big><font color='" + PublicVariable.colorLightDarkOpposite + "'>" + appName(PackageName) + "</font></big>"))
                    .setIcon(popupItemAppIcon);
        }

        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case 0://Float It
                        if (freeFormSupport(context) && FreeForm()) {
                            appsLaunchPad(PackageName, ClassName,
                                    ((displayX() / 2) - (anchorView.getWidth() / 2)),
                                    ((displayY() / 2) - (anchorView.getWidth() / 2)),
                                    anchorView.getWidth(),
                                    true);
                        } else {
                            Toast(context.getString(R.string.notEnabledFloatIt), Gravity.TOP);
                        }

                        break;
                    case 1://Split It
                        sendInteractionObserverEvent(anchorView,
                                PackageName,
                                ClassName,
                                AccessibilityEvent.TYPE_TOUCH_INTERACTION_END, 10296);

                        break;
                    case 2://LockIt/UnloockIt
                        if (isAppLocked(PackageName)) {
                            try {
                                AuthOpenAppValues.authUnlockIt = true;

                                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(context, android.R.anim.fade_in, android.R.anim.fade_out);
                                context.startActivity(new Intent(context, AuthActivityHelper.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), activityOptions.toBundle());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            saveFile(PackageName + ".LOCKED", context.getString(R.string.authing));
                        }

                        break;
                    case 3://Create Floating Shortcuts
                        sendFloatingShortcuts(activity, PackageName, ClassName);

                        break;
                    case 4://Create Shortcuts on Home Screen
                        Intent differentIntent = new Intent(context, AuthShortcuts.class);
                        differentIntent.setAction(Intent.ACTION_MAIN);
                        differentIntent.putExtra("PackageName", PackageName);
                        differentIntent.putExtra("ClassName", ClassName);
                        differentIntent.putExtra("OrderNumber", orderNumber);

                        Drawable drawableAppIcon;
                        try {
                            if (returnAPI() >= 26) {
                                drawableAppIcon = appIcon(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0));
                            } else {
                                drawableAppIcon = bitmapToDrawable(appIconBitmap(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0)));
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                            drawableAppIcon = appIcon(PackageName);
                        }

                        if (returnAPI() >= 26) {
                            try {
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    ShortcutInfo shortcutInfo = new ShortcutInfo.Builder(context, PackageName + SystemClock.currentThreadTimeMillis())
                                            .setShortLabel(activityLabel(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0)))
                                            .setLongLabel(activityLabel(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0)))
                                            .setIcon(Icon.createWithAdaptiveBitmap(drawableToBitmap(drawableAppIcon)))
                                            .setIntent(differentIntent)
                                            .build();

                                    context.getSystemService(ShortcutManager.class).requestPinShortcut(shortcutInfo, null);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                Intent removeIntent = new Intent();
                                removeIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, differentIntent);
                                removeIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, activityLabel(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0)));
                                removeIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
                                context.sendBroadcast(removeIntent);

                                Intent addIntent = new Intent();
                                addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, differentIntent);
                                addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, activityLabel(context.getPackageManager().getActivityInfo(new ComponentName(PackageName, ClassName), 0)));
                                addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, drawableToBitmap(drawableAppIcon));
                                addIntent.putExtra("duplicate", true);
                                addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                                context.sendBroadcast(addIntent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public void openAuthInvocation() {
        try {
            ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(context, android.R.anim.fade_in, android.R.anim.fade_out);
            context.startActivity(new Intent(context, AuthActivityHelper.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), activityOptions.toBundle());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean initCipher(Cipher cipher, String keyName) {
        try {
            FunctionsClass.AuthOpenAppValues.keyStore.load(null);
            SecretKey key = (SecretKey) FunctionsClass.AuthOpenAppValues.keyStore.getKey(keyName, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    public void Authed(boolean withFingerprint, @Nullable FingerprintManager.CryptoObject cryptoObject) {
        if (withFingerprint) {
            assert cryptoObject != null;
            tryEncrypt(cryptoObject.getCipher());
        }
    }

    public void authConfirmed(byte[] encrypted) {
        if (encrypted != null) {
            System.out.println("*** Authed ***");
            if (AuthOpenAppValues.authUnlockIt) {
                context.deleteFile(AuthOpenAppValues.authPackageName + ".LOCKED");
            } else if (AuthOpenAppValues.authPopupOption) {
                popupOptions(context, AuthOpenAppValues.authAnchorView, AuthOpenAppValues.authPackageName, AuthOpenAppValues.authClassName, AuthOpenAppValues.authOrderNumber);
            } else {
                appsLaunchPad(AuthOpenAppValues.authPackageName, AuthOpenAppValues.authClassName, AuthOpenAppValues.authPositionX, AuthOpenAppValues.authPositionY, AuthOpenAppValues.authHW, FreeForm());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!databaseAvailableIntelligentService()) {
                            SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context.getApplicationContext(), sqLiteIntelligentServiceFileName(), 1);
                            sqLiteDataBaseIntelligentServices.insertAppInfo(
                                    String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                    AuthOpenAppValues.authPackageName,
                                    AuthOpenAppValues.authClassName,
                                    1,
                                    0
                            );
                            sqLiteDataBaseIntelligentServices.close();
                        } else {
                            try {
                                int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                                int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                                int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                                int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                                String lastUsed = String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond);

                                Cursor cursor = FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices.getAppInfoDataFromOrderNumber(AuthOpenAppValues.authOrderNumber);
                                cursor.moveToFirst();
                                int previousCounter = cursor.getInt(cursor.getColumnIndexOrThrow(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER));
                                FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices.updateAppInfoFromNumber(
                                        String.valueOf(AuthOpenAppValues.authOrderNumber),
                                        previousCounter + 1,
                                        Integer.parseInt(lastUsed)
                                );
                                if (!cursor.isClosed()) {
                                    cursor.close();
                                }
                            } catch (Exception cursorIndexOutOfBoundsException) {
                                cursorIndexOutOfBoundsException.printStackTrace();
                                FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices.insertAppInfo(
                                        String.valueOf(FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                        AuthOpenAppValues.authPackageName,
                                        AuthOpenAppValues.authClassName,
                                        1,
                                        0
                                );
                            }
                        }

                        if (activity.getClass().getSimpleName().equals(AuthShortcuts.class.getSimpleName())) {
                            Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                            homeScreen.addCategory(Intent.CATEGORY_HOME);
                            homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(homeScreen, ActivityOptions.makeCustomAnimation(context, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                        }
                    }
                }, 333);
            }
        } else {

        }
    }

    public void authError() {

        System.out.println("Auth Error");

    }

    private void tryEncrypt(Cipher cipher) {
        try {
            byte[] encrypted = cipher.doFinal("Geeks Empire".getBytes());
            authConfirmed(encrypted);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void createKey(String keyName, boolean invalidatedByBiometricEnrollment) {
        // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
        // for your flow. Use of keys is necessary if you need to know if the set of
        // enrolled fingerprints has changed.
        try {
            FunctionsClass.AuthOpenAppValues.keyStore.load(null);
            // Set the alias of the entry in Android KeyStore where the key will appear
            // and the constrains (purposes) in the constructor of the Builder

            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    // Require the user to authenticate with a fingerprint to authorize every use
                    // of the key
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);

            // This is a workaround to avoid crashes on devices whose API level is < 24
            // because KeyGenParameterSpec.Builder#setInvalidatedByBiometricEnrollment is only
            // visible on API level +24.
            // Ideally there should be a compat library for KeyGenParameterSpec.Builder but
            // which isn't available yet.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);
            }
            FunctionsClass.AuthOpenAppValues.keyGenerator.init(builder.build());
            FunctionsClass.AuthOpenAppValues.keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isAppLocked(String PackageName) {
        return context.getFileStreamPath(PackageName + ".LOCKED").exists();
    }

    /*FreeForm*/
    public boolean addFloatItItem() {
        boolean forceFloatIt = false;
        if (!FreeForm()) {
            if (freeFormSupport(context)) {
                forceFloatIt = true;
            }
        }
        return forceFloatIt;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public boolean freeFormSupport(Context context) {
        return (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_FREEFORM_WINDOW_MANAGEMENT)) ||
                (Settings.Global.getInt(context.getContentResolver(), "enable_freeform_support", 0) != 0) &&
                        (Settings.Global.getInt(context.getContentResolver(), "force_resizable_activities", 0) != 0);
    }

    public String getWindowingModeMethodName() {
        if (returnAPI() >= 28)
            return "setLaunchWindowingMode";
        else
            return "setLaunchStackId";
    }

    private int getFreeformWindowModeId() {
        if (returnAPI() >= 28) {
            return WindowMode.WINDOWING_MODE_FREEFORM;
        } else {
            return WindowMode.FREEFORM_WORKSPACE_STACK_ID;
        }
    }

    /*Open Functions*/
    public void openPrimaryApplications(int orderNumber, String className, String packageName, int xPosition, int yPosition, int HW) {
        String PackageName = packageName;
        String ClassName = className;
        if (BuildConfig.DEBUG) {
            System.out.println("#" + packageName + " | " + className);
        }
        int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
        int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
        int timeMiliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
        String lastUsed = String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMiliSecond);
        SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context, sqLiteIntelligentServiceFileName(), 1);

        File sqLiteFile = databaseIntelligentService();
        if (!sqLiteFile.exists()) {
            sqLiteDataBaseIntelligentServices.insertAppInfo(
                    String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                    PackageName,
                    ClassName,
                    0,
                    0
            );
        } else {
            try {
                Cursor cursor = sqLiteDataBaseIntelligentServices.getAppInfoDataFromOrderNumber(orderNumber);
                cursor.moveToFirst();
                int previousCounter = cursor.getInt(cursor.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER));
                sqLiteDataBaseIntelligentServices.updateAppInfoFromNumber(
                        String.valueOf(orderNumber),
                        previousCounter + 1,
                        Integer.parseInt(lastUsed)
                );
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                sqLiteDataBaseIntelligentServices.insertAppInfo(
                        String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                        PackageName,
                        ClassName,
                        0,
                        0
                );
            }
        }
        sqLiteDataBaseIntelligentServices.close();

        int[] positionXY = new int[2];
        positionXY[0] = xPosition;
        positionXY[1] = yPosition;
        if (BuildConfig.DEBUG) {
            System.out.println("X Position *** " + positionXY[0]);
            System.out.println("Y Position *** " + positionXY[1]);
        }
        appsLaunchPad(PackageName, ClassName, positionXY[0], positionXY[1], HW, FreeForm());
    }

    public void openApplicationFromActivity(String packageName, String className) {
        if (BuildConfig.DEBUG) {
            System.out.println("#" + packageName + " | " + className);
        }
        if (appInstalledOrNot(packageName)) {
            try {
                ComponentName componentName = new ComponentName(packageName, className);
                ActivityInfo activityInfo = context.getPackageManager().getActivityInfo(componentName, 0);
                Toast.makeText(context,
                        activityInfo.loadLabel(context.getPackageManager()), Toast.LENGTH_SHORT).show();

                Intent openActivity = new Intent();
                openActivity.setClassName(packageName, className);
                openActivity.setFlags(
                        Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT |
                                Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                activity.startActivity(openActivity);
            } catch (Exception classException) {
                classException.printStackTrace();
                try {
                    Intent openApp = context.getPackageManager().getLaunchIntentForPackage(packageName);
                    openApp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(openApp);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, context.getString(R.string.not_install), Toast.LENGTH_LONG).show();
                    Intent playStore = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(context.getString(R.string.play_store_link) + packageName));
                    playStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(playStore);
                }
            }
        } else {
            Toast.makeText(context, context.getString(R.string.not_install), Toast.LENGTH_LONG).show();
            Intent playStore = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context.getString(R.string.play_store_link) + packageName));
            playStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(playStore);
        }
    }

    public void openApplication(String packageName) {
        if (BuildConfig.DEBUG) {
            System.out.println("#" + packageName);
        }
        try {
            Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, context.getString(R.string.not_install), Toast.LENGTH_LONG).show();
            Intent playStore = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context.getString(R.string.play_store_link) + packageName));
            playStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(playStore);
        }
    }

    public void openApplicationFreeForm(String PackageName, String ClassName/*, int leftPositionX*//*X*//*, int rightPositionX, int topPositionY*//*Y*//*, int bottomPositionY*/) {
        //Enable Developer Option & Turn ON 'Force Activities to be Resizable'
        //adb shell settings put global enable_freeform_support 1
        //adb shell settings put global force_resizable_activities 1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (freeFormSupport(context) && FreeForm()) {
                if (returnAPI() < 28) {
                    try {
                        Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                        homeScreen.addCategory(Intent.CATEGORY_HOME);
                        homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(homeScreen);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ActivityOptions activityOptions = ActivityOptions.makeBasic();
                        try {
                            Method method = ActivityOptions.class.getMethod(getWindowingModeMethodName(), int.class);
                            method.invoke(activityOptions, getFreeformWindowModeId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        activityOptions.setLaunchBounds(
                                new Rect(
                                        displayX() / 4,
                                        (displayX() / 2),
                                        displayY() / 4,
                                        (displayY() / 2))
                        );

                        Intent openAlias = new Intent();
                        openAlias.setClassName(PackageName, ClassName);
                        openAlias.setFlags(
                                Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT |
                                        Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(openAlias, activityOptions.toBundle());
                    }
                }, 1000);
            } else {
                Toast(context.getString(R.string.notEnabledFloatIt), Gravity.TOP);
            }
        }
    }

    public void appsLaunchPad(String packageName, String className, int positionX, int positionY, int HW, boolean floatIt) {
        Intent intent = new Intent(context, LaunchPad.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("packageName", packageName);
        intent.putExtra("className", className);
        intent.putExtra("positionX", positionX);
        intent.putExtra("positionY", positionY);
        intent.putExtra("HW", HW);
        intent.putExtra("floatIt", floatIt);
        context.startActivity(intent);
    }

    /*File Functions*/
    public void saveBitmapIcon(String fileName, Bitmap bitmapToSave) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void saveBitmapIcon(String fileName, Drawable drawableToSave) {
        FileOutputStream fileOutputStream = null;
        try {
            Bitmap bitmapToSave = drawableToBitmap(drawableToSave);
            fileOutputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void saveFileEmpty(String fileName) {
        try {
            FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_PRIVATE);

            fOut.flush();
            fOut.close();
        } catch (Exception e) {

        }
    }

    public void saveFile(String fileName, String content) {
        try {
            FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fOut.write((content).getBytes());

            fOut.flush();
            fOut.close();
        } catch (Exception e) {
        }
    }

    public void saveFileAppendLine(String fileName, String content) {
        try {
            FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_APPEND);
            fOut.write((content + "\n").getBytes());

            fOut.flush();
            fOut.close();
        } catch (Exception e) {
        }
    }

    public String[] readFileLine(String fileName) {
        String[] contentLine = null;
        if (context.getFileStreamPath(fileName).exists()) {
            try {
                FileInputStream fin = new FileInputStream(context.getFileStreamPath(fileName));
                BufferedReader myDIS = new BufferedReader(new InputStreamReader(fin));

                int count = countLine(fileName);
                contentLine = new String[count];
                String line = "";
                int i = 0;
                while ((line = myDIS.readLine()) != null) {
                    contentLine[i] = line;
                    i++;
                }
            } catch (Exception e) {
            }
        }
        return contentLine;
    }

    public String readFile(String fileName) {
        String temp = "0";

        File G = context.getFileStreamPath(fileName);
        if (!G.exists()) {
            temp = "0";
        } else {
            try {
                FileInputStream fin = context.openFileInput(fileName);
                BufferedReader br = new BufferedReader(new InputStreamReader(fin, "UTF-8"), 1024);

                int c;
                temp = "";
                while ((c = br.read()) != -1) {
                    temp = temp + Character.toString((char) c);
                }
            } catch (Exception e) {
            }
        }

        return temp;
    }

    public int countLine(String fileName) {
        int nLines = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(context.getFileStreamPath(fileName)));
            while (reader.readLine() != null) {
                nLines++;
            }
            reader.close();
        } catch (Exception e) {
        }
        return nLines;
    }

    public void removeLine(String fileName, String lineToRemove) {
        try {
            FileInputStream fin = context.openFileInput(fileName);
            BufferedReader myDIS = new BufferedReader(new InputStreamReader(fin));
            OutputStreamWriter fOut = new OutputStreamWriter(context.openFileOutput(fileName + ".tmp", Context.MODE_APPEND));

            String tmp = "";
            while ((tmp = myDIS.readLine()) != null) {
                if (!tmp.trim().equals(lineToRemove)) {
                    fOut.write(tmp);
                    fOut.write("\n");
                }
            }

            fOut.close();
            myDIS.close();
            fin.close();

            File tmpD = context.getFileStreamPath(fileName + ".tmp");
            File New = context.getFileStreamPath(fileName);

            if (tmpD.isFile()) {
            }
            context.deleteFile(fileName);
            tmpD.renameTo(New);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
        }
    }

    public void savePreference(String PreferenceName, String KEY, String VALUE) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putString(KEY, VALUE);
        editorSharedPreferences.apply();
    }

    public void savePreference(String PreferenceName, String KEY, int VALUE) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putInt(KEY, VALUE);
        editorSharedPreferences.apply();
    }

    public void savePreference(String PreferenceName, String KEY, boolean VALUE) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putBoolean(KEY, VALUE);
        editorSharedPreferences.apply();
    }

    public void saveDefaultPreference(String KEY, int VALUE) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putInt(KEY, VALUE);
        editorSharedPreferences.apply();
    }

    public void saveDefaultPreference(String KEY, String VALUE) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putString(KEY, VALUE);
        editorSharedPreferences.apply();
    }

    public void saveDefaultPreference(String KEY, boolean VALUE) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editorSharedPreferences = sharedPreferences.edit();
        editorSharedPreferences.putBoolean(KEY, VALUE);
        editorSharedPreferences.apply();
    }

    public String readPreference(String PreferenceName, String KEY, String defaultVALUE) {
        return context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE).getString(KEY, defaultVALUE);
    }

    public int readPreference(String PreferenceName, String KEY, int defaultVALUE) {
        return context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE).getInt(KEY, defaultVALUE);
    }

    public boolean readPreference(String PreferenceName, String KEY, boolean defaultVALUE) {
        return context.getSharedPreferences(PreferenceName, Context.MODE_PRIVATE).getBoolean(KEY, defaultVALUE);
    }

    public int readDefaultPreference(String KEY, int defaultVALUE) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(KEY, defaultVALUE);
    }

    public String readDefaultPreference(String KEY, String defaultVALUE) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY, defaultVALUE);
    }

    public boolean readDefaultPreference(String KEY, boolean defaultVALUE) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(KEY, defaultVALUE);
    }

    public void deleteDatabase(String databaseName) {
        context.deleteDatabase(databaseName + ".db");
    }

    /*Checkpoint Dialogue Functions*/
    public void DialogueLicense(final Activity activity) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.license_title)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.license_msg)));
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton(context.getString(R.string.buy), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent r = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_link) + context.getPackageName()));
                r.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(r);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        uninstallApp(context.getPackageName());
                    }
                }, 2333);
            }
        });
        alertDialog.setNegativeButton(context.getString(R.string.free), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent r = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_link) + context.getPackageName().replace(".PRO", "")));
                r.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(r);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        uninstallApp(context.getPackageName());
                    }
                }, 2333);
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.contact), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                //dialog.dismiss();
                String[] contactOption = new String[]{
                        "Send an Email",
                        "Contact via Forum"};
                AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
                if (LightDark()) {
                    builder = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
                } else {
                    builder = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
                }
                builder.setTitle(context.getString(R.string.supportTitle));
                builder.setSingleChoiceItems(contactOption, 0, null);
                builder.setCancelable(false);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        if (selectedPosition == 0) {
                            String textMsg = "\n\n\n\n\n"
                                    + getDeviceName() + " | " + "API " + Build.VERSION.SDK_INT + " | " + getCountryIso().toUpperCase();
                            Intent email = new Intent(Intent.ACTION_SEND);
                            email.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.support)});
                            email.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.feedback_tag) + " [" + appVersionName(context.getPackageName()) + "] ");
                            email.putExtra(Intent.EXTRA_TEXT, textMsg);
                            email.setType("message/*");
                            email.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(Intent.createChooser(email, context.getString(R.string.feedback_tag)));
                        }
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        activity.finish();
                    }
                });
                builder.show();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        try {
            alertDialog.show();
        } catch (Exception e) {
            activity.finish();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    context.startActivity(context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }, 300);
        }
    }

    public void ChangeLog(Activity activity, boolean showChangeLog) {
        if (returnAPI() > 22) {
            try {
                AlertDialog.Builder alertDialog = null;
                if (LightDark()) {
                    alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
                } else {
                    alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
                }
                alertDialog.setTitle(Html.fromHtml(context.getString(R.string.whatsnew)) + " | " + "βeta Stage");
                alertDialog.setMessage(Html.fromHtml(context.getString(R.string.changelog)));

                alertDialog.setIcon(R.drawable.ic_launcher);
                alertDialog.setCancelable(true);
                alertDialog.setPositiveButton(context.getString(R.string.like), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.link_facebook_app)))
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });
                alertDialog.setNeutralButton(context.getString(R.string.shareit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_link) + context.getPackageName()))
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });
                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        saveFile(".Updated", String.valueOf(appVersionCode(context.getPackageName())));
                        dialog.dismiss();
                    }
                });

                if (showChangeLog == true) {
                    alertDialog.show();
                } else if (!context.getFileStreamPath(".Updated").exists()) {
                    alertDialog.show();
                } else {
                    if (appVersionCode(context.getPackageName()) > Integer.parseInt(readFile(".Updated"))) {
                        alertDialog.show();

                        if (!BuildConfig.DEBUG && networkConnection() && readPreference(".BETA", "isBetaTester", false)) {
                            FirebaseAuth.getInstance().addAuthStateListener(
                                    new FirebaseAuth.AuthStateListener() {
                                        @Override
                                        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                            FirebaseUser user = firebaseAuth.getCurrentUser();
                                            if (user == null) {
                                                savePreference(".BETA", "testerEmail", null);

                                            } else {
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            File betaFile = new File("/data/data/" + context.getPackageName() + "/shared_prefs/.BETA.xml");
                                                            Uri uriBetaFile = Uri.fromFile(betaFile);
                                                            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

                                                            StorageReference storageReference = firebaseStorage.getReference("/betaTesters/" + "API" + returnAPI() + "/" +
                                                                    readPreference(".BETA", "testerEmail", null));
                                                            UploadTask uploadTask = storageReference.putFile(uriBetaFile);

                                                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception exception) {
                                                                    exception.printStackTrace();
                                                                }
                                                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                                @Override
                                                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                                    System.out.println("Firebase Activities Done Successfully");
                                                                }
                                                            });

                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }, 333);
                                            }
                                        }
                                    }
                            );
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void upcomingChangeLog(Activity activity, String updateInfo, String versionCode) {
        if (returnAPI() > 22) {
            AlertDialog.Builder alertDialog = null;
            if (LightDark()) {
                alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
            } else if (!LightDark()) {
                alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
            }
            alertDialog.setTitle(Html.fromHtml("<small>" + context.getString(R.string.whatsnew) + " | " + versionCode + "</small>"));
            alertDialog.setMessage(Html.fromHtml(updateInfo));

            LayerDrawable layerDrawableNewUpdate = (LayerDrawable) context.getDrawable(R.drawable.ic_update);
            BitmapDrawable gradientDrawableNewUpdate = (BitmapDrawable) layerDrawableNewUpdate.findDrawableByLayerId(R.id.ic_launcher_back_layer);
            gradientDrawableNewUpdate.setTint(PublicVariable.primaryColor);

            alertDialog.setIcon(layerDrawableNewUpdate);
            alertDialog.setCancelable(true);
            alertDialog.setPositiveButton(context.getString(R.string.like), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.link_facebook_app)))
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
            alertDialog.setNeutralButton(context.getString(R.string.newupdate), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    dialog.dismiss();
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_link) + context.getPackageName()))
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    Toast(context.getString(R.string.rate), Gravity.BOTTOM);
                }
            });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    public void UsageAccess(final Activity activity) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(context.getString(R.string.smartTitle));
        alertDialog.setMessage(Html.fromHtml(context.getString(R.string.smartPermission)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //activity.finish();
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                Configurations.alertDialogueShow = false;
                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //activity.finish();
                Configurations.alertDialogueShow = false;
                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //activity.finish();
                Configurations.alertDialogueShow = false;
                dialog.dismiss();
            }
        });
        try {
            alertDialog.show();
            Configurations.alertDialogueShow = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UsageAccess(Activity activity, final SwitchPreference switchPreference) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(context.getString(R.string.smartTitle));
        alertDialog.setMessage(Html.fromHtml(context.getString(R.string.smartPermission)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                switchPreference.setChecked(false);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void AccessibilityService(Activity activity, final SwitchPreference switchPreference) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.observeTitle)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.observeDesc)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                try {
                    switchPreference.setChecked(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        alertDialog.show();
    }

    public void AccessibilityService(Activity activity) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.observeTitle)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.observeDesc)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alertDialog.show();
    }

    public void AccessibilityServiceEntry(final Activity activity) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.observeTitle)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.observeDescEntry)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                savePreference("InteractionObserver", "Later", true);

                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alertDialog.show();
        Toast(context.getString(R.string.wait), Gravity.BOTTOM);
    }

    public void NotificationAccessService(Activity activity, final SwitchPreference switchPreference) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.notificationTitle)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.notificationDesc)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent notification = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                notification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(notification);

                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                try {
                    switchPreference.setChecked(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        alertDialog.show();
    }

    public void NotificationAccessService(Activity activity) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.notificationTitle)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.notificationDesc)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent notification = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                notification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(notification);

                dialog.dismiss();
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alertDialog.show();
    }

    public AlertDialog RuntimePermissions(final Activity activity, boolean ShowDismiss) {
        context = activity.getApplicationContext();
        AlertDialog.Builder alertDialog = null;
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(context.getString(R.string.permissionsTitle));
        alertDialog.setMessage(Html.fromHtml(context.getString(R.string.permissionsDesc)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                savePreference(".Configurations", "Permissions", true);
                String[] Permissions = new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CHANGE_NETWORK_STATE,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CHANGE_WIFI_STATE,
                        Manifest.permission.RECEIVE_BOOT_COMPLETED,
                        Manifest.permission.GET_ACCOUNTS
                };
                activity.requestPermissions(Permissions, 777);
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                        || context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    savePreference(".Configurations", "Permissions", false);
                    activity.finish();
                    return;
                }
                dialog.dismiss();
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                        || context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    savePreference(".Configurations", "Permissions", false);
                }
                dialog.dismiss();
            }
        });
        if (ShowDismiss) {
            alertDialog.show();
        } else {
            alertDialog.setMessage(Html.fromHtml("done!"));
            alertDialog.show().cancel();
        }
        return alertDialog.create();
    }

    public void FreeFormInformation(final Activity activity, final SwitchPreference switchPreference) {
        context = activity.getApplicationContext();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        if (LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            alertDialog = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        alertDialog.setTitle(Html.fromHtml(context.getResources().getString(R.string.freeFormTitle)));
        alertDialog.setMessage(Html.fromHtml(context.getResources().getString(R.string.freeFormInfo)));
        alertDialog.setIcon(context.getDrawable(R.drawable.ic_launcher));
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton(context.getString(R.string.grantFreeForm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                switchPreference.setChecked(true);
            }
        });
        alertDialog.setNeutralButton(context.getString(R.string.contactSupport), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                switchPreference.setChecked(false);
                ContactSupport(activity);
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (freeFormSupport(context)) {
                    switchPreference.setChecked(true);
                } else {
                    switchPreference.setChecked(false);
                }
            }
        });
        alertDialog.show();
    }

    public void ContactSupport(final Activity activity) {
        String[] contactOption = new String[]{
                "Send an Email",
                "Send a Message",
                "Contact via Forum",
                "Different Contact Options",
                "Join Beta Program",
                "Rate & Write Review"};
        AlertDialog.Builder builder = null;
        if (LightDark()) {
            builder = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Light);
        } else if (!LightDark()) {
            builder = new AlertDialog.Builder(activity, R.style.GeeksEmpire_Dialogue_Dark);
        }
        LayerDrawable drawSupport = (LayerDrawable) context.getResources().getDrawable(R.drawable.draw_support);
        GradientDrawable backSupport = (GradientDrawable) drawSupport.findDrawableByLayerId(R.id.backtemp);
        backSupport.setColor(PublicVariable.primaryColorOpposite);
        builder.setIcon(drawSupport);
        builder.setTitle(context.getString(R.string.supportCategory));
        builder.setSingleChoiceItems(contactOption, 0, null);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                if (selectedPosition == 0) {
                    String textMsg = "\n\n\n\n\n"
                            + "[Essential Information]" + "\n"
                            + getDeviceName() + " | " + "API " + Build.VERSION.SDK_INT + " | " + getCountryIso().toUpperCase();
                    Intent email = new Intent(Intent.ACTION_SEND);
                    email.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.support)});
                    email.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.feedback_tag) + " [" + appVersionName(context.getPackageName()) + "] ");
                    email.putExtra(Intent.EXTRA_TEXT, textMsg);
                    email.setType("text/*");
                    email.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(Intent.createChooser(email, context.getString(R.string.feedback_tag)));
                } else if (selectedPosition == 1) {
                    Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.link_facebook_app)));
                    activity.startActivity(a);
                } else if (selectedPosition == 2) {
                    Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.link_xda)));
                    activity.startActivity(a);
                } else if (selectedPosition == 3) {
                    activity.startService(new Intent(context, SharingService.class));
                } else if (selectedPosition == 4) {
                    Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.link_alpha) + context.getPackageName()));
                    activity.startActivity(a);

                    Toast(context.getResources().getString(R.string.alphaTitle), Gravity.BOTTOM);
                } else if (selectedPosition == 5) {
                    Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_link) + context.getPackageName()));
                    activity.startActivity(a);

                    Toast(context.getResources().getString(R.string.alphaTitle), Gravity.BOTTOM);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.show();
    }

    /*Object Tags Functions*/
    public String getOrderNumber(Object tagObject) {
        return ((String[]) tagObject)[0];
    }

    public String getPackageName(Object tagObject) {
        return ((String[]) tagObject)[1];
    }

    public String getClassName(Object tagObject) {
        return ((String[]) tagObject)[2];
    }

    /*Uninstall Functions*/
    public void uninstallApp(String packageName) {
        Uri packageUri = Uri.parse("package:" + packageName);
        Intent uninstallIntent =
                new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
        uninstallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(uninstallIntent);
    }

    /*SqLite Checkpoint Functions*/
    public String sqLiteIntelligentServiceFileName() {
        String DATABASE_FILE_NAME = null;
        int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
        if (timeMinutes < 30) {
            //13001330
            DATABASE_FILE_NAME = String.valueOf(timeHours) + String.valueOf(00) + String.valueOf(timeHours) + String.valueOf(30);
        } else if (timeMinutes >= 30) {
            //13301359
            DATABASE_FILE_NAME = String.valueOf(timeHours) + String.valueOf(30) + String.valueOf(timeHours) + String.valueOf(59);
        }
        DATABASE_FILE_NAME = DATABASE_FILE_NAME + "_" + networkSSID() + "_" + activityType();
        return DATABASE_FILE_NAME;
    }

    public File databaseIntelligentService() {
        return (new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
                + File.separator +
                ".IntelligentServicesSqLiteDataBase"
                + File.separator + sqLiteIntelligentServiceFileName() + ".db"));
    }

    public List<String> intelligentServicesFiles() {
        File directoryIntelligentService = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
                + File.separator +
                ".IntelligentServicesSqLiteDataBase");
        List<String> filesIntelligentServiceDatabase = new ArrayList<String>();
        File[] filesIntelligentService = directoryIntelligentService.listFiles();
        for (File aFile : filesIntelligentService) {
            if (!aFile.getName().contains("journal")) {
                filesIntelligentServiceDatabase.add(aFile.getName().replace(".db", ""));
            }
        }
        return filesIntelligentServiceDatabase;
    }

    public boolean databaseAvailable(String databaseName) {
        return context.getDatabasePath(databaseName + ".db").exists();
    }

    public int databaseAppInfoCountRow() {
        SqLiteDataBaseAppInfo sqLiteDataBaseAppInfo = new SqLiteDataBaseAppInfo(context, 1);
        return sqLiteDataBaseAppInfo.numberOfRows();
    }

    public boolean databaseAvailableIntelligentService() {
        return (databaseIntelligentService()).exists();
    }

    public int databaseIntelligentServiceCountRow() {
        SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context, sqLiteIntelligentServiceFileName(), 1);
        return sqLiteDataBaseIntelligentServices.numberOfRows();
    }

    public String activityType() {
        return ActivityRecognitionEnabled() ? readPreference("ActivityRecognition", "Type", "NONE") : "NONE";
    }

    /*Checkpoint Functions*/
    public boolean UsageStatsEnabled() {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOp("android:get_usage_stats", android.os.Process.myUid(), context.getPackageName());
        return (mode == AppOpsManager.MODE_ALLOWED);
    }

    public boolean AccessibilityServiceEnabled() {
        ComponentName expectedComponentName = new ComponentName(context, InteractionObserver.class);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    public boolean NotificationAccess() {
        boolean notificationAccess = false;
        try {
            if ((Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners")).contains(context.getPackageName())) {
                notificationAccess = true;
            } else {
                notificationAccess = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Set<String> packageNames = NotificationManagerCompat.getEnabledListenerPackages(context);
                for (String packageName : packageNames) {
                    if (packageName.contains(context.getPackageName())) {
                        return true;
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return notificationAccess;
    }

    public boolean SettingServiceRunning(Class aClass) {
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : activityManager.getRunningServices(Integer.MAX_VALUE)) {
                if (aClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean ActivityRecognitionEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("activityRecognition", false);
    }

    public boolean FloatingEnabled() {
        boolean overlyDraw = false;
        if (Settings.canDrawOverlays(context)) {
            overlyDraw = true;
        } else {
            overlyDraw = false;
        }
        return overlyDraw;
    }

    public boolean networkConnection() throws Exception {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public String networkSSID() {
        String networkSSID = "OFFLINE";
        try {
            if (networkConnection()) {
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                if (wifiManager.isWifiEnabled()) {
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    if (wifiInfo != null) {
                        NetworkInfo.DetailedState detailedStateOf = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                        if (detailedStateOf == NetworkInfo.DetailedState.CONNECTED || detailedStateOf == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                            networkSSID = wifiInfo.getSSID().replaceAll("\"", "");
                        } else {
                            networkSSID = "LTE";
                        }
                    } else {
                        networkSSID = "LTE";
                    }
                } else {
                    networkSSID = "LTE";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            networkSSID = "OFFLINE";
        }
        return networkSSID;
    }

    public boolean FreeForm() {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("freeForm", false);
    }

    /*Apps Checkpoint Functions*/
    public String appName(String packageName) {
        String Name = null;
        try {
            ApplicationInfo app = context.getPackageManager().getApplicationInfo(packageName, 0);
            Name = context.getPackageManager().getApplicationLabel(app).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Name;
    }

    public String activityLabel(ActivityInfo activityInfo) {
        String Name = context.getString(R.string.app_name);
        try {
            Name = activityInfo.loadLabel(context.getPackageManager()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            Name = appName(activityInfo.packageName);
        }
        return Name;
    }

    public String appVersionName(String packageName) {
        String Version = "0";

        try {
            PackageInfo packInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            Version = packInfo.versionName;
        } catch (Exception e) {
        }

        return Version;
    }

    public int appVersionCode(String packageName) {
        int VersionCode = 0;

        try {
            PackageInfo packInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            VersionCode = packInfo.versionCode;
        } catch (Exception e) {
        }

        return VersionCode;
    }

    public boolean appInstalledOrNot(String packName) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(packName, 0);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        } catch (Exception e) {

            app_installed = false;
        }
        return app_installed;
    }

    public boolean ifSystem(String packageName) {
        boolean ifSystem = false;
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo targetPkgInfo = packageManager.getPackageInfo(
                    packageName, PackageManager.GET_SIGNATURES);
            PackageInfo sys = packageManager.getPackageInfo(
                    "android", PackageManager.GET_SIGNATURES);
            ifSystem = (targetPkgInfo != null && targetPkgInfo.signatures != null && sys.signatures[0]
                    .equals(targetPkgInfo.signatures[0]));
        } catch (PackageManager.NameNotFoundException e) {

            ifSystem = false;
        } catch (Exception e) {
        }
        return ifSystem;
    }

    public boolean ifDefaultLauncher(String packageName) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo defaultLauncher = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        String defaultLauncherStr = defaultLauncher.activityInfo.packageName;
        if (defaultLauncherStr.equals(packageName)) {
            return true;
        }
        return false;
    }

    public boolean canLaunch(String packageName) {
        return (context.getPackageManager().getLaunchIntentForPackage(packageName) != null);
    }

    public int returnAPI() {
        return API;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return CapitalizeFirstChar(model);
        } else {
            return CapitalizeFirstChar(manufacturer) + " " + model;
        }
    }

    private String CapitalizeFirstChar(String text) {
        if (text == null || text.length() == 0) {
            return "";
        }
        char first = text.charAt(0);
        if (Character.isUpperCase(first)) {
            return text;
        } else {
            return Character.toUpperCase(first) + text.substring(1);
        }
    }

    public String getCountryIso() {
        String countryISO = "Undefined";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            countryISO = telephonyManager.getSimCountryIso();
            if (countryISO.length() < 2) {
                countryISO = "Undefined";
            }
        } catch (Exception e) {

            countryISO = "Undefined";
        }
        return countryISO;
    }

    public int countInstalledApplication() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, 0);
        Collections.sort(resolveInfos, new ResolveInfo.DisplayNameComparator(context.getPackageManager()));

        int rowNumber = 0;
        for (ResolveInfo resolveInfo : resolveInfos) {
            try {
                if (context.getPackageManager().getLaunchIntentForPackage(resolveInfo.activityInfo.packageName) != null) {
                    try {
                        rowNumber++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return rowNumber;
    }

    public boolean alwaysReady() {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("alwaysReady", false);
    }

    public int alwaysReadyMode() {
        return alwaysReady() ? Service.START_STICKY : Service.START_NOT_STICKY;
    }

    public int bindServicePriority(String HiLo) {
        int notificationPriority = Integer.MIN_VALUE;
        if (HiLo.equals("High")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationPriority = NotificationManager.IMPORTANCE_HIGH;
            } else {
                notificationPriority = Notification.PRIORITY_HIGH;
            }
        } else if (HiLo.equals("Low")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationPriority = NotificationManager.IMPORTANCE_MIN;
            } else {
                notificationPriority = Notification.PRIORITY_MIN;
            }
        }
        return notificationPriority;
    }

    public void reloadData() {
        try {
            File[] dbFiles = (new File("/data/data/" + context.getPackageName() + "/databases/")).listFiles();
            for (File dbFile : dbFiles) {
                dbFile.delete();
            }

            File[] savedFiles = context.getFileStreamPath("").listFiles();
            for (File aFile : savedFiles) {
                if (!aFile.getName().contains(".LOCK")) {
                    aFile.delete();
                }
            }

            Toast(context.getString(R.string.wait), Gravity.BOTTOM);
            LoadSaveAppInfoIntelligentService(SettingGUIDark.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Color GUI Functions*/
    public void loadSavedColor(/*Light-Dark*/) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(".ThemeColors", Context.MODE_PRIVATE);
        SharedPreferences primeColor = PreferenceManager.getDefaultSharedPreferences(context);
        if (primeColor.getString("LightDark", "3").equals("1")) {//Light
            PublicVariable.primaryColor = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
            PublicVariable.primaryColorOpposite = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
            PublicVariable.dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));
            PublicVariable.colorLightDark = context.getResources().getColor(R.color.light);
            PublicVariable.colorLightDarkOpposite = context.getResources().getColor(R.color.dark);
        } else if (primeColor.getString("LightDark", "3").equals("2")) {//Dark
            PublicVariable.primaryColor = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
            PublicVariable.primaryColorOpposite = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
            PublicVariable.dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));
            PublicVariable.colorLightDark = context.getResources().getColor(R.color.dark);
            PublicVariable.colorLightDarkOpposite = context.getResources().getColor(R.color.light);
        } else if (primeColor.getString("LightDark", "3").equals("3")) {//Dynamic
            if (colorLightDarkWallpaper()) {
                PublicVariable.primaryColor = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
                PublicVariable.primaryColorOpposite = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
                PublicVariable.dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));
                PublicVariable.colorLightDark = context.getResources().getColor(R.color.light);
                PublicVariable.colorLightDarkOpposite = context.getResources().getColor(R.color.dark);
            } else if (!colorLightDarkWallpaper()) {
                PublicVariable.primaryColor = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
                PublicVariable.primaryColorOpposite = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
                PublicVariable.dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));
                PublicVariable.colorLightDark = context.getResources().getColor(R.color.dark);
                PublicVariable.colorLightDarkOpposite = context.getResources().getColor(R.color.light);
            }
        } else if (primeColor.getString("LightDark", "3").equals("4")) {//Automatic
            int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (timeHours >= 18 || timeHours < 6) {//Night
                PublicVariable.primaryColor = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
                PublicVariable.primaryColorOpposite = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
                PublicVariable.dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));
                PublicVariable.colorLightDark = context.getResources().getColor(R.color.dark);
                PublicVariable.colorLightDarkOpposite = context.getResources().getColor(R.color.light);
            } else {
                PublicVariable.primaryColor = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
                PublicVariable.primaryColorOpposite = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
                PublicVariable.dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));
                PublicVariable.colorLightDark = context.getResources().getColor(R.color.light);
                PublicVariable.colorLightDarkOpposite = context.getResources().getColor(R.color.dark);
            }
        }
    }

    public void extractWallpaperColor() {
        int primaryColorLight, primaryColorDark, dominantColor;
        Palette currentColor;
        try {
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
            final Drawable currentWallpaper = wallpaperManager.getDrawable();
            final Bitmap bitmap = ((BitmapDrawable) currentWallpaper).getBitmap();

            if (bitmap != null && !bitmap.isRecycled()) {
                currentColor = Palette.from(bitmap).generate();
            } else {
                Bitmap bitmapTemp = BitmapFactory.decodeResource(context.getResources(), R.drawable.brilliant);
                currentColor = Palette.from(bitmapTemp).generate();
            }

            int defaultColor = context.getResources().getColor(R.color.default_color);

            primaryColorLight = currentColor.getVibrantColor(defaultColor);
            primaryColorDark = currentColor.getDarkMutedColor(defaultColor);
            dominantColor = currentColor.getDominantColor(defaultColor);
        } catch (Exception e) {
            e.printStackTrace();

            primaryColorLight = context.getResources().getColor(R.color.default_color);
            primaryColorDark = context.getResources().getColor(R.color.default_color);
            dominantColor = context.getResources().getColor(R.color.default_color);
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(".ThemeColors", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("primaryColorLight", primaryColorLight);
        editor.putInt("primaryColorDark", primaryColorDark);
        editor.putInt("dominantColor", dominantColor);
        editor.apply();
    }

    public int extractVibrantColor(Drawable drawable) {
        int VibrantColor = context.getResources().getColor(R.color.default_color);
        Bitmap bitmap = null;
        if (returnAPI() >= 26) {
            if (drawable instanceof VectorDrawable) {
                bitmap = drawableToBitmap(drawable);
            } else if (drawable instanceof AdaptiveIconDrawable) {
                try {
                    bitmap = ((BitmapDrawable) ((AdaptiveIconDrawable) drawable).getBackground()).getBitmap();
                } catch (Exception e) {
                    try {
                        bitmap = ((BitmapDrawable) ((AdaptiveIconDrawable) drawable).getForeground()).getBitmap();
                    } catch (Exception e1) {
                        bitmap = drawableToBitmap(drawable);
                    }
                }
            } else {
                bitmap = drawableToBitmap(drawable);
            }
        } else {
            bitmap = drawableToBitmap(drawable);
        }
        Palette currentColor;
        try {
            if (bitmap != null && !bitmap.isRecycled()) {
                currentColor = Palette.from(bitmap).generate();

                int defaultColor = context.getResources().getColor(R.color.default_color);
                VibrantColor = currentColor.getVibrantColor(defaultColor);
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (bitmap != null && !bitmap.isRecycled()) {
                    currentColor = Palette.from(bitmap).generate();

                    int defaultColor = context.getResources().getColor(R.color.default_color);
                    VibrantColor = currentColor.getMutedColor(defaultColor);
                }
            } catch (Exception e1) {

            }
        }
        return VibrantColor;
    }

    public int extractDominantColor(Drawable drawable) {
        int VibrantColor = context.getResources().getColor(R.color.default_color);
        Bitmap bitmap = null;
        if (returnAPI() >= 26) {
            if (drawable instanceof VectorDrawable) {
                bitmap = drawableToBitmap(drawable);
            } else if (drawable instanceof AdaptiveIconDrawable) {
                try {
                    bitmap = ((BitmapDrawable) ((AdaptiveIconDrawable) drawable).getBackground()).getBitmap();
                } catch (Exception e) {
                    try {
                        bitmap = ((BitmapDrawable) ((AdaptiveIconDrawable) drawable).getForeground()).getBitmap();
                    } catch (Exception e1) {
                        bitmap = drawableToBitmap(drawable);
                    }
                }
            } else {
                bitmap = drawableToBitmap(drawable);
            }
        } else {
            bitmap = drawableToBitmap(drawable);
        }
        Palette currentColor;
        try {
            if (bitmap != null && !bitmap.isRecycled()) {
                currentColor = Palette.from(bitmap).generate();

                int defaultColor = context.getResources().getColor(R.color.default_color);
                VibrantColor = currentColor.getDominantColor(defaultColor);
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (bitmap != null && !bitmap.isRecycled()) {
                    currentColor = Palette.from(bitmap).generate();

                    int defaultColor = context.getResources().getColor(R.color.default_color);
                    VibrantColor = currentColor.getMutedColor(defaultColor);
                }
            } catch (Exception e1) {

            }
        }
        return VibrantColor;
    }

    public Bitmap brightenBitmap(Bitmap bitmap) {
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Color.RED);
        ColorFilter filter = new LightingColorFilter(0xFFFFFFFF, 0x00222222); // lighten
        //ColorFilter filter = new LightingColorFilter(0xFF7F7F7F, 0x00000000);    // darken
        paint.setColorFilter(filter);
        canvas.drawBitmap(bitmap, new Matrix(), paint);
        return bitmap;
    }

    public Bitmap darkenBitmap(Bitmap bitmap) {
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Color.RED);
        //ColorFilter filter = new LightingColorFilter(0xFFFFFFFF , 0x00222222); // lighten
        ColorFilter filter = new LightingColorFilter(0xFF7F7F7F, 0x00000000);    // darken
        paint.setColorFilter(filter);
        canvas.drawBitmap(bitmap, new Matrix(), paint);
        return bitmap;
    }

    public int manipulateColor(int color, float aFactor) {
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * aFactor);
        int g = Math.round(Color.green(color) * aFactor);
        int b = Math.round(Color.blue(color) * aFactor);
        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255));
    }

    public int mixColors(int color1, int color2, float ratio /*0 -- 1*/) {
        /*ratio = 1 >> color1*/
        /*ratio = 0 >> color2*/
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

    public int setColorAlpha(int color, float alphaValue /*1 -- 255*/) {
        int alpha = Math.round(Color.alpha(color) * alphaValue);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    /*Shaping Functions*/
    public Drawable appIcon(ActivityInfo activityInfo) {
        Drawable icon = null;
        try {
            icon = activityInfo.loadIcon(context.getPackageManager());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (icon == null) {
                try {
                    icon = context.getPackageManager().getDefaultActivityIcon();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return icon;
    }

    public Drawable appIcon(String packageName) {
        Drawable icon = null;
        try {
            icon = context.getPackageManager().getApplicationIcon(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (icon == null) {
                try {
                    icon = context.getPackageManager().getDefaultActivityIcon();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return icon;
    }

    public int shapesImageId() {
        int shapesImageId = 0;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        switch (sharedPreferences.getInt("iconShape", 0)) {
            case 1:
                shapesImageId = 1;
                break;
            case 2:
                shapesImageId = 2;
                break;
            case 3:
                shapesImageId = 3;
                break;
            case 4:
                shapesImageId = 4;
                break;
            case 0:
                shapesImageId = 0;
                break;
        }
        return shapesImageId;
    }

    public Drawable shapesDrawables() {
        Drawable shapeDrawable = null;
        switch (shapesImageId()) {
            case 1:
                shapeDrawable = context.getDrawable(R.drawable.droplet_icon).mutate();
                break;
            case 2:
                shapeDrawable = context.getDrawable(R.drawable.circle_icon).mutate();
                break;
            case 3:
                shapeDrawable = context.getDrawable(R.drawable.square_icon).mutate();
                break;
            case 4:
                shapeDrawable = context.getDrawable(R.drawable.squircle_icon).mutate();
                break;
            case 0:
                shapeDrawable = null;
                break;
        }
        return shapeDrawable;
    }

    public ShapesImage initShapesImage(ViewGroup viewGroup, int viewId) {
        ShapesImage shapesImage = (ShapesImage) viewGroup.findViewById(viewId);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        switch (sharedPreferences.getInt("iconShape", 0)) {
            case 1:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.droplet_icon));
                break;
            case 2:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.circle_icon));
                break;
            case 3:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.square_icon));
                break;
            case 4:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.squircle_icon));
                break;
            case 0:
                shapesImage.setShapeDrawable(null);
                break;
        }
        return shapesImage;
    }

    public ShapesImage initShapesImage(View view, int viewId) {
        ShapesImage shapesImage = (ShapesImage) view.findViewById(viewId);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        switch (sharedPreferences.getInt("iconShape", 0)) {
            case 1:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.droplet_icon));
                break;
            case 2:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.circle_icon));
                break;
            case 3:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.square_icon));
                break;
            case 4:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.squircle_icon));
                break;
            case 0:
                shapesImage.setShapeDrawable(null);
                break;
        }
        return shapesImage;
    }

    public ShapesImage initShapesImage(Activity activity, int viewId) {
        ShapesImage shapesImage = (ShapesImage) activity.findViewById(viewId);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        switch (sharedPreferences.getInt("iconShape", 0)) {
            case 1:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.droplet_icon));
                break;
            case 2:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.circle_icon));
                break;
            case 3:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.square_icon));
                break;
            case 4:
                shapesImage.setShapeDrawable(context.getDrawable(R.drawable.squircle_icon));
                break;
            case 0:
                shapesImage.setShapeDrawable(null);
                break;
        }
        return shapesImage;
    }

    public Drawable shapedNotificationUser(Drawable drawable) {
        Drawable drawableBack = new ColorDrawable(extractVibrantColor(drawable));
        Drawable drawableFront = drawable;
        LayerDrawable shapedDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
        shapedDrawable.setPaddingMode(LayerDrawable.PADDING_MODE_NEST);
        shapedDrawable.setLayerInset(1, 7, 7, 7, 7);
        return shapedDrawable;
    }

    public Drawable shapedAppIcon(ActivityInfo activityInfo) {
        Drawable appIconDrawable = null;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (sharedPreferences.getInt("iconShape", 0) == 1
                || sharedPreferences.getInt("iconShape", 0) == 2
                || sharedPreferences.getInt("iconShape", 0) == 3
                || sharedPreferences.getInt("iconShape", 0) == 4) {
            Drawable drawableBack = null;
            Drawable drawableFront = null;
            LayerDrawable layerDrawable = null;
            if (returnAPI() >= 26) {
                AdaptiveIconDrawable adaptiveIconDrawable = null;
                try {
                    Drawable tempAppIcon = appIcon(activityInfo);
                    if (tempAppIcon instanceof AdaptiveIconDrawable) {
                        adaptiveIconDrawable = (AdaptiveIconDrawable) tempAppIcon;
                        drawableBack = adaptiveIconDrawable.getBackground();
                        drawableFront = adaptiveIconDrawable.getForeground();
                        layerDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
                        appIconDrawable = layerDrawable;
                    } else {
                        drawableBack = new ColorDrawable(extractDominantColor(tempAppIcon));
                        drawableFront = tempAppIcon;
                        layerDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
                        layerDrawable.setPaddingMode(LayerDrawable.PADDING_MODE_NEST);
                        layerDrawable.setLayerInset(1, 35, 35, 35, 35);
                        appIconDrawable = layerDrawable;
                    }
                } catch (Exception e) {
                }
            } else {
                Drawable tempAppIcon = appIcon(activityInfo);
                drawableBack = new ColorDrawable(extractDominantColor(tempAppIcon));
                drawableFront = tempAppIcon;
                layerDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
                layerDrawable.setPaddingMode(LayerDrawable.PADDING_MODE_NEST);
                layerDrawable.setLayerInset(1, 35, 35, 35, 35);
                appIconDrawable = layerDrawable;
            }
        } else if (sharedPreferences.getInt("iconShape", 0) == 0) {
            appIconDrawable = appIcon(activityInfo);
        }
        return appIconDrawable;
    }

    public Drawable shapedAppIcon(String packageName) {
        Drawable appIconDrawable = null;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (sharedPreferences.getInt("iconShape", 0) == 1
                || sharedPreferences.getInt("iconShape", 0) == 2
                || sharedPreferences.getInt("iconShape", 0) == 3
                || sharedPreferences.getInt("iconShape", 0) == 4) {
            Drawable drawableBack = null;
            Drawable drawableFront = null;
            LayerDrawable layerDrawable = null;
            if (returnAPI() >= 26) {
                AdaptiveIconDrawable adaptiveIconDrawable = null;
                try {
                    Drawable tempAppIcon = appIcon(packageName);
                    if (tempAppIcon instanceof AdaptiveIconDrawable) {
                        adaptiveIconDrawable = (AdaptiveIconDrawable) tempAppIcon;
                        drawableBack = adaptiveIconDrawable.getBackground();
                        drawableFront = adaptiveIconDrawable.getForeground();
                        layerDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
                        appIconDrawable = layerDrawable;
                    } else {
                        drawableBack = new ColorDrawable(extractDominantColor(tempAppIcon));
                        drawableFront = tempAppIcon;
                        layerDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
                        layerDrawable.setPaddingMode(LayerDrawable.PADDING_MODE_NEST);
                        layerDrawable.setLayerInset(1, 35, 35, 35, 35);
                        appIconDrawable = layerDrawable;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Drawable tempAppIcon = appIcon(packageName);
                drawableBack = new ColorDrawable(extractDominantColor(tempAppIcon));
                drawableFront = tempAppIcon;
                layerDrawable = new LayerDrawable(new Drawable[]{drawableBack, drawableFront});
                layerDrawable.setPaddingMode(LayerDrawable.PADDING_MODE_NEST);
                layerDrawable.setLayerInset(1, 35, 35, 35, 35);
                appIconDrawable = layerDrawable;
            }
        } else if (sharedPreferences.getInt("iconShape", 0) == 0) {
            appIconDrawable = appIcon(packageName);
        }
        return appIconDrawable;
    }

    public Bitmap appIconBitmap(ActivityInfo activityInfo) {
        Bitmap bitmap = null;
        try {
            LayerDrawable drawAppShortcuts;
            Drawable drawableIcon = appIcon(activityInfo);
            Drawable shapeTempDrawable = shapesDrawables();
            if (shapeTempDrawable != null) {
                shapeTempDrawable.setTint(extractDominantColor(drawableIcon));
                drawAppShortcuts = new LayerDrawable(new Drawable[]{shapeTempDrawable, drawableIcon});
                drawAppShortcuts.setLayerInset(1, 43, 43, 43, 43);
            } else {
                shapeTempDrawable = new ColorDrawable(Color.TRANSPARENT);
                drawAppShortcuts = new LayerDrawable(new Drawable[]{shapeTempDrawable, drawableIcon});
                drawAppShortcuts.setLayerInset(1, 1, 1, 1, 1);
            }
            bitmap = Bitmap
                    .createBitmap(drawAppShortcuts.getIntrinsicWidth(), drawAppShortcuts.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            drawAppShortcuts.setBounds(0, 0, drawAppShortcuts.getIntrinsicWidth(), drawAppShortcuts.getIntrinsicHeight());
            drawAppShortcuts.draw(new Canvas(bitmap));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap appIconBitmap(String packageName) {
        Bitmap bitmap = null;
        try {
            LayerDrawable drawAppShortcuts;
            Drawable drawableIcon = appIcon(packageName);
            Drawable shapeTempDrawable = shapesDrawables();
            if (shapeTempDrawable != null) {
                shapeTempDrawable.setTint(extractDominantColor(drawableIcon));
                drawAppShortcuts = new LayerDrawable(new Drawable[]{shapeTempDrawable, drawableIcon});
                drawAppShortcuts.setLayerInset(1, 43, 43, 43, 43);
            } else {
                shapeTempDrawable = new ColorDrawable(Color.TRANSPARENT);
                drawAppShortcuts = new LayerDrawable(new Drawable[]{shapeTempDrawable, drawableIcon});
                drawAppShortcuts.setLayerInset(1, 1, 1, 1, 1);
            }
            bitmap = Bitmap
                    .createBitmap(drawAppShortcuts.getIntrinsicWidth(), drawAppShortcuts.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            drawAppShortcuts.setBounds(0, 0, drawAppShortcuts.getIntrinsicWidth(), drawAppShortcuts.getIntrinsicHeight());
            drawAppShortcuts.draw(new Canvas(bitmap));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /*Converters GUI Functions*/
    public Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable instanceof VectorDrawable) {
            VectorDrawable vectorDrawable = (VectorDrawable) drawable;
            bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            vectorDrawable.draw(canvas);
        } else if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                bitmap = bitmapDrawable.getBitmap();
            }
        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;

            bitmap = Bitmap.createBitmap(layerDrawable.getIntrinsicWidth(), layerDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            layerDrawable.draw(canvas);
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        }
        return bitmap;
    }

    public Drawable bitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public byte[] drawableToByte(Drawable drawable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        drawableToBitmap(drawable).compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public Drawable byteToDrawable(byte[] byteImage) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length);
        return bitmapToDrawable(bitmap);
    }

    public float DpToPixel(float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public float PixelToDp(float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public int DpToInteger(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public int columnCount(int itemWidth) {
        return (int) (displayX() / DpToPixel(itemWidth));
    }

    /*Checkpoint GUI Functions*/
    public boolean LightDark() {
        boolean modeLightDark = true;
        SharedPreferences primeColor = PreferenceManager.getDefaultSharedPreferences(context);
        if (primeColor.getString("LightDark", "3").equals("1")) {//Light
            modeLightDark = true;
        } else if (primeColor.getString("LightDark", "3").equals("2")) {//Dark
            modeLightDark = false;
        } else if (primeColor.getString("LightDark", "3").equals("3")) {
            if (colorLightDarkWallpaper()) {//light
                modeLightDark = true;
            } else if (!colorLightDarkWallpaper()) {//dark
                modeLightDark = false;
            }
        } else if (primeColor.getString("LightDark", "4").equals("3")) {
            int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (timeHours >= 18 || timeHours < 6) {//Night
                modeLightDark = false;
            } else {//Day
                modeLightDark = true;
            }
        }
        return modeLightDark;
    }

    public boolean StaticActive() {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("StaticActive", true);
    }

    public int displayX() {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public int displayY() {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public boolean colorLightDarkWallpaper() {
        boolean LightDark = false;
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(".ThemeColors", Context.MODE_PRIVATE);

            int vibrantColor = sharedPreferences.getInt("primaryColorLight", context.getResources().getColor(R.color.default_color));
            int darkMutedColor = sharedPreferences.getInt("primaryColorDark", context.getResources().getColor(R.color.default_color));
            int dominantColor = sharedPreferences.getInt("dominantColor", context.getResources().getColor(R.color.default_color));

            int initMix = mixColors(vibrantColor, darkMutedColor, 0.50f);
            int finalMix = mixColors(dominantColor, initMix, 0.50f);

            double calculateLuminance = ColorUtils.calculateLuminance(dominantColor);
            if (calculateLuminance > 0.50) {//light
                LightDark = true;
            } else if (calculateLuminance <= 0.50) {//dark
                LightDark = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return LightDark;
    }

    public boolean colorBrightDark(int aColor) {
        boolean LightDark = false;

        double calculateLuminance = ColorUtils.calculateLuminance(aColor);
        if (calculateLuminance > 0.50) {//light
            LightDark = true;
        } else if (calculateLuminance <= 0.50) {//dark
            LightDark = false;
        }

        return LightDark;
    }

    public Notification notificationCreator(String titleText, String contentText, int notificationId, int notificationImportance) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder mBuilder = new Notification.Builder(context);
        mBuilder.setContentTitle(Html.fromHtml("<b><font color='" + PublicVariable.primaryColor + "'>" + titleText + "</font></b>"));
        mBuilder.setContentText(Html.fromHtml("<font color='" + PublicVariable.primaryColor + "'>" + contentText + "</font>"));
        mBuilder.setTicker(context.getResources().getString(R.string.app_name));
        mBuilder.setSmallIcon(R.drawable.ic_notification);
        mBuilder.setAutoCancel(true);
        mBuilder.setColor(PublicVariable.primaryColor);
        mBuilder.setPriority(notificationImportance/*Notification.PRIORITY_MIN*/);

        Intent newUpdate = new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.play_store_link) + context.getPackageName()));
        PendingIntent newUpdatePendingIntent = PendingIntent.getActivity(context, 5, newUpdate, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(context.getPackageName(), context.getString(R.string.app_name), notificationImportance/*NotificationManager.IMPORTANCE_MIN*/);
            notificationManager.createNotificationChannel(notificationChannel);
            mBuilder.setChannelId(context.getPackageName());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Notification.Action.Builder builderActionNotification = new Notification.Action.Builder(
                    Icon.createWithResource(context, R.drawable.ic_launcher),
                    context.getString(R.string.rate),
                    newUpdatePendingIntent
            );
            mBuilder.addAction(builderActionNotification.build());
        }
        mBuilder.setContentIntent(newUpdatePendingIntent);
        notificationManager.notify(notificationId, mBuilder.build());
        return mBuilder.build();
    }

    public boolean wallpaperStaticLive() {
        boolean wallpaperMode = false;
        if (WallpaperManager.getInstance(context).getWallpaperInfo() == null) {//static
            wallpaperMode = true;
        } else if (WallpaperManager.getInstance(context).getWallpaperInfo() != null) {//live
            wallpaperMode = false;
        }
        return wallpaperMode;
    }

    public int displaySection(int X, int Y) {
        int section = 1;
        if (X < displayX() / 2 && Y < displayY() / 2) {//top-left
            section = 1;
        } else if (X > displayX() / 2 && Y < displayY() / 2) {//top-right
            section = 2;
        } else if (X < displayX() / 2 && Y > displayY() / 2) {//bottom-left
            section = 3;
        } else if (X > displayX() / 2 && Y > displayY() / 2) {//bottom-right
            section = 4;
        }
        return section;
    }

    /*Checkpoint GUI Right/LeftFunctions*/
    public boolean RightLeft() {
        boolean handed = true;
        String RightLeft = PreferenceManager.getDefaultSharedPreferences(context).getString("RightLeft", "1");
        if (RightLeft.equals("1")) {
            handed = true;
        } else if (RightLeft.equals("2")) {
            handed = false;
        }
        return handed;
    }

    /*Theme GUI Functions*/
    public void setThemeColor(View view) {
        if (wallpaperStaticLive()) {
            setAppThemeBlur(activity, 13);
        }
        view.setBackgroundColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), 180));

        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (LightDark()) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (API >= 26) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            }
        }
        window.setStatusBarColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), 180));
        window.setNavigationBarColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), 180));

    }

    public void setThemeColor(Activity activityToChange, View view) {
        context.sendBroadcast(new Intent(context.getString(R.string.resetTextColor)));
        context.sendBroadcast(new Intent(context.getString(R.string.resetActionColor)));
        TextView textView = (TextView) activityToChange.getLayoutInflater()
                .inflate(R.layout.side_index_item_right, null);
        textView.setTextColor(PublicVariable.colorLightDarkOpposite);
        if (wallpaperStaticLive()) {
            setAppThemeBlur(activityToChange, 13);
        }
        view.setBackgroundColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), 180));

        Window window = activityToChange.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (LightDark()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            }
        } else if (!LightDark()) {
            window.getDecorView().setSystemUiVisibility(0);
        }
        window.setStatusBarColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), 180));
        window.setNavigationBarColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), 180));

    }

    public void setAppThemeBlur(Activity activity, float blurRadius /*0 -- 25*/) {
        boolean blurEffect = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("blurryTheme", true);
        if (blurEffect == true) {
            try {
                WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
                BitmapDrawable wallpaper = (BitmapDrawable) wallpaperManager.getDrawable();

                Bitmap bitmapWallpaper = wallpaper.getBitmap();
                Bitmap inputBitmap = null;
                if (bitmapWallpaper.getWidth() < displayX() || bitmapWallpaper.getHeight() < displayY()) {
                    inputBitmap = Bitmap.createScaledBitmap(bitmapWallpaper, displayX(), displayY(), false);
                } else {
                    inputBitmap = Bitmap.createBitmap(
                            bitmapWallpaper,
                            (bitmapWallpaper.getWidth() / 2) - (displayX() / 2),
                            (bitmapWallpaper.getHeight() / 2) - (displayY() / 2),
                            displayX(),
                            displayY()
                    );
                }
                Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

                RenderScript rs = RenderScript.create(context);
                ScriptIntrinsicBlur intrinsicBlur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
                Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);

                intrinsicBlur.setRadius(blurRadius);
                intrinsicBlur.setInput(tmpIn);
                intrinsicBlur.forEach(tmpOut);
                tmpOut.copyTo(outputBitmap);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), outputBitmap);
                activity.getWindow().getDecorView().setBackground(bitmapDrawable);
            } catch (Exception e) {

            }
        } else if (blurEffect == false) {
        }
    }

    public void setThemeColorPreferences(Activity activity, View view, String title, String subTitle) {
        try {
            if (wallpaperStaticLive()) {
                WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
                Drawable wallpaper = wallpaperManager.getDrawable();
                activity.getWindow().getDecorView().setBackground(wallpaper);

                setAppThemeBlur(activity, 25);
            }
            view.setBackgroundColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), wallpaperStaticLive() ? 180 : 100));

            ActionBar actionBar = activity.getActionBar();
            actionBar.setBackgroundDrawable(new ColorDrawable(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.77f), wallpaperStaticLive() ? 130 : 40)));
            if (LightDark()) {
                actionBar.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.dark) + "'>" + title + "</font>"));
                actionBar.setSubtitle(Html.fromHtml("<small><font color='" + context.getResources().getColor(R.color.dark) + "'>" + subTitle + "</font></small>"));
            } else {
                actionBar.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.light) + "'>" + title + "</font>"));
                actionBar.setSubtitle(Html.fromHtml("<small><font color='" + context.getResources().getColor(R.color.light) + "'>" + subTitle + "</font></small>"));
            }

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (LightDark()) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                if (API > 25) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
                }
            }
            window.setStatusBarColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.77f), wallpaperStaticLive() ? 130 : 40));
            window.setNavigationBarColor(setColorAlpha(mixColors(PublicVariable.primaryColor, PublicVariable.colorLightDark, 0.33f), wallpaperStaticLive() ? 180 : 100));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*GUI Functions*/
    public void Toast(String toastContent, int gravity) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.toast_view, null/*(ViewGroup) activity.findViewById(R.id.toastView)*/);

        LayerDrawable drawToast = null;
        if (gravity == Gravity.TOP) {
            drawToast = (LayerDrawable) context.getResources().getDrawable(R.drawable.toast_background_top);
        } else if (gravity == Gravity.BOTTOM) {
            drawToast = (LayerDrawable) context.getResources().getDrawable(R.drawable.toast_background_bottom);
        }
        GradientDrawable backToast = (GradientDrawable) drawToast.findDrawableByLayerId(R.id.backtemp);

        TextView textView = (TextView) layout.findViewById(R.id.toastText);
        textView.setText(toastContent);
        if (LightDark()) {
            backToast.setColor(context.getResources().getColor(R.color.light_trans));
            textView.setBackground(drawToast);
            textView.setTextColor(context.getResources().getColor(R.color.dark));
            textView.setShadowLayer(0.02f, 2, 2, context.getResources().getColor(R.color.trans_black_high));
        } else if (!LightDark()) {
            backToast.setColor(context.getResources().getColor(R.color.trans_black));
            textView.setBackground(drawToast);
            textView.setTextColor(context.getResources().getColor(R.color.light));
            textView.setShadowLayer(0.02f, 2, 2, context.getResources().getColor(R.color.light_trans_high));
        }
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.FILL_HORIZONTAL | gravity, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /*Let Me Know*/
    public List<String> letMeKnow(Activity activity, int maxValue, long startTime /*‪86400000‬ = 1 days*/, long endTime  /*System.currentTimeMillis()*/) {
        /*‪86400000 = 24h --- 82800000 = 23h‬*/
        List<String> freqApps = new ArrayList<String>();
        try {
            if (UsageStatsEnabled()) {
                UsageStatsManager mUsageStatsManager = (UsageStatsManager) activity.getSystemService(Context.USAGE_STATS_SERVICE);
                List<UsageStats> queryUsageStats = mUsageStatsManager
                        .queryUsageStats(UsageStatsManager.INTERVAL_BEST,
                                System.currentTimeMillis() - startTime,
                                endTime);
                Collections.sort(
                        queryUsageStats,
                        new Comparator<UsageStats>() {
                            @Override
                            public int compare(UsageStats left, UsageStats right) {
                                return Long.compare(
                                        right.getTotalTimeInForeground(), left.getTotalTimeInForeground()
                                );
                            }
                        }
                );
                for (int i = 0; i < maxValue; i++) {
                    String aPackageName = queryUsageStats.get(i).getPackageName();
                    try {
                        if (!aPackageName.equals(context.getPackageName())) {
                            if (appInstalledOrNot(aPackageName)) {
                                if (!ifSystem(aPackageName)) {
                                    if (!ifDefaultLauncher(aPackageName)) {
                                        if (canLaunch(aPackageName)) {
                                            freqApps.add(aPackageName);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Set<String> stringHashSet = new LinkedHashSet<>(freqApps);
                freqApps.clear();
                freqApps.addAll(stringHashSet);
            } else {
                UsageAccess(activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return freqApps;
    }

    /*Activity Recognition*/
    public void enableActivityRecognition(Activity activity) {
        ActivityRecognitionClient activityRecognitionClient = new ActivityRecognitionClient(activity);
        Intent intent = new Intent(activity.getApplicationContext(), ActivityRecognitionService.class);
        PendingIntent pendingIntent = PendingIntent.getService(activity.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Task<Void> task = activityRecognitionClient.requestActivityUpdates(
                ((1000 * 60) * (3)),
                pendingIntent);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void result) {
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void enableActivityRecognition(Activity activity, final SwitchPreference switchPreference) {
        ActivityRecognitionClient activityRecognitionClient = new ActivityRecognitionClient(activity);
        Intent intent = new Intent(activity.getApplicationContext(), ActivityRecognitionService.class);
        PendingIntent pendingIntent = PendingIntent.getService(activity.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Task<Void> task = activityRecognitionClient.requestActivityUpdates(
                ((1000 * 60) * (3)),
                pendingIntent);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void result) {
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                switchPreference.setChecked(false);
            }
        });
    }

    /*Interaction Observer*/
    public void sendInteractionObserverEvent(View view, String packageName, String className, int accessibilityEvent, int accessibilityAction) {
        if (!AccessibilityServiceEnabled()) {
            AccessibilityService(activity);
        } else {
            InteractionObserver.setPackageNameClassName(packageName, className);
            AccessibilityManager accessibilityManager = (AccessibilityManager) context.getSystemService(ACCESSIBILITY_SERVICE);
            AccessibilityEvent event = AccessibilityEvent.obtain();
            event.setSource(view);
            event.setEventType(accessibilityEvent);
            event.setAction(accessibilityAction);
            event.getText().add(context.getPackageName());
            accessibilityManager.sendAccessibilityEvent(event);
        }
    }

    /*Notification Function*/
    public void PopupNotificationShortcuts(View view, final String notificationPackage, String className, int itemPosition) {
        try {
            Vibrator vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(50);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            int W = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    221,
                    context.getResources().getDisplayMetrics());

            ArrayList<NavDrawerItem> navDrawerItemsSaved = new ArrayList<NavDrawerItem>();
            navDrawerItemsSaved.clear();

            SqLiteDataBaseNotification sqLiteDataBaseNotification = new SqLiteDataBaseNotification(context, notificationPackage, 1);
            List<String> notificationsTimes = sqLiteDataBaseNotification.getAllNotificationInfo(SqLiteDataBaseNotification.NOTIFICATION_COLUMN_TIME_VALUE, "ASC");
            Cursor notificationCursor = null;
            for (int i = 0; i < notificationsTimes.size(); i++) {

                String notificationTime = notificationsTimes.get(i);
                notificationCursor = sqLiteDataBaseNotification.getNotificationColumnInfo(SqLiteDataBaseNotification.NOTIFICATION_COLUMN_TIME_VALUE, notificationsTimes.get(i));
                notificationCursor.moveToFirst();

                String notificationTitle = notificationCursor.getString(notificationCursor.getColumnIndex(SqLiteDataBaseNotification.NOTIFICATION_COLUMN_TITLE_VALUE));
                String notificationText = notificationCursor.getString(notificationCursor.getColumnIndex(SqLiteDataBaseNotification.NOTIFICATION_COLUMN_TEXT_VALUE));
                String notificationId = notificationCursor.getString(notificationCursor.getColumnIndex(SqLiteDataBaseNotification.NOTIFICATION_COLUMN_KEY_VALUE));
                byte[] notificationIconByte = notificationCursor.getBlob(4);
                Drawable notificationIcon = byteToDrawable(notificationIconByte);

                navDrawerItemsSaved.add(
                        new NavDrawerItem(
                                notificationTime,
                                notificationPackage,
                                appName(notificationPackage),
                                notificationTitle,
                                notificationText,
                                shapedAppIcon(notificationPackage),
                                shapedNotificationUser(notificationIcon),
                                notificationId
                        )
                );
            }
            if (notificationCursor != null) {
                notificationCursor.close();
            }
            sqLiteDataBaseNotification.close();
            PopupShortcutsNotification popupShortcutsNotification =
                    new PopupShortcutsNotification(context, navDrawerItemsSaved, className, notificationPackage, itemPosition);

            final ListPopupWindow listPopupWindow = new ListPopupWindow(context);
            listPopupWindow.setAdapter(popupShortcutsNotification);
            listPopupWindow.setAnchorView(view);
            listPopupWindow.setWidth(W);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setModal(true);
            listPopupWindow.setBackgroundDrawable(null);
            try {
                listPopupWindow.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(context.getString(R.string.hidePopupListViewShortcutsNotification));
            final ListPopupWindow finalListPopupWindow = listPopupWindow;
            final BroadcastReceiver counterReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(context.getString(R.string.hidePopupListViewShortcutsNotification))) {
                        if (finalListPopupWindow.isShowing()) {
                            finalListPopupWindow.dismiss();
                        }
                    }
                }
            };
            context.registerReceiver(counterReceiver, intentFilter);

            listPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    context.unregisterReceiver(counterReceiver);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Action Center Functions*/
    public void Preferences(final View actionCenter) {
        PublicVariable.actionCenterX = (int) actionCenter.getX();
        PublicVariable.actionCenterY = (int) actionCenter.getY();
        PublicVariable.actionCenterWidth = actionCenter.getWidth();
        PublicVariable.actionCenterHeight = actionCenter.getHeight();
        actionCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(activity, actionCenter, "transition");
                        PublicVariable.actionCenterRevealX = (int) (PublicVariable.actionCenterX + (PublicVariable.actionCenterWidth / 2));
                        PublicVariable.actionCenterRevealY = (int) (PublicVariable.actionCenterY + (PublicVariable.actionCenterHeight / 2));
                        Intent intent = new Intent();
                        intent.putExtra("EXTRA_CIRCULAR_REVEAL_X", PublicVariable.actionCenterRevealX);
                        intent.putExtra("EXTRA_CIRCULAR_REVEAL_Y", PublicVariable.actionCenterRevealY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (LightDark()) {
                            intent.setClass(context, SettingGUILight.class);
                        } else if (!LightDark()) {
                            intent.setClass(context, SettingGUIDark.class);
                        }
                        context.startActivity(intent, options.toBundle());
                    }
                }, 113);
            }
        });
    }

    /*App Shortcuts*/
    public void setAppShortcutsScheduler() {
        try {
            Intent alarmIntent = new Intent(context, SetAppShortcuts.class);
            PendingIntent alarmIntentPendingIntent = PendingIntent.getService(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                    AlarmManager.INTERVAL_HALF_HOUR,
                    alarmIntentPendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAppShortcuts() {
        AppShortcutsLoadIntelligentServices appShortcutsLoadIntelligentServices = new AppShortcutsLoadIntelligentServices();
        appShortcutsLoadIntelligentServices.execute();
    }

    /*Splash*/
    public WindowManager.LayoutParams splashRevealParams(int W, int H, int X, int Y) {
        int navBarHeight = 0;
        int resourceIdNav = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceIdNav > 0) {
            navBarHeight = context.getResources().getDimensionPixelSize(resourceIdNav);
        }
        int statusBarHeight = 0;
        int resourceIdStatus = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceIdStatus > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceIdStatus);
        }
        WindowManager.LayoutParams layoutParams = null;
        if (API > 25) {
            layoutParams = new WindowManager.LayoutParams(
                    W,
                    H + statusBarHeight + navBarHeight,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);
        } else {
            layoutParams = new WindowManager.LayoutParams(
                    W,
                    H + statusBarHeight + navBarHeight,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);
        }

        layoutParams.gravity = Gravity.TOP | Gravity.START;
        layoutParams.x = X;
        layoutParams.y = Y - statusBarHeight;
        return layoutParams;
    }

    public void circularRevealViewScreen(final View view,
                                         final int xPosition, final int yPosition,
                                         boolean circularExpandCollapse) {
        if (circularExpandCollapse) {
            int startRadius = 0;
            int endRadius = (int) Math.hypot(displayX(), displayY());
            Animator animator = ViewAnimationUtils.createCircularReveal(
                    view,
                    (xPosition),
                    (yPosition),
                    startRadius,
                    endRadius
            );
            animator.setInterpolator(new FastOutLinearInInterpolator());
            animator.setDuration(777);
            animator.start();
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
        } else {
            int startRadius = (int) Math.hypot(displayX(), displayY());
            int endRadius = 0;
            Animator animator = ViewAnimationUtils.createCircularReveal(
                    view,
                    (xPosition),
                    (yPosition),
                    startRadius,
                    endRadius
            );
            animator.setInterpolator(new FastOutLinearInInterpolator());
            animator.setDuration(333);
            animator.start();
        }
    }

    public void circularRevealSplashScreenRemoval(final View view,
                                                  final int xPosition, final int yPosition) {
        int startRadius = 0;
        int endRadius = (int) Math.hypot(displayX(), displayY());
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view,
                (xPosition),
                (yPosition),
                startRadius,
                endRadius
        );
        animator.setInterpolator(new FastOutLinearInInterpolator());
        animator.setDuration(555);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    public void circularRevealSplashScreen(final View view, final View childView,
                                           final int xPosition, final int yPosition, final int iconColor,
                                           final String packageName, final String className,
                                           boolean floatIt,
                                           boolean circularExpandCollapse) throws Exception {
        if (circularExpandCollapse) {
            view.setBackgroundColor(iconColor);
            new HeartBeat(packageName, childView);

            int startRadius = 0;
            int endRadius = (int) Math.hypot(displayX(), displayY());
            Animator animator = ViewAnimationUtils.createCircularReveal(
                    view,
                    (xPosition + (childView.getWidth() / 2)),
                    (yPosition + (childView.getHeight() / 2)),
                    startRadius,
                    endRadius
            );
            animator.setInterpolator(new FastOutLinearInInterpolator());
            animator.setDuration(555);
            animator.start();
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (floatIt) {
                                if (freeFormSupport(context) && FreeForm()) {
                                    openApplicationFreeForm(packageName, className);
                                }
                            } else if (className != null) {
                                openApplicationFromActivity(packageName, className);
                            } else {
                                openApplication(packageName);
                            }
                        }
                    }, 51);
                    if (UsageStatsEnabled()) {
                        try {
                            UsageStatsManager mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
                            List<UsageStats> queryUsageStats = mUsageStatsManager
                                    .queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                                            System.currentTimeMillis() - 1000 * 60,            //begin
                                            System.currentTimeMillis());                    //end
                            Collections.sort(queryUsageStats, new Comparator<UsageStats>() {
                                @Override
                                public int compare(UsageStats left, UsageStats right) {
                                    return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
                                }
                            });
                            String inFrontPackage = queryUsageStats.get(0).getPackageName();
                            if (inFrontPackage.contains(packageName)) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        context.stopService(new Intent(context, FloatingSplash.class));
                                    }
                                }, 133);
                            } else {
                                PublicVariable.hearBeatCheckPoint = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        context.stopService(new Intent(context, FloatingSplash.class));
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
        } else {
            view.setBackgroundColor(iconColor);
            int startRadius = (int) Math.hypot(displayX(), displayY());
            int endRadius = 0;
            Animator animator = ViewAnimationUtils.createCircularReveal(
                    view,
                    (xPosition + (childView.getWidth() / 2)),
                    (yPosition + (childView.getHeight() / 2)),
                    startRadius,
                    endRadius
            );
            animator.setInterpolator(new FastOutLinearInInterpolator());
            animator.setDuration(123);
            animator.start();
        }
    }

    public void circularRevealSplashScreenClose(final String packageName, final View view, final View childView,
                                                final int xPosition, final int yPosition) throws Exception {
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
        BitmapDrawable wallpaper = (BitmapDrawable) wallpaperManager.getDrawable();

        Bitmap bitmapWallpaper = wallpaper.getBitmap();
        Bitmap inputBitmap = Bitmap.createBitmap(
                bitmapWallpaper,
                (bitmapWallpaper.getWidth() / 2) - (displayX() / 2),
                (bitmapWallpaper.getHeight() / 2) - (displayY() / 2),
                displayX(),
                displayY()
        );
        view.setBackground(bitmapToDrawable(inputBitmap));

        int startRadius = 0;
        int endRadius = (int) Math.hypot(displayX(), displayY());
        Animator animator = ViewAnimationUtils.createCircularReveal(
                view,
                (xPosition + (childView.getWidth() / 2)),
                (yPosition + (childView.getHeight() / 2)),
                startRadius,
                endRadius
        );
        animator.setInterpolator(new FastOutLinearInInterpolator());
        animator.setDuration(333);
        animator.start();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (UsageStatsEnabled()) {
                    try {
                        Intent homeScreen = new Intent(Intent.ACTION_MAIN);
                        homeScreen.addCategory(Intent.CATEGORY_HOME);
                        homeScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(homeScreen,
                                ActivityOptions.makeCustomAnimation(context, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        new HeartBeatClose(packageName, childView);
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    public boolean SplashRevealEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("floatingSplash", true);
    }

    /*Firebase Remote Config*/
    public boolean isBetaTester() {
        return readPreference(".BETA", "isBetaTester", false);
    }

    public String versionCodeRemoteConfigKey() {
        String versionCodeKey = null;
        if (readPreference(".BETA", "isBetaTester", false)) {
            versionCodeKey = context.getString(R.string.BETAintegerVersionCodeNewUpdatePhone);
        } else {
            versionCodeKey = context.getString(R.string.integerVersionCodeNewUpdatePhone);
        }
        return versionCodeKey;
    }

    public String versionNameRemoteConfigKey() {
        String versionCodeKey = null;
        if (readPreference(".BETA", "isBetaTester", false)) {
            versionCodeKey = context.getString(R.string.BETAstringVersionNameNewUpdatePhone);
        } else {
            versionCodeKey = context.getString(R.string.stringVersionNameNewUpdatePhone);
        }
        return versionCodeKey;
    }

    public String upcomingChangeLogRemoteConfigKey() {
        String versionCodeKey = null;
        if (readPreference(".BETA", "isBetaTester", false)) {
            versionCodeKey = context.getString(R.string.BETAstringUpcomingChangeLogPhone);
        } else {
            versionCodeKey = context.getString(R.string.stringUpcomingChangeLogPhone);
        }
        return versionCodeKey;
    }

    public String upcomingChangeLogSummaryConfigKey() {
        String versionCodeKey = null;
        if (readPreference(".BETA", "isBetaTester", false)) {
            versionCodeKey = context.getString(R.string.BETAstringUpcomingChangeLogSummaryPhone);
        } else {
            versionCodeKey = context.getString(R.string.stringUpcomingChangeLogSummaryPhone);
        }
        return versionCodeKey;
    }

    /*Fingerprint Checkpoint*/
    public static class AuthOpenAppValues {
        public static SqLiteDataBaseIntelligentServices authSqLiteDataBaseIntelligentServices;

        public static String authPackageName;
        public static String authClassName;
        public static int authOrderNumber;
        public static int authPositionX;
        public static int authPositionY;
        public static int authHW;

        public static boolean authUnlockIt = false;
        public static boolean authPopupOption = false;

        public static View authAnchorView;

        public static KeyStore keyStore;
        public static KeyGenerator keyGenerator;
    }

    public static class WindowMode {
        static final int WINDOWING_MODE_FREEFORM = 5;
        private static final int FREEFORM_WORKSPACE_STACK_ID = 2;
    }

    public static class DisplaySection {
        public static final int TopLeft = 1;
        public static final int TopRight = 2;
        public static final int BottomLeft = 3;
        public static final int BottomRight = 4;
    }

    private class LoadApplications extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                filterInstalledApplications(context.getPackageManager().queryIntentActivities(intent, 0));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Intent configuration = new Intent(context, Configurations.class);
            configuration.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            activity.startActivity(configuration);
        }
    }

    private class LoadApplicationsIntelligentService extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = null;
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                sqLiteDataBaseIntelligentServices = filterInstalledApplicationsIntelligentService(context.getPackageManager().queryIntentActivities(intent, 0));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (sqLiteDataBaseIntelligentServices != null) {
                    if (databaseAvailableIntelligentService()) {
                        List<String> freqApps = letMeKnow(activity, ((columnCount(105) + 1) * 7), (86400000 * 7), System.currentTimeMillis());
                        for (int i = 0; i < freqApps.size(); i++) {
                            try {
                                sqLiteDataBaseIntelligentServices.updateAppInfoFromPackageName(
                                        freqApps.get(i),//PackageName
                                        freqApps.size() - i,//GeneralCounter
                                        i//LastUsed
                                );
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            PublicVariable.forceReload = true;

            Intent hybrid = new Intent();
            if (StaticActive()) {
                hybrid.setClass(context, HybridIntelligentStaticPrimary.class);
            } else if (!StaticActive()) {
                hybrid.setClass(context, HybridIntelligentActivePrimary.class);
            }
            hybrid.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(hybrid, ActivityOptions.makeCustomAnimation(context, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        }
    }

    private class UpdateSqLiteDatabase extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... packageNameToDelete) {
            try {
                deleteDatabase("AppInfo");

                SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = null;
                List<String> intelligentServicesDatabase = intelligentServicesFiles();
                for (String sqLiteName : intelligentServicesDatabase) {
                    sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context, sqLiteName, 1);
                    sqLiteDataBaseIntelligentServices.deleteAppInfoData(packageNameToDelete[0]);
                }
                if (sqLiteDataBaseIntelligentServices != null) {
                    sqLiteDataBaseIntelligentServices.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    public class InvokeAuth {

        Cipher cipher;
        String keyName;

        public InvokeAuth(Cipher cipher, String keyName) {
            this.cipher = cipher;
            this.keyName = keyName;

            if (initCipher(this.cipher, this.keyName)) {
                FingerprintAuthenticationDialogFragment fingerprintAuthenticationDialogFragment = new FingerprintAuthenticationDialogFragment();
                fingerprintAuthenticationDialogFragment.setCryptoObject(new FingerprintManager.CryptoObject(this.cipher));
                fingerprintAuthenticationDialogFragment.show(activity.getFragmentManager(), context.getPackageName());
            }
        }
    }

    class AppShortcutsLoadIntelligentServices extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<String> doInBackground(Void... voids) {
            int secondaryCount = (columnCount(105)) * 2;
            List<String> packageNames = new ArrayList<String>();
            try {
                packageNames.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (databaseAvailableIntelligentService()) {
                try {
                    SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(context.getApplicationContext(), sqLiteIntelligentServiceFileName(), 1);

                    int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                    int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                    int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                    int timeNow = Integer.parseInt(String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond));

                    int maxValueSame = secondaryCount + 1;
                    List<Integer> allAppLastUsed = sqLiteDataBaseIntelligentServices.getAllAppLastUsed("DESC", maxValueSame);
                    List<Integer> allAppOrderNumber = sqLiteDataBaseIntelligentServices.getAllAppInfoOfOrderNumber(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, "DESC", maxValueSame);
                    List<Integer> minusList = new ArrayList<Integer>();

                    Map<Integer, Integer> mapOrderNumberMinusTime = new LinkedHashMap<Integer, Integer>();
                    for (int i = 0; i < allAppLastUsed.size(); i++) {
                        int minusTime = Math.abs(timeNow - allAppLastUsed.get(i));

                        sqLiteDataBaseIntelligentServices.updateAppInfoFromLastUsed(
                                String.valueOf(allAppOrderNumber.get(i)),
                                String.valueOf(allAppLastUsed.get(i)),
                                minusTime
                        );
                        minusList.add(minusTime);
                        mapOrderNumberMinusTime.put(allAppOrderNumber.get(i), allAppLastUsed.get(i));
                    }
                    Collections.sort(minusList);

                    Cursor cursorPrimary = null;
                    for (int i = 0; i < maxValueSame; i++) {
                        cursorPrimary = sqLiteDataBaseIntelligentServices.getMinusData(minusList.get(i));
                        cursorPrimary.moveToFirst();

                        String primaryPackages = cursorPrimary.getString(cursorPrimary.getColumnIndex(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_PACKAGES));
                        packageNames.add(primaryPackages);
                    }

                    Set<String> stringHashSet = new LinkedHashSet<>(packageNames);
                    packageNames.clear();
                    packageNames.addAll(stringHashSet);

                    try {
                        cursorPrimary.close();
                        sqLiteDataBaseIntelligentServices.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return packageNames;
        }

        @Override
        protected void onPostExecute(List<String> packageNames) {
            super.onPostExecute(packageNames);
            if (packageNames != null) {
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                        ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
                        shortcutManager.removeAllDynamicShortcuts();

                        List<ShortcutInfo> infoShortcuts = new ArrayList<ShortcutInfo>();
                        infoShortcuts.clear();

                        int maxLoopShortcuts;
                        if (packageNames.size() > shortcutManager.getMaxShortcutCountPerActivity()) {
                            maxLoopShortcuts = shortcutManager.getMaxShortcutCountPerActivity();
                        } else {
                            maxLoopShortcuts = packageNames.size();
                        }
                        for (int shortcutRank = 0; shortcutRank < maxLoopShortcuts; shortcutRank++) {
                            try {
                                Intent queryIntent = new Intent();
                                queryIntent.setAction(Intent.ACTION_MAIN);
                                queryIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                                queryIntent.setPackage(packageNames.get(shortcutRank));

                                Intent intent = new Intent(context, LaunchPad.class);
                                intent.setAction("App_Shortcuts_Action");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                intent.putExtra("packageName", packageNames.get(shortcutRank));
                                intent.putExtra("className", context.getPackageManager().queryIntentActivities(queryIntent, 0).get(0).activityInfo.name);
                                intent.putExtra("positionX", ((displayX() / 2) - (DpToInteger(77) / 2)));
                                intent.putExtra("positionY", ((displayY() / 2) - (DpToInteger(77) / 2)));
                                intent.putExtra("HW", DpToInteger(77));

                                ActivityInfo activityInfo = context.getPackageManager()
                                        .getActivityInfo(new ComponentName(packageNames.get(shortcutRank), context.getPackageManager().queryIntentActivities(queryIntent, 0).get(0).activityInfo.name), 0);

                                ShortcutInfo shortcutInfo = new ShortcutInfo.Builder(context, (packageNames.get(shortcutRank)))
                                        .setShortLabel(activityLabel(activityInfo))
                                        .setLongLabel(activityLabel(activityInfo))
                                        .setIcon(Icon.createWithBitmap(appIconBitmap(activityInfo)))
                                        .setIntent(intent)
                                        .setRank(shortcutRank)
                                        .build();

                                infoShortcuts.add(shortcutInfo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        shortcutManager.addDynamicShortcuts(infoShortcuts);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    context.stopService(new Intent(context, SetAppShortcuts.class));
                }
            }
        }
    }

    public class HeartBeat {
        String packageName;
        View viewToBeat;
        boolean getFirstApp = false;
        Animator.AnimatorListener scaleDownListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (PublicVariable.hearBeatCheckPoint) {
                    if (getFirstApp) {
                        try {
                            UsageStatsManager mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
                            List<UsageStats> queryUsageStats = mUsageStatsManager
                                    .queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                                            System.currentTimeMillis() - 1000 * 60,            //begin
                                            System.currentTimeMillis());                    //end
                            Collections.sort(queryUsageStats, new Comparator<UsageStats>() {
                                @Override
                                public int compare(UsageStats left, UsageStats right) {
                                    return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
                                }
                            });
                            try {
                                String inFrontPackageName = queryUsageStats.get(0).getPackageName();
                                String inFrontSecondPackageName = queryUsageStats.get(1).getPackageName();
                                if (inFrontPackageName.contains(packageName) || inFrontSecondPackageName.contains(packageName)) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            context.stopService(new Intent(context, FloatingSplash.class));
                                        }
                                    }, 133);
                                } else {
                                    viewToBeat.animate().scaleXBy(0.33f).scaleYBy(0.33f).setDuration(233).setListener(scaleUpListener);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            context.stopService(new Intent(context, FloatingSplash.class));
                        }
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        };
        Animator.AnimatorListener scaleUpListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                viewToBeat.animate().scaleXBy(-0.33f).scaleYBy(-0.33f).setDuration(333).setListener(scaleDownListener);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        };

        public HeartBeat(String packageName, View viewToBeat) {
            this.packageName = packageName;
            this.viewToBeat = viewToBeat;
            this.viewToBeat.animate().scaleXBy(0.33f).scaleYBy(0.33f).setDuration(233).setListener(scaleUpListener);

            if (UsageStatsEnabled()) {
                getFirstApp = true;
            }
        }
    }

    public class HeartBeatClose {
        String packageName;
        View viewToBeat;
        Animator.AnimatorListener scaleDownListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                viewToBeat.animate().scaleXBy(0.33f).scaleYBy(0.33f).setDuration(233).setListener(scaleUpListener);

                try {
                    UsageStatsManager mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
                    List<UsageStats> queryUsageStats = mUsageStatsManager
                            .queryUsageStats(UsageStatsManager.INTERVAL_DAILY,
                                    System.currentTimeMillis() - 1000 * 60,            //begin
                                    System.currentTimeMillis());                    //end
                    Collections.sort(queryUsageStats, new Comparator<UsageStats>() {
                        @Override
                        public int compare(UsageStats left, UsageStats right) {
                            return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
                        }
                    });
                    String inFrontPackageName = queryUsageStats.get(0).getPackageName();

                    PackageManager localPackageManager = context.getPackageManager();
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    String defaultLauncher = localPackageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY).activityInfo.packageName;
                    if (inFrontPackageName.contains(defaultLauncher)) {
                        context.stopService(new Intent(context, FloatingSplash.class));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    context.stopService(new Intent(context, FloatingSplash.class));
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        };
        Animator.AnimatorListener scaleUpListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                viewToBeat.animate().scaleXBy(-0.33f).scaleYBy(-0.33f).setDuration(333).setListener(scaleDownListener);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        };

        public HeartBeatClose(String packageName, View viewToBeat) {
            this.packageName = packageName;
            this.viewToBeat = viewToBeat;
            this.viewToBeat.animate().scaleXBy(0.33f).scaleYBy(0.33f).setDuration(233).setListener(scaleUpListener);
        }
    }
}
