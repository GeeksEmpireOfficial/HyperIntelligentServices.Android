/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.Settings;
import android.text.Html;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;

import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

public class LicenseValidator extends Service {

    private static final String BASE64_PUBLIC_KEY =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgJYSPdNEhqMN6T6TgbqfrdWujgpmYIE4FDBHnBaNnu+IKdG9Fq+tg9S4FZCucqwwyZ3UefspLMXkwVa" +
                    "OTR++KOEKLBepTQWEQNDih/2fmJy6LL9zMO82giENW2Be/YvLB8ROyhr2pGZ9qEKDc1/yBMyz7X5qmuUNqI7dLCppY73I4+pUatwXTYBMmAWs2X26g/" +
                    "OVmQiqa+qF4mzOFEzFta3sDXDKh15p+f/2IYBoMFR4xz1x0t1P28YJa7zblIECNzrn18jRyIIYYyyYyYXXdz6lLefhOZDFXCeGrgGV+KZ3luZ+f6mLc" +
                    "9+gvuryE9Nvku3Ht22H3KdKitF8o4w1iQIDAQAB";
    private static final byte[] SALT = new byte[]{
            -16, -13, 30, -128, -103, -57, 74, -64, 53, 88, -97, -45, 77, -113, -36, -113, -11, 32, -64, 89
    };
    FunctionsClass functionsClass;
    LicenseChecker licenseChecker;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        if (functionsClass.returnAPI() < 26) {
            startForeground(111, bindServiceLOW());
        } else {
            startForeground(111, bindServiceHIGH());
        }

        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        licenseChecker = new LicenseChecker(
                getApplicationContext(),
                new ServerManagedPolicy(getApplicationContext(), new AESObfuscator(SALT, getPackageName(), deviceId)),
                BASE64_PUBLIC_KEY
        );
        final LicenseCheckerCallback licenseCheckerCallback = new LicenseCheckerCallback() {
            @Override
            public void allow(int reason) {
                if (reason == Policy.LICENSED) {
                    functionsClass.saveFileAppendLine(".License", String.valueOf(reason));
                    stopSelf();
                } else if (reason == Policy.RETRY) {
                    stopSelf();
                }
            }

            @Override
            public void dontAllow(int reason) {
                if (reason == Policy.NOT_LICENSED) {
                    sendBroadcast(new Intent(getString(R.string.license)));
                } else if (reason == Policy.RETRY) {
                    stopSelf();
                }
            }

            @Override
            public void applicationError(int errorCode) {
            }
        };
        licenseChecker.checkAccess(licenseCheckerCallback);

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        functionsClass = new FunctionsClass(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            licenseChecker.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Notification bindServiceHIGH() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationChannel notificationChannel = new NotificationChannel(getPackageName(), getString(R.string.app_name), NotificationManager.IMPORTANCE_MAX);
        notificationManager.createNotificationChannel(notificationChannel);

        Notification.Builder mBuilder = new Notification.Builder(getApplicationContext());
        mBuilder.setColor(getResources().getColor(R.color.default_color));
        mBuilder.setContentTitle(getString(R.string.license_info));
        mBuilder.setContentText(getString(R.string.license_info_desc));
        mBuilder.setContentTitle(Html.fromHtml("<b><font color='" + getResources().getColor(R.color.default_color_darker) + "'>" + getString(R.string.license_info) + "</font></b>"));
        mBuilder.setContentText(Html.fromHtml("<font color='" + getResources().getColor(R.color.default_color_darker) + "'>" + getString(R.string.license_info_desc) + "</font>"));
        mBuilder.setTicker(getResources().getString(R.string.updating_info));
        mBuilder.setSmallIcon(R.drawable.ic_notification);
        mBuilder.setAutoCancel(false);
        mBuilder.setProgress(0, 0, true);
        mBuilder.setChannelId(getPackageName());

        return mBuilder.build();
    }

    protected Notification bindServiceLOW() {
        Notification.Builder mBuilder = new Notification.Builder(getApplicationContext());
        mBuilder.setColor(getResources().getColor(R.color.default_color));
        mBuilder.setContentTitle(getString(R.string.license_info));
        mBuilder.setContentText(getString(R.string.license_info_desc));
        mBuilder.setContentTitle(Html.fromHtml("<b><font color='" + getResources().getColor(R.color.default_color_darker) + "'>" + getString(R.string.license_info) + "</font></b>"));
        mBuilder.setContentText(Html.fromHtml("<font color='" + getResources().getColor(R.color.default_color_darker) + "'>" + getString(R.string.license_info_desc) + "</font>"));
        mBuilder.setTicker(getResources().getString(R.string.updating_info));
        mBuilder.setSmallIcon(R.drawable.ic_notification);
        mBuilder.setAutoCancel(false);
        mBuilder.setProgress(0, 0, true);

        return mBuilder.build();
    }
}
