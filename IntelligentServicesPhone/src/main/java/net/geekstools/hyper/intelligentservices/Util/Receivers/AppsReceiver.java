/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;

public class AppsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!PublicVariable.inMemory) {

        }
        FunctionsClass functionsClass = new FunctionsClass(context);
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_FULLY_REMOVED)) {
            try {
                String packageNameToDelete = intent.getData().getEncodedSchemeSpecificPart();
                functionsClass.UpdateSqLiteDatabaseInfo(packageNameToDelete);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
