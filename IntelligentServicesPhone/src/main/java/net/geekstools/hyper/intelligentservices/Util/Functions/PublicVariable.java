/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Functions;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class PublicVariable {

    public static Activity HybridIntelligent;
    public static Activity settingGUI;
    public static Context contextStatic;

    public static List<String> allApps = null;
    public static List<Object> primaryAppObjects = new ArrayList<Object>();

    public static int primaryColor;
    public static int primaryColorOpposite;
    public static int dominantColor;
    public static int colorLightDark;
    public static int colorLightDarkOpposite;
    public static int countAppInfo;
    public static int actionBarHeight;
    public static int statusBarHeight;
    public static int actionCenterX;
    public static int actionCenterY;
    public static int actionCenterWidth;
    public static int actionCenterHeight;
    public static int actionCenterRevealX;
    public static int actionCenterRevealY;

    public static String databaseOldFile;
    public static String previousDuplicated;

    public static boolean forceReload = false;
    public static boolean forceRelaunch = false;
    public static boolean newIcon = false;
    public static boolean inFront = false;
    public static boolean inMemory = false;
    public static boolean showShare = false;
    public static boolean hearBeatCheckPoint = false;
}
