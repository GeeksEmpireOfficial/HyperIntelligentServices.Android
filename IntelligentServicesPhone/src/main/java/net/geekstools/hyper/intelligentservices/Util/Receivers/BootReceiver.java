/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseAppInfo;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        try {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) || intent.getAction().equals(Intent.ACTION_LOCKED_BOOT_COMPLETED)) {
                FunctionsClass functionsClass = new FunctionsClass(context);
                SqLiteDataBaseAppInfo sqLiteDataBaseAppInfo = new SqLiteDataBaseAppInfo(context, 1);
                PublicVariable.allApps = sqLiteDataBaseAppInfo.getAllAppInfo(SqLiteDataBaseAppInfo.APP_INFO_COLUMN_APP_NAME, "ASC");
                try {
                    sqLiteDataBaseAppInfo.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*if (functionsClass.alwaysReady()){
                    Intent openBackground = new Intent(context, functionsClass.StaticActive() ? HybridIntelligentStaticPrimary.class : HybridIntelligentActivePrimary.class);
                    openBackground.putExtra("goHome", true);
                    openBackground.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(openBackground);
                }*/
                functionsClass.setAppShortcutsScheduler();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
