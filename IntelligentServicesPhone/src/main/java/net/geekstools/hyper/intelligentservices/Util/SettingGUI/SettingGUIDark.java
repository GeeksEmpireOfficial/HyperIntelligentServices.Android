/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.SettingGUI;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import net.geekstools.hyper.intelligentservices.BuildConfig;
import net.geekstools.hyper.intelligentservices.Configurations;
import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.ActivityRecognition.ActivityRecognitionService;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;
import net.geekstools.hyper.intelligentservices.Util.InteractionObserver.InteractionObserver;
import net.geekstools.hyper.intelligentservices.Util.Notification.NotificationListener;
import net.geekstools.hyper.intelligentservices.Util.SharingService;


public class SettingGUIDark extends PreferenceActivity implements OnSharedPreferenceChangeListener {

    FunctionsClass functionsClass;

    SharedPreferences sharedPreferences;
    SwitchPreference smart, observe, activityRecognition, notification, floatingSplash, alwaysReady, freeForm;
    ListPreference LightDark, RightLeft;
    Preference shapes, reload, whatsnew, support, adsfloating;

    View rootLayout;

    int revealX, revealY;
    String previousShape, newShape;

    FirebaseRemoteConfig firebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle saved) {
        super.onCreate(saved);
        addPreferencesFromResource(R.xml.setting_gui);
        try {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        smart = (SwitchPreference) findPreference("smart");
        observe = (SwitchPreference) findPreference("observe");
        activityRecognition = (SwitchPreference) findPreference("activityRecognition");
        notification = (SwitchPreference) findPreference("notification");
        floatingSplash = (SwitchPreference) findPreference("floatingSplash");
        alwaysReady = (SwitchPreference) findPreference("alwaysReady");
        freeForm = (SwitchPreference) findPreference("freeForm");

        LightDark = (ListPreference) findPreference("LightDark");
        RightLeft = (ListPreference) findPreference("RightLeft");

        shapes = (Preference) findPreference("shapes");
        reload = (Preference) findPreference("reload");
        whatsnew = (Preference) findPreference("whatsnew");
        support = (Preference) findPreference("support");
        adsfloating = (Preference) findPreference("adsfloating");

        PublicVariable.settingGUI = this;
        functionsClass = new FunctionsClass(getApplicationContext(), SettingGUIDark.this);
        functionsClass.setThemeColorPreferences(SettingGUIDark.this, SettingGUIDark.this.getListView(), getString(R.string.settingTitle), functionsClass.appVersionName(getPackageName()));
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        this.getListView().setCacheColorHint(Color.TRANSPARENT);
        this.getListView().setVerticalFadingEdgeEnabled(true);
        this.getListView().setFadingEdgeLength(functionsClass.DpToInteger(13));
        this.getListView().setVerticalScrollBarEnabled(false);
        this.getListView().setDivider(new ColorDrawable(Color.TRANSPARENT));
        this.getListView().setDividerHeight((int) functionsClass.DpToPixel(3));

        rootLayout = this.getWindow().getDecorView();
        rootLayout.setVisibility(View.INVISIBLE);

        revealX = getIntent().getIntExtra("EXTRA_CIRCULAR_REVEAL_X", (functionsClass.displayX() / 2));
        revealY = getIntent().getIntExtra("EXTRA_CIRCULAR_REVEAL_Y", (functionsClass.displayY() + PublicVariable.statusBarHeight) / 2);

        ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int finalRadius = (int) Math.hypot(functionsClass.displayX(), functionsClass.displayY());
                    Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, revealX, revealY, 0, finalRadius);
                    circularReveal.setDuration(1300);
                    circularReveal.setInterpolator(new AccelerateInterpolator());

                    rootLayout.setVisibility(View.VISIBLE);
                    circularReveal.start();
                    rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    circularReveal.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
                }
            });
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }

        support.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String[] contactOption = new String[]{
                        "Send an Email",
                        "Send a Message",
                        "Contact via Forum",
                        "Different Contact Options",
                        "Join Beta Program",
                        "Rate & Write Review"};
                AlertDialog.Builder builder = null;
                if (functionsClass.LightDark()) {
                    builder = new AlertDialog.Builder(SettingGUIDark.this, R.style.GeeksEmpire_Dialogue_Light);
                } else if (!functionsClass.LightDark()) {
                    builder = new AlertDialog.Builder(SettingGUIDark.this, R.style.GeeksEmpire_Dialogue_Dark);
                }
                LayerDrawable drawSupport = (LayerDrawable) getResources().getDrawable(R.drawable.draw_support);
                GradientDrawable backSupport = (GradientDrawable) drawSupport.findDrawableByLayerId(R.id.backtemp);
                backSupport.setColor(PublicVariable.primaryColorOpposite);
                builder.setIcon(drawSupport);
                builder.setTitle(getString(R.string.supportCategory));
                builder.setSingleChoiceItems(contactOption, 0, null);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        if (selectedPosition == 0) {
                            String textMsg = "\n\n\n\n\n"
                                    + "[Essential Information]" + "\n"
                                    + functionsClass.getDeviceName() + " | " + "API " + Build.VERSION.SDK_INT + " | " + functionsClass.getCountryIso().toUpperCase();
                            Intent email = new Intent(Intent.ACTION_SEND);
                            email.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.support)});
                            email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_tag) + " [" + functionsClass.appVersionName(getPackageName()) + "] ");
                            email.putExtra(Intent.EXTRA_TEXT, textMsg);
                            email.setType("text/*");
                            email.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(Intent.createChooser(email, getString(R.string.feedback_tag)));
                        } else if (selectedPosition == 1) {
                            Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_facebook_app)));
                            startActivity(a);
                        } else if (selectedPosition == 2) {
                            Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_xda)));
                            startActivity(a);
                        } else if (selectedPosition == 3) {
                            startService(new Intent(getApplicationContext(), SharingService.class));
                        } else if (selectedPosition == 4) {
                            Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_alpha) + getPackageName()));
                            startActivity(a);

                            functionsClass.Toast(getResources().getString(R.string.alphaTitle), Gravity.BOTTOM);
                        } else if (selectedPosition == 5) {
                            Intent a = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.play_store_link) + getPackageName()));
                            startActivity(a);

                            functionsClass.Toast(getResources().getString(R.string.alphaTitle), Gravity.BOTTOM);
                        }
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                builder.show();
                return true;
            }
        });

        adsfloating.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent ads = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_floating)));
                ads.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ads);
                return false;
            }
        });
        applyTheme();
    }

    @Override
    public void onStart() {
        super.onStart();
        smart.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (!Settings.ACTION_USAGE_ACCESS_SETTINGS.isEmpty()) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    if (sharedPreferences.getBoolean("smart", true) == true) {
                        smart.setChecked(true);

                        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                        startActivity(intent);
                    } else if (sharedPreferences.getBoolean("smart", true) == false) {
                        smart.setChecked(false);

                        functionsClass.UsageAccess(SettingGUIDark.this, smart);
                    }
                }
                return true;
            }
        });

        observe.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (!functionsClass.AccessibilityServiceEnabled() && !functionsClass.SettingServiceRunning(InteractionObserver.class)) {
                    functionsClass.AccessibilityService(SettingGUIDark.this, observe);
                } else {
                    Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                return true;
            }
        });

        activityRecognition.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                try {
                    ActivityRecognitionClient activityRecognitionClient = new ActivityRecognitionClient(PublicVariable.HybridIntelligent);
                    Intent intent = new Intent(getApplicationContext(), ActivityRecognitionService.class);
                    PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    if (functionsClass.ActivityRecognitionEnabled() == false) {
                        functionsClass.enableActivityRecognition(PublicVariable.HybridIntelligent, activityRecognition);
                    } else {
                        stopService(new Intent(getApplicationContext(), ActivityRecognitionService.class));
                        Task<Void> task = activityRecognitionClient.removeActivityUpdates(pendingIntent);
                        task.addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void result) {
                                activityRecognition.setChecked(false);
                            }
                        });

                        task.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                e.printStackTrace();
                                activityRecognition.setChecked(false);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        notification.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (functionsClass.NotificationAccess() && functionsClass.SettingServiceRunning(NotificationListener.class)) {
                    Intent notification = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    notification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(notification);
                } else {
                    functionsClass.NotificationAccessService(SettingGUIDark.this, notification);
                }
                return true;
            }
        });

        shapes.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setShapes();

                return true;
            }
        });

        LightDark.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                sharedPreferences.edit().putString("LightDark", String.valueOf(newValue)).apply();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String appTheme = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("LightDark", "3");
                        if (appTheme.equals("1")) {//light
                            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[0]);
                            startActivity(new Intent(getApplicationContext(), SettingGUILight.class));
                            SettingGUIDark.this.finish();
                        } else if (appTheme.equals("2")) {//dark
                            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[1]);
                        } else if (appTheme.equals("3")) {//dynamic
                            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[2]);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (functionsClass.LightDark()) {//light
                                        startActivity(new Intent(getApplicationContext(), SettingGUILight.class));
                                        SettingGUIDark.this.finish();
                                    } else if (!functionsClass.LightDark()) {/*dark*/}
                                }
                            }, 113);
                        } else if (appTheme.equals("4")) {//automatic
                            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[3]);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (functionsClass.LightDark()) {//light
                                        startActivity(new Intent(getApplicationContext(), SettingGUILight.class));
                                        SettingGUIDark.this.finish();
                                    } else if (!functionsClass.LightDark()) {/*dark*/}
                                }
                            }, 113);
                        }
                    }
                }, 113);
                return true;
            }
        });

        RightLeft.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                sharedPreferences.edit().putString("RightLeft", String.valueOf(newValue)).apply();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String handed = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("RightLeft", "1");
                        if (handed.equals("1")) {//right
                            RightLeft.setSummary(Html.fromHtml("<b>" + getString(R.string.right_handed) + "</b>"));
                        } else if (handed.equals("2")) {//left
                            RightLeft.setSummary(Html.fromHtml("<b>" + getString(R.string.left_handed) + "</b>"));
                        }
                        PublicVariable.forceRelaunch = true;
                    }
                }, 113);
                return true;
            }
        });

        reload.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                functionsClass.reloadData();

                return true;
            }
        });

        freeForm.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (functionsClass.FreeForm()) {
                    functionsClass.FreeFormInformation(SettingGUIDark.this, freeForm);
                } else {
                    freeForm.setChecked(false);
                }
                return true;
            }
        });

        alwaysReady.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if (!Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS.isEmpty()) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    if (sharedPreferences.getBoolean("alwaysReady", true) == true) {
                        alwaysReady.setChecked(true);

                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                        startActivity(intent);
                    } else if (sharedPreferences.getBoolean("alwaysReady", true) == false) {
                        alwaysReady.setChecked(false);

                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                        startActivity(intent);
                    }
                }
                return true;
            }
        });

        whatsnew.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                functionsClass.ChangeLog(SettingGUIDark.this, true);
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_default);

        firebaseRemoteConfig.fetch(0)
                .addOnCompleteListener(SettingGUIDark.this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            firebaseRemoteConfig.activateFetched();
                            if (firebaseRemoteConfig.getLong(functionsClass.versionCodeRemoteConfigKey()) > functionsClass.appVersionCode(getPackageName())) {
                                functionsClass.upcomingChangeLog(
                                        SettingGUIDark.this,
                                        firebaseRemoteConfig.getString(functionsClass.upcomingChangeLogRemoteConfigKey()),
                                        String.valueOf(firebaseRemoteConfig.getLong(functionsClass.versionCodeRemoteConfigKey()))
                                );
                            } else {
                            }
                        } else {
                        }
                    }
                });

        applyTheme();

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        alwaysReady.setChecked(powerManager.isIgnoringBatteryOptimizations(getPackageName()));
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
        stopService(new Intent(getApplicationContext(), SharingService.class));
        PublicVariable.showShare = false;
    }

    @Override
    public void onBackPressed() {
        if (PublicVariable.forceRelaunch) {
            startActivity(new Intent(getApplicationContext(), Configurations.class), ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
            finish();
            return;
        }
        stopService(new Intent(this, SharingService.class));
        PublicVariable.showShare = false;

        functionsClass.setThemeColor(PublicVariable.HybridIntelligent, PublicVariable.HybridIntelligent.findViewById(R.id.listWhole));
        float finalRadius = (int) Math.hypot(functionsClass.displayX(), functionsClass.displayY());
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                rootLayout, PublicVariable.actionCenterRevealX, PublicVariable.actionCenterRevealY, finalRadius, 0);

        circularReveal.setDuration(700);
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                rootLayout.setVisibility(View.INVISIBLE);
                finish();
            }
        });
        circularReveal.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_gui_menu, menu);

        MenuItem osp = menu.findItem(R.id.facebook);
        MenuItem share = menu.findItem(R.id.share);


        LayerDrawable drawOSP = (LayerDrawable) getResources().getDrawable(R.drawable.draw_facebook);
        GradientDrawable backOSP = (GradientDrawable) drawOSP.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable drawShare = (LayerDrawable) getResources().getDrawable(R.drawable.draw_share);
        GradientDrawable backShare = (GradientDrawable) drawShare.findDrawableByLayerId(R.id.backtemp);

        backOSP.setColor(PublicVariable.primaryColor);
        backShare.setColor(PublicVariable.primaryColor);

        osp.setIcon(drawOSP);
        share.setIcon(drawShare);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = true;
        switch (item.getItemId()) {
            case R.id.facebook: {
                Intent b = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_facebook_app)));
                b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(b);
                break;
            }
            case R.id.share: {
                if (PublicVariable.showShare == false) {
                    startService(new Intent(getApplicationContext(), SharingService.class));
                    PublicVariable.showShare = true;
                } else {
                    stopService(new Intent(getApplicationContext(), SharingService.class));
                    PublicVariable.showShare = false;
                }
                break;
            }
            case android.R.id.home: {
                functionsClass.setThemeColor(PublicVariable.HybridIntelligent, PublicVariable.HybridIntelligent.findViewById(R.id.listWhole));
                if (PublicVariable.forceRelaunch) {
                    startActivity(new Intent(getApplicationContext(), Configurations.class), ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                    finish();
                    super.onOptionsItemSelected(item);
                }
                float finalRadius = (int) Math.hypot(functionsClass.displayX(), functionsClass.displayY());
                Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                        rootLayout, PublicVariable.actionCenterRevealX, PublicVariable.actionCenterRevealY, finalRadius, 0);

                circularReveal.setDuration(700);
                circularReveal.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        rootLayout.setVisibility(View.INVISIBLE);
                        finish();
                    }
                });
                circularReveal.start();
                break;
            }
            default: {
                result = super.onOptionsItemSelected(item);
                break;
            }
        }
        return result;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }

    private void applyTheme() {
        String appTheme = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("LightDark", "3");
        if (appTheme.equals("1")) {
            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[0]);
        } else if (appTheme.equals("2")) {
            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[1]);
        } else if (appTheme.equals("3")) {
            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[2]);
        } else if (appTheme.equals("4")) {
            LightDark.setSummary(getResources().getStringArray(R.array.appThemeOptions)[3]);
        }

        if (functionsClass.RightLeft()) {
            RightLeft.setSummary(Html.fromHtml("<b>" + getString(R.string.right_handed) + "</b>"));
        } else if (!functionsClass.RightLeft()) {
            RightLeft.setSummary(Html.fromHtml("<b>" + getString(R.string.left_handed) + "</b>"));
        }

        reload.setSummary(Html.fromHtml("<big>" + getString(R.string.reloadAppInfo) + "</big>"));

        LayerDrawable drawSmart = (LayerDrawable) getResources().getDrawable(R.drawable.draw_smart);
        GradientDrawable backSmart = (GradientDrawable) drawSmart.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable drawPref = (LayerDrawable) getResources().getDrawable(R.drawable.draw_pref);
        GradientDrawable backPref = (GradientDrawable) drawPref.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable drawNotification = (LayerDrawable) getResources().getDrawable(R.drawable.draw_notification);
        GradientDrawable backNotification = (GradientDrawable) drawNotification.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable drawHanded = (LayerDrawable) getResources().getDrawable(R.drawable.draw_handed);
        GradientDrawable backHanded = (GradientDrawable) drawHanded.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable drawReload = (LayerDrawable) getResources().getDrawable(R.drawable.draw_reload);
        GradientDrawable backReload = (GradientDrawable) drawReload.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable drawFloatIt = (LayerDrawable) getResources().getDrawable(R.drawable.draw_floatit);
        GradientDrawable backFloatIt = (GradientDrawable) drawFloatIt.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable layerDrawableLoadLogo = (LayerDrawable) getDrawable(R.drawable.ic_launcher_layer);
        BitmapDrawable gradientDrawableLoadLogo = (BitmapDrawable) layerDrawableLoadLogo.findDrawableByLayerId(R.id.ic_launcher_back_layer);

        LayerDrawable drawSupport = (LayerDrawable) getResources().getDrawable(R.drawable.draw_support);
        GradientDrawable backSupport = (GradientDrawable) drawSupport.findDrawableByLayerId(R.id.backtemp);

        LayerDrawable layerDrawableLoadAds = (LayerDrawable) getDrawable(R.drawable.ic_floating_layer);
        BitmapDrawable gradientDrawableLoadAds = (BitmapDrawable) layerDrawableLoadAds.findDrawableByLayerId(R.id.ic_launcher_back_layer);

        backSmart.setColor(PublicVariable.primaryColor);
        backPref.setColor(PublicVariable.primaryColor);
        backNotification.setColor(PublicVariable.primaryColor);
        backHanded.setColor(PublicVariable.primaryColor);
        backReload.setColor(PublicVariable.primaryColor);
        backFloatIt.setColor(PublicVariable.primaryColor);
        gradientDrawableLoadLogo.setTint(PublicVariable.primaryColorOpposite);
        backSupport.setColor(PublicVariable.primaryColorOpposite);
        gradientDrawableLoadAds.setTint(PublicVariable.primaryColorOpposite);

        smart.setIcon(drawSmart);
        observe.setIcon(drawSmart);
        activityRecognition.setIcon(drawSmart);
        notification.setIcon(drawNotification);
        LightDark.setIcon(drawPref);
        floatingSplash.setIcon(drawPref);
        RightLeft.setIcon(drawHanded);
        reload.setIcon(drawReload);
        freeForm.setIcon(drawFloatIt);
        alwaysReady.setIcon(drawPref);
        whatsnew.setIcon(layerDrawableLoadLogo);
        support.setIcon(drawSupport);
        adsfloating.setIcon(layerDrawableLoadAds);

        switch (sharedPreferences.getInt("iconShape", 0)) {
            case 1:
                Drawable drawableTeardrop = getDrawable(R.drawable.droplet_icon);
                drawableTeardrop.setTint(PublicVariable.primaryColor);
                LayerDrawable layerDrawable1 = new LayerDrawable(new Drawable[]{drawableTeardrop, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable1);
                previousShape = getString(R.string.droplet);
                break;
            case 2:
                Drawable drawableCircle = getDrawable(R.drawable.circle_icon);
                drawableCircle.setTint(PublicVariable.primaryColor);
                LayerDrawable layerDrawable2 = new LayerDrawable(new Drawable[]{drawableCircle, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable2);
                previousShape = getString(R.string.circle);
                break;
            case 3:
                Drawable drawableSquare = getDrawable(R.drawable.square_icon);
                drawableSquare.setTint(PublicVariable.primaryColor);
                LayerDrawable layerDrawable3 = new LayerDrawable(new Drawable[]{drawableSquare, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable3);
                previousShape = getString(R.string.square);
                break;
            case 4:
                Drawable drawableSquircle = getDrawable(R.drawable.squircle_icon);
                drawableSquircle.setTint(PublicVariable.primaryColor);
                LayerDrawable layerDrawable4 = new LayerDrawable(new Drawable[]{drawableSquircle, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable4);
                previousShape = getString(R.string.squircle);
                break;
            case 0:
                Drawable drawableNoShape = getDrawable(R.drawable.w_pref);
                shapes.setIcon(drawableNoShape);
                previousShape = getString(R.string.noshape);
                break;
        }

        if (functionsClass.UsageStatsEnabled()) {
            smart.setChecked(true);
        } else {
            smart.setChecked(false);
        }

        if (functionsClass.AccessibilityServiceEnabled() && functionsClass.SettingServiceRunning(InteractionObserver.class)) {
            observe.setChecked(true);
        } else {
            observe.setChecked(false);
        }

        if (functionsClass.NotificationAccess() && functionsClass.SettingServiceRunning(NotificationListener.class)) {
            notification.setChecked(true);
        } else {
            notification.setChecked(false);
        }

        if (functionsClass.freeFormSupport(getApplicationContext()) && functionsClass.FreeForm()) {
            freeForm.setChecked(true);
        } else {
            freeForm.setChecked(false);
        }
    }

    private void setShapes() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        int dialogueWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 377, getResources().getDisplayMetrics());
        int dialogueHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 387, getResources().getDisplayMetrics());

				/*layoutParams.gravity = Gravity.TOP | Gravity.START;
				layoutParams.x = 0;
				layoutParams.y = 0;*/
        layoutParams.width = dialogueWidth;
        layoutParams.height = dialogueHeight;
        layoutParams.windowAnimations = android.R.style.Animation_Dialog;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.57f;

        final Dialog dialog = new Dialog(SettingGUIDark.this);
        dialog.setContentView(R.layout.icons_shapes_preferences);
        dialog.setTitle(Html.fromHtml("<font color='" + PublicVariable.colorLightDarkOpposite + "'>" + getString(R.string.shapedDesc) + "</font>"));
        dialog.getWindow().setAttributes(layoutParams);
        dialog.getWindow().getDecorView().setBackgroundColor(PublicVariable.colorLightDark);
        dialog.setCancelable(true);

        final Drawable drawableTeardrop = getDrawable(R.drawable.droplet_icon);
        drawableTeardrop.setTint(PublicVariable.primaryColor);
        final Drawable drawableCircle = getDrawable(R.drawable.circle_icon);
        drawableCircle.setTint(PublicVariable.primaryColor);
        final Drawable drawableSquare = getDrawable(R.drawable.square_icon);
        drawableSquare.setTint(PublicVariable.primaryColor);
        final Drawable drawableSquircle = getDrawable(R.drawable.squircle_icon);
        drawableSquircle.setTint(PublicVariable.primaryColor);

        RelativeLayout teardropShape = (RelativeLayout) dialog.findViewById(R.id.teardropShape);
        RelativeLayout circleShape = (RelativeLayout) dialog.findViewById(R.id.circleShape);
        RelativeLayout squareShape = (RelativeLayout) dialog.findViewById(R.id.squareShape);
        RelativeLayout squircleShape = (RelativeLayout) dialog.findViewById(R.id.squircleShape);

        Button noShape = (Button) dialog.findViewById(R.id.noShape);

        ImageView teardropImage = (ImageView) dialog.findViewById(R.id.teardropImage);
        ImageView circleImage = (ImageView) dialog.findViewById(R.id.circleImage);
        ImageView squareImage = (ImageView) dialog.findViewById(R.id.squareImage);
        ImageView squircleImage = (ImageView) dialog.findViewById(R.id.squircleImage);

        TextView teardropText = (TextView) dialog.findViewById(R.id.teardropText);
        TextView circleText = (TextView) dialog.findViewById(R.id.circleText);
        TextView squareText = (TextView) dialog.findViewById(R.id.squareText);
        TextView squircleText = (TextView) dialog.findViewById(R.id.squircleText);

        teardropImage.setImageDrawable(drawableTeardrop);
        circleImage.setImageDrawable(drawableCircle);
        squareImage.setImageDrawable(drawableSquare);
        squircleImage.setImageDrawable(drawableSquircle);

        teardropText.setTextColor(PublicVariable.colorLightDarkOpposite);
        circleText.setTextColor(PublicVariable.colorLightDarkOpposite);
        squareText.setTextColor(PublicVariable.colorLightDarkOpposite);
        squircleText.setTextColor(PublicVariable.colorLightDarkOpposite);

        noShape.setTextColor(PublicVariable.colorLightDarkOpposite);

        teardropShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("iconShape", 1);
                editor.apply();
                LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{drawableTeardrop, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable);
                newShape = getString(R.string.droplet);
                dialog.dismiss();
            }
        });
        circleShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("iconShape", 2);
                editor.apply();
                LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{drawableCircle, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable);
                newShape = getString(R.string.circle);
                dialog.dismiss();
            }
        });
        squareShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("iconShape", 3);
                editor.apply();
                LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{drawableSquare, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable);
                newShape = getString(R.string.square);
                dialog.dismiss();
            }
        });
        squircleShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("iconShape", 4);
                editor.apply();
                LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{drawableSquircle, getDrawable(R.drawable.w_pref)});
                shapes.setIcon(layerDrawable);
                newShape = getString(R.string.squircle);
                dialog.dismiss();
            }
        });
        noShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("iconShape", 0);
                editor.apply();

                Drawable drawableNoShape = getDrawable(R.drawable.w_pref);
                shapes.setIcon(drawableNoShape);
                newShape = getString(R.string.noshape);
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!previousShape.equals(newShape)) {
                    PublicVariable.newIcon = true;
                    PublicVariable.forceReload = true;
                    PublicVariable.inMemory = false;

                    if (functionsClass.returnAPI() >= 25) {
                        functionsClass.setAppShortcuts();
                    }
                }
            }
        });
        dialog.show();
    }
}
