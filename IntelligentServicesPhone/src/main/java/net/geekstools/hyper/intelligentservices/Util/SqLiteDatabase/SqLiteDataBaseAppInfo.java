/*
 * Copyright © 2020 By Geeks Empire.
 *
 * Created by Elias Fazel on 4/14/20 9:44 PM
 * Last modified 2/9/20 5:46 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

import java.util.ArrayList;
import java.util.List;

public class SqLiteDataBaseAppInfo extends SQLiteOpenHelper {

    public static final String APP_INFO_TABLE_NAME = "AppInfo";
    public static final String APP_INFO_COLUMN_NUMBER = "Number";
    public static final String APP_INFO_COLUMN_PACKAGE_NAME = "PackageName";
    public static final String APP_INFO_COLUMN_APP_NAME = "AppName";
    public static final String APP_INFO_COLUMN_CLASS_NAME = "ClassName";
    public static String DATABASE_FILE_NAME = "AppInfo.db";
    FunctionsClass functionsClass;

    public SqLiteDataBaseAppInfo(Context context, int sqlVersion) {
        super(context,
                DATABASE_FILE_NAME,
                null,
                sqlVersion
        );
        functionsClass = new FunctionsClass(context);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "CREATE TABLE IF NOT EXISTS "
                        + APP_INFO_TABLE_NAME
                        + " "
                        + "(Number TEXT PRIMARY KEY, PackageName TEXT, ClassName TEXT, AppName TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_FILE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertAppInfo(int APP_INFO_COLUMN_NUMBER,
                                 String APP_INFO_COLUMN_PACKAGE_NAME, String APP_INFO_COLUMN_CLASS_NAME, String APP_INFO_COLUMN_APP_NAME) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Number", APP_INFO_COLUMN_NUMBER);
        contentValues.put("PackageName", APP_INFO_COLUMN_PACKAGE_NAME);
        contentValues.put("ClassName", APP_INFO_COLUMN_CLASS_NAME);
        contentValues.put("AppName", APP_INFO_COLUMN_APP_NAME);
        db.insert(APP_INFO_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean updateAppInfo(String APP_INFO_COLUMN_NUMBER,
                                 String APP_INFO_COLUMN_PACKAGE_NAME, String APP_INFO_COLUMN_CLASS_NAME, String APP_INFO_COLUMN_APP_NAME) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Number", APP_INFO_COLUMN_NUMBER);
        contentValues.put("PackageName", APP_INFO_COLUMN_PACKAGE_NAME);
        contentValues.put("ClassName", APP_INFO_COLUMN_CLASS_NAME);
        contentValues.put("AppName", APP_INFO_COLUMN_APP_NAME);
        db.update(APP_INFO_TABLE_NAME, contentValues, "Number = ? ", new String[]{(APP_INFO_COLUMN_NUMBER)});
        return true;
    }

    public int numberOfRows() {
        String countQuery = "SELECT * FROM " + APP_INFO_TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(countQuery, null);
        int numRows = cursor.getCount();
        cursor.close();
        return numRows;
    }

    public Cursor getAppInfoDataRowFromNumber(String Number) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE Number='" + Number + "'", null);
    }

    public Cursor getAppInfoDataRowFromPackageName(String PackageName) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE PackageName='" + PackageName + "'", null);
    }

    public List<String> getAllAppInfo(String sortParameters, String sortByAscDesc) {
        List<String> array_list = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        //cursor = db.rawQuery("SELECT * FROM AppInfo", null);
        cursor = db.query(APP_INFO_TABLE_NAME,
                null, null, null, null, null,
                (sortParameters) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            array_list.add(cursor.getString(cursor.getColumnIndex(APP_INFO_COLUMN_NUMBER)));
            cursor.moveToNext();
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return array_list;
    }

    public List<String> getAllAppInfoAppName(String sortParameters, String sortByAscDesc) {
        List<String> array_list = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        //cursor = db.rawQuery("SELECT * FROM AppInfo", null);
        cursor = db.query(APP_INFO_TABLE_NAME,
                null, null, null, null, null, (sortParameters) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            array_list.add(cursor.getString(cursor.getColumnIndex(APP_INFO_COLUMN_APP_NAME)));
            cursor.moveToNext();
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return array_list;
    }
}