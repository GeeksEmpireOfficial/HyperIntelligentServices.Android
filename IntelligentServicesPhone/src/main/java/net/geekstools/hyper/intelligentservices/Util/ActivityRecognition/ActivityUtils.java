/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.ActivityRecognition;

import android.content.Context;

import com.google.android.gms.location.DetectedActivity;

import net.geekstools.hyper.intelligentservices.R;

public class ActivityUtils {

    private ActivityUtils() {
    }

    static String getActivityType(Context context, int detectedActivityType) {
        switch (detectedActivityType) {
            case DetectedActivity.IN_VEHICLE:
                return context.getString(R.string.in_vehicle);
            case DetectedActivity.ON_BICYCLE:
                return context.getString(R.string.on_bicycle);
            case DetectedActivity.ON_FOOT:
                return context.getString(R.string.walking);
            case DetectedActivity.WALKING:
                return context.getString(R.string.walking);
            case DetectedActivity.RUNNING:
                return context.getString(R.string.running);
            default:
                return "NONE";
        }
    }
}