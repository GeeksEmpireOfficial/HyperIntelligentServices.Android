/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Notification;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

import java.util.ArrayList;
import java.util.List;

public class SqLiteDataBaseNotification extends SQLiteOpenHelper {

    public static final String NOTIFICATION_TABLE_NAME = "SqLiteDataBaseNotification";
    public static final String NOTIFICATION_COLUMN_TIME_VALUE = "TimeValue";
    public static final String NOTIFICATION_COLUMN_KEY_VALUE = "KeyValue";
    public static final String NOTIFICATION_COLUMN_TITLE_VALUE = "TitleValue";
    public static final String NOTIFICATION_COLUMN_TEXT_VALUE = "TextValue";
    public static final String NOTIFICATION_COLUMN_ICON_VALUE = "IconValue";
    public static String DATABASE_FILE_NAME_PACKAGE = "SqLiteDataBaseNotification.db";
    FunctionsClass functionsClass;

    public SqLiteDataBaseNotification(Context context, String DATABASE_FILE_NAME_PACKAGE, int sqlVersion) {
        super(context,
                DATABASE_FILE_NAME_PACKAGE + ".db",
                null,
                sqlVersion
        );
        SqLiteDataBaseNotification.DATABASE_FILE_NAME_PACKAGE = DATABASE_FILE_NAME_PACKAGE + ".db";
        functionsClass = new FunctionsClass(context);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "CREATE TABLE IF NOT EXISTS "
                        + NOTIFICATION_TABLE_NAME
                        + " "
                        + "(TimeValue TEXT PRIMARY KEY, KeyValue TEXT, TitleValue TEXT, TextValue TEXT, IconValue BLOB)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_FILE_NAME_PACKAGE);
        onCreate(sqLiteDatabase);
    }

    public void insertNotificationInfo(String NOTIFICATION_COLUMN_TIME_VALUE, String NOTIFICATION_COLUMN_KEY_VALUE,
                                       String NOTIFICATION_COLUMN_TITLE_VALUE, String NOTIFICATION_COLUMN_TEXT_VALUE, byte[] NOTIFICATION_COLUMN_ICON_VALUE) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("TimeValue", NOTIFICATION_COLUMN_TIME_VALUE);
        contentValues.put("KeyValue", NOTIFICATION_COLUMN_KEY_VALUE);
        contentValues.put("TitleValue", NOTIFICATION_COLUMN_TITLE_VALUE);
        contentValues.put("TextValue", NOTIFICATION_COLUMN_TEXT_VALUE);
        contentValues.put("IconValue", NOTIFICATION_COLUMN_ICON_VALUE);
        sqLiteDatabase.insert(NOTIFICATION_TABLE_NAME, null, contentValues);
    }

    public int numberOfRows() {
        String countQuery = "SELECT * FROM " + NOTIFICATION_TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(countQuery, null);
        int numRows = cursor.getCount();
        cursor.close();
        return numRows;
    }

    public Cursor getNotificationColumnInfo(final String columnName, final String columnValue) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        NOTIFICATION_TABLE_NAME +
                        " WHERE " + columnName + "='" + columnValue + "'", null);
    }

    public List<String> getAllNotificationInfo(String sortParameters, String sortByAscDesc) {
        List<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        //cursor = db.rawQuery("SELECT * FROM AppInfo", null);
        cursor = db.query(NOTIFICATION_TABLE_NAME,
                null, null, null, null, null,
                (sortParameters) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            arrayList.add(cursor.getString(cursor.getColumnIndex(NOTIFICATION_COLUMN_TIME_VALUE)));
            cursor.moveToNext();
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return arrayList;
    }
}
