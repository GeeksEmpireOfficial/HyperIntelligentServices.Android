/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;

import net.geekstools.hyper.intelligentservices.Configurations;
import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

public class PermissionsCheckPoint extends Activity {

    FunctionsClass functionsClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_point);

        functionsClass = new FunctionsClass(getApplicationContext(), PermissionsCheckPoint.this);
        functionsClass.setThemeColor(findViewById(R.id.entryPoint));
        functionsClass.Toast(getString(R.string.wait), Gravity.BOTTOM);

        WebView webView = (WebView) findViewById(R.id.configurationWaiting);
        webView.setVisibility(View.VISIBLE);
        webView.getSettings();
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.loadUrl("file:///android_asset/gifLoader.html");

        functionsClass.RuntimePermissions(PermissionsCheckPoint.this, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 777: {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    functionsClass.savePreference(".Configurations", "Permissions", true);
                    functionsClass.RuntimePermissions(PermissionsCheckPoint.this, false).dismiss();
                    functionsClass.extractWallpaperColor();
                    startActivity(new Intent(getApplicationContext(), Configurations.class));
                } else {
                    functionsClass.savePreference(".Configurations", "Permissions", false);
                    PermissionsCheckPoint.this.finish();
                }
                break;
            }
        }
    }

}
