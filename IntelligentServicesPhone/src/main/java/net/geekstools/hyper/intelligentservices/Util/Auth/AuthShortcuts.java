/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Auth;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase.SqLiteDataBaseIntelligentServices;

import java.util.Calendar;

public class AuthShortcuts extends Activity {

    FunctionsClass functionsClass;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        functionsClass = new FunctionsClass(getApplicationContext(), AuthShortcuts.this);

        Intent intent = getIntent();
        if (intent.hasExtra("PackageName") && intent.hasExtra("ClassName") && intent.hasExtra("OrderNumber")) {
            String packageName = intent.getStringExtra("PackageName");
            String className = intent.getStringExtra("ClassName");
            int orderNumber = intent.getIntExtra("OrderNumber", 0);

            if (functionsClass.isAppLocked(packageName)) {
                FunctionsClass.AuthOpenAppValues.authSqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(getApplicationContext(), functionsClass.sqLiteIntelligentServiceFileName(), 1);

                FunctionsClass.AuthOpenAppValues.authPackageName = packageName;
                FunctionsClass.AuthOpenAppValues.authClassName = className;
                FunctionsClass.AuthOpenAppValues.authOrderNumber = orderNumber;
                FunctionsClass.AuthOpenAppValues.authPositionX = ((functionsClass.displayX() / 2) - functionsClass.DpToInteger(44));
                FunctionsClass.AuthOpenAppValues.authPositionY = ((functionsClass.displayY() / 2) - functionsClass.DpToInteger(44));
                FunctionsClass.AuthOpenAppValues.authHW = functionsClass.DpToInteger(88);

                FunctionsClass.AuthOpenAppValues.authUnlockIt = false;
                FunctionsClass.AuthOpenAppValues.authPopupOption = false;

                functionsClass.openAuthInvocation();
            } else {
                functionsClass.appsLaunchPad(packageName, className,
                        ((functionsClass.displayX() / 2) - functionsClass.DpToInteger(44)),
                        ((functionsClass.displayY() / 2) - functionsClass.DpToInteger(44)),
                        functionsClass.DpToInteger(88),
                        functionsClass.FreeForm());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!functionsClass.databaseAvailableIntelligentService()) {
                            SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(getApplicationContext(), functionsClass.sqLiteIntelligentServiceFileName(), 1);
                            sqLiteDataBaseIntelligentServices.insertAppInfo(
                                    String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                    packageName,
                                    className,
                                    1,
                                    0
                            );
                            sqLiteDataBaseIntelligentServices.close();
                        } else {
                            SqLiteDataBaseIntelligentServices sqLiteDataBaseIntelligentServices = new SqLiteDataBaseIntelligentServices(getApplicationContext(), functionsClass.sqLiteIntelligentServiceFileName(), 1);
                            try {
                                int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                                int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
                                int timeSecond = Calendar.getInstance().get(Calendar.SECOND);
                                int timeMilliSecond = Calendar.getInstance().get(Calendar.MILLISECOND);
                                String lastUsed = String.valueOf(timeHours) + String.valueOf(timeMinutes) + String.valueOf(timeSecond) + String.valueOf(timeMilliSecond);

                                Cursor cursor = sqLiteDataBaseIntelligentServices.getAppInfoDataFromOrderNumber(orderNumber);
                                cursor.moveToFirst();
                                int previousCounter = cursor.getInt(cursor.getColumnIndexOrThrow(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER));
                                sqLiteDataBaseIntelligentServices.updateAppInfoFromNumber(
                                        String.valueOf(orderNumber),
                                        previousCounter + 1,
                                        Integer.parseInt(lastUsed)
                                );
                                if (!cursor.isClosed()) {
                                    cursor.close();
                                }
                            } catch (Exception cursorIndexOutOfBoundsException) {
                                cursorIndexOutOfBoundsException.printStackTrace();
                                sqLiteDataBaseIntelligentServices.insertAppInfo(
                                        String.valueOf(sqLiteDataBaseIntelligentServices.numberOfRows() + 1),
                                        packageName,
                                        className,
                                        1,
                                        0
                                );
                            }
                        }
                    }
                }, 333);
            }
        }

        finish();
    }
}
