/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.UI.Splash.FloatingSplash;

public class LaunchPad extends Activity {

    FunctionsClass functionsClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        functionsClass = new FunctionsClass(getApplicationContext(), LaunchPad.this);
        String packageName = getIntent().getStringExtra("packageName");
        String className = getIntent().getStringExtra("className");
        if (functionsClass.SplashRevealEnabled()) {
            if (functionsClass.FloatingEnabled()) {
                Intent splashReveal = new Intent(getApplicationContext(), FloatingSplash.class);
                splashReveal.putExtra("packageName", packageName);
                splashReveal.putExtra("className", className);
                splashReveal.putExtra("positionX", getIntent().getIntExtra("positionX", (functionsClass.displayX() / 2)));
                splashReveal.putExtra("positionY", getIntent().getIntExtra("positionY", (functionsClass.displayY() / 2)));
                splashReveal.putExtra("HW", getIntent().getIntExtra("HW", 0));
                splashReveal.putExtra("floatIt", getIntent().getBooleanExtra("floatIt", false));
                startService(splashReveal);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    startActivity(
                            new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()))
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                }
            }
        } else {
            //Check FloatIt
            if (getIntent().hasExtra("floatIt")) {
                if (getIntent().getBooleanExtra("floatIt", false)) {
                    functionsClass.openApplicationFreeForm(packageName, className);
                } else {
                    functionsClass.openApplicationFromActivity(packageName, className);
                }
            }
        }
        finish();
    }
}
