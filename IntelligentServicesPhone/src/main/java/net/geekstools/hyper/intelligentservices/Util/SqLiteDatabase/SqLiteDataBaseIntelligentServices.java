/*
 * Copyright © 2020 By Geeks Empire.
 *
 * Created by Elias Fazel on 2/7/20 9:46 AM
 * Last modified 2/7/20 9:40 AM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.SqLiteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SqLiteDataBaseIntelligentServices extends SQLiteOpenHelper {

    public static final String APP_INFO_TABLE_NAME = "SqLiteDataBaseIntelligentServices";
    public static final String APP_INFO_COLUMN_NUMBER = "Number";
    public static final String APP_INFO_COLUMN_PACKAGES = "PackageName";
    public static final String APP_INFO_COLUMN_CLASS_NAME = "ClassName";
    public static final String APP_INFO_COLUMN_GENERAL_COUNTER = "GeneralCounter";
    public static final String APP_INFO_COLUMN_LAST_USED = "LastUsed";
    public static final String APP_INFO_COLUMN_MINUS = "Minus";
    public static String DATABASE_FILE_NAME = "SqLiteDataBaseIntelligentServices.db";
    Context context;
    FunctionsClass functionsClass;

    public SqLiteDataBaseIntelligentServices(Context context, String DATABASE_FILE_NAME, int sqlVersion) {
        super(context,
                context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
                        + File.separator +
                        ".IntelligentServicesSqLiteDataBase"
                        + File.separator +
                        DATABASE_FILE_NAME + ".db",
                null,
                sqlVersion);
        this.context = context;
        SqLiteDataBaseIntelligentServices.DATABASE_FILE_NAME = DATABASE_FILE_NAME + ".db";
        functionsClass = new FunctionsClass(context);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "CREATE TABLE IF NOT EXISTS "
                        + APP_INFO_TABLE_NAME
                        + " "
                        + "(Number TEXT PRIMARY KEY, PackageName TEXT, ClassName TEXT, GeneralCounter INTEGER, LastUsed INTEGER, Minus INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_FILE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void insertAppInfo(String APP_INFO_COLUMN_NUMBER,
                              String APP_INFO_COLUMN_PACKAGES, String APP_INFO_COLUMN_CLASS_NAME,
                              int APP_INFO_COLUMN_GENERAL_COUNTER, int APP_INFO_COLUMN_LAST_USED) {
        if (!APP_INFO_COLUMN_PACKAGES.equals(context.getPackageName())
                && !functionsClass.ifDefaultLauncher(APP_INFO_COLUMN_PACKAGES) && !functionsClass.ifSystem(APP_INFO_COLUMN_PACKAGES) && functionsClass.canLaunch(APP_INFO_COLUMN_PACKAGES)) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("Number", APP_INFO_COLUMN_NUMBER);
            contentValues.put("PackageName", APP_INFO_COLUMN_PACKAGES);
            contentValues.put("ClassName", APP_INFO_COLUMN_CLASS_NAME);
            contentValues.put("GeneralCounter", APP_INFO_COLUMN_GENERAL_COUNTER);
            contentValues.put("LastUsed", APP_INFO_COLUMN_LAST_USED);
            db.insertWithOnConflict(APP_INFO_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        }
    }

    public void updateAppInfoFromLastUsed(String APP_INFO_COLUMN_NUMBER, String APP_INFO_COLUMN_LAST_USED, int APP_INFO_COLUMN_MINUS) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_MINUS, APP_INFO_COLUMN_MINUS);
        sqLiteDatabase.update(APP_INFO_TABLE_NAME, contentValues, "Number = ? AND LastUsed = ? ", new String[]{APP_INFO_COLUMN_NUMBER, APP_INFO_COLUMN_LAST_USED});
    }

    public void updateAppInfoFromNumber(String APP_INFO_COLUMN_NUMBER,
                                        int APP_INFO_COLUMN_GENERAL_COUNTER, int APP_INFO_COLUMN_LAST_USED) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, APP_INFO_COLUMN_GENERAL_COUNTER);
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_LAST_USED, APP_INFO_COLUMN_LAST_USED);
        sqLiteDatabase.update(APP_INFO_TABLE_NAME, contentValues, "Number = ? ", new String[]{(APP_INFO_COLUMN_NUMBER)});
    }

    public boolean updateAppInfoFromClassName(String APP_INFO_COLUMN_CLASS_NAME,
                                              int APP_INFO_COLUMN_MINUS) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_MINUS, APP_INFO_COLUMN_MINUS);
        sqLiteDatabase.update(APP_INFO_TABLE_NAME, contentValues, "ClassName = ? ", new String[]{(APP_INFO_COLUMN_CLASS_NAME)});
        return true;
    }

    public boolean updateAppInfoFromPackageName(String APP_INFO_COLUMN_PACKAGES,
                                                int APP_INFO_COLUMN_GENERAL_COUNTER) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, APP_INFO_COLUMN_GENERAL_COUNTER);
        sqLiteDatabase.update(APP_INFO_TABLE_NAME, contentValues, "PackageName = ? ", new String[]{(APP_INFO_COLUMN_PACKAGES)});
        return true;
    }

    public boolean updateAppInfoFromPackageName(String APP_INFO_COLUMN_PACKAGES,
                                                int APP_INFO_COLUMN_GENERAL_COUNTER, int APP_INFO_COLUMN_LAST_USED) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, APP_INFO_COLUMN_GENERAL_COUNTER);
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_LAST_USED, APP_INFO_COLUMN_LAST_USED);
        sqLiteDatabase.update(APP_INFO_TABLE_NAME, contentValues, "PackageName = ? ", new String[]{(APP_INFO_COLUMN_PACKAGES)});
        return true;
    }

    public void updateAppInfoAccessibility(String APP_INFO_COLUMN_CLASS_NAME,
                                           int APP_INFO_COLUMN_GENERAL_COUNTER, int APP_INFO_COLUMN_LAST_USED) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_GENERAL_COUNTER, APP_INFO_COLUMN_GENERAL_COUNTER);
        contentValues.put(SqLiteDataBaseIntelligentServices.APP_INFO_COLUMN_LAST_USED, APP_INFO_COLUMN_LAST_USED);
        sqLiteDatabase.update(APP_INFO_TABLE_NAME, contentValues, "ClassName = ? ", new String[]{(APP_INFO_COLUMN_CLASS_NAME)});
    }

    public Integer deleteAppInfoData(String packages) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        return sqLiteDatabase.delete(
                APP_INFO_TABLE_NAME,
                APP_INFO_COLUMN_PACKAGES + " = ? ",
                new String[]{(packages)});
    }

    public int numberOfRows() {
        String countQuery = "SELECT * FROM " + APP_INFO_TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(countQuery, null);
        int numRows = cursor.getCount();
        cursor.close();
        return numRows;
    }

    public Cursor getAppInfoDataFromOrderNumber(int Number) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE Number=" + Number + "", null);
    }

    public Cursor getAppInfoDataFromPackageName(String packageName) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE PackageName='" + packageName + "'", null);
    }

    public Cursor getAppInfoDataFromClassName(String className) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE ClassName='" + className + "'", null);
    }

    public Cursor getMinusData(int Minus) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE Minus=" + Minus + "", null);
    }

    public Cursor getAppInfoDataFromPackageNameClassName(String packageName, String className) {
        return this.getReadableDatabase()
                .rawQuery("SELECT * FROM " +
                        APP_INFO_TABLE_NAME +
                        " WHERE PackageName='" + packageName + "'" + " AND " + " ClassName='" + className + "'", null);
    }

    public List<Integer> getAllAppInfoOfOrderNumber(String sortParameters, String sortByAscDesc, int maxValue) {
        List<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        cursor = db.query(APP_INFO_TABLE_NAME,
                null, null, null, null, null,
                (sortParameters) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        int loopCounter = 0;
        while (!cursor.isAfterLast()) {
            if (loopCounter == maxValue) {
                break;
            }
            array_list.add(cursor.getInt(cursor.getColumnIndex(APP_INFO_COLUMN_NUMBER)));
            cursor.moveToNext();
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return array_list;
    }

    public List<String> getAllAppInfo(String sortParameters, String sortByAscDesc, int maxValue) {
        List<String> array_list = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        //cursor = db.rawQuery("SELECT * FROM AppInfo", null);
        cursor = db.query(APP_INFO_TABLE_NAME,
                null, null, null, null, null,
                (sortParameters) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        int loopCounter = 0;
        while (!cursor.isAfterLast()) {
            if (loopCounter == maxValue) {
                break;
            }
            array_list.add(cursor.getString(cursor.getColumnIndex(APP_INFO_COLUMN_CLASS_NAME)));
            cursor.moveToNext();
            loopCounter++;
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return array_list;
    }

    public List<Integer> getAllAppCounter(String sortByAscDesc) {
        List<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        cursor = db.query(APP_INFO_TABLE_NAME,
                null, null, null, null, null,
                (APP_INFO_COLUMN_GENERAL_COUNTER) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            array_list.add(cursor.getInt(cursor.getColumnIndex(APP_INFO_COLUMN_GENERAL_COUNTER)));
            cursor.moveToNext();
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return array_list;
    }

    public List<Integer> getAllAppLastUsed(String sortByAscDesc, int maxValue) {
        List<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        cursor = db.query(APP_INFO_TABLE_NAME,
                null, null, null, null, null,
                (APP_INFO_COLUMN_GENERAL_COUNTER) + " " + sortByAscDesc);
        //ASC
        //DESC
        cursor.moveToFirst();
        int loopCounter = 0;
        while (!cursor.isAfterLast()) {
            if (loopCounter == maxValue) {
                break;
            }
            array_list.add(cursor.getInt(cursor.getColumnIndex(APP_INFO_COLUMN_LAST_USED)));
            cursor.moveToNext();
            loopCounter++;
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return array_list;
    }
}