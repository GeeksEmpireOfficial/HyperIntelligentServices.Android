/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.Notification;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import net.geekstools.hyper.intelligentservices.BuildConfig;
import net.geekstools.hyper.intelligentservices.R;
import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;
import net.geekstools.hyper.intelligentservices.Util.Functions.PublicVariable;

public class NotificationListener extends NotificationListenerService {

    FunctionsClass functionsClass;

    String notificationTitle, notificationText, notificationPackage, notificationTime, notificationId;
    Drawable notificationIcon;

    @Override
    public void onCreate() {
        super.onCreate();
        functionsClass = new FunctionsClass(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        return START_STICKY;
    }

    @Override
    public void onNotificationPosted(final StatusBarNotification statusBarNotification) {
        if (statusBarNotification.getPackageName() != null && statusBarNotification.isClearable()) {
            if (PublicVariable.previousDuplicated == null) {
                PublicVariable.previousDuplicated = statusBarNotification.getPackageName();
            } else {
                if (PublicVariable.previousDuplicated.equals(notificationPackage)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            PublicVariable.previousDuplicated = null;
                        }
                    }, 500);
                    return;
                }
            }
            try {
                notificationPackage = statusBarNotification.getPackageName();
                notificationId = statusBarNotification.getKey();
                notificationTime = String.valueOf(statusBarNotification.getPostTime());
                Bundle extras = statusBarNotification.getNotification().extras;
                if (!extras.isEmpty()) {
                    try {
                        notificationTitle = extras.getString(Notification.EXTRA_TITLE);
                    } catch (Exception e) {
                        notificationTitle = functionsClass.appName(notificationPackage);
                    }
                    /*LOAD TEXT CONTENT*/
                    try {
                        notificationText = extras.getString(Notification.EXTRA_TEXT);
                    } catch (Exception e) {
                        notificationText = extras.getString(Notification.EXTRA_BIG_TEXT);
                    }
                    /*LOAD ICON*/
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        try {
                            notificationIcon = statusBarNotification.getNotification().getLargeIcon().loadDrawable(getApplicationContext());
                        } catch (Exception e) {
                            try {
                                notificationIcon = statusBarNotification.getNotification().getSmallIcon().loadDrawable(getApplicationContext());
                            } catch (Exception e1) {
                                notificationIcon = functionsClass.appIcon(notificationPackage);
                            }
                        }
                    } else {
                        try {
                            notificationIcon = functionsClass.bitmapToDrawable(statusBarNotification.getNotification().largeIcon);
                        } catch (Exception e) {
                            notificationIcon = functionsClass.appIcon(notificationPackage);
                        }
                    }
                }

                if (BuildConfig.DEBUG) {
                    System.out.println("::: Package ::: " + notificationPackage);
                    System.out.println("::: Key ::: " + notificationId);
                    System.out.println("::: Title ::: " + notificationTitle);
                    System.out.println("::: Text ::: " + notificationText);
                    System.out.println("::: Time ::: " + notificationTime);
                }

                if (getPackageManager().getLaunchIntentForPackage(notificationPackage) != null) {
                    functionsClass.saveFileAppendLine(".notificationPackages", notificationPackage);
                    SqLiteDataBaseNotification sqLiteDataBaseNotification = new SqLiteDataBaseNotification(getApplicationContext(), notificationPackage, 1);
                    sqLiteDataBaseNotification.insertNotificationInfo(notificationTime, notificationId, notificationTitle, notificationText, functionsClass.drawableToByte(notificationIcon));
                    sqLiteDataBaseNotification.close();
                }
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(getString(R.string.removeNotificationKey));
                BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        try {
                            NotificationListener.this.cancelAllNotifications();
                            if (intent.getAction().equals(getString(R.string.removeNotificationKey))) {
                                NotificationListener.this.cancelNotification(intent.getStringExtra("notification_key"));

                                try {
                                    String notificationPackage = intent.getStringExtra("notification_package");
                                    if (getPackageManager().getLaunchIntentForPackage(notificationPackage) != null) {
                                        functionsClass.removeLine(".notificationPackages", notificationPackage);
                                        functionsClass.deleteDatabase(notificationPackage);
                                        sendBroadcast(new Intent(getString(R.string.notificationDotNo)).putExtra("NotificationPackage", notificationPackage));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                try {
                    registerReceiver(broadcastReceiver, intentFilter);
                } catch (AssertionError assertionError) {
                    assertionError.printStackTrace();
                }
                sendBroadcast(new Intent(getString(R.string.notificationDot)).putExtra("NotificationPackage", notificationPackage));
                sendBroadcast(new Intent(getString(R.string.newNotificationData)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public StatusBarNotification[] getActiveNotifications() {
        return super.getActiveNotifications();
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification statusBarNotification) {
        if (statusBarNotification.getPackageName() != null && statusBarNotification.isClearable()) {
            try {
                String notificationPackage = statusBarNotification.getPackageName();
                if (getPackageManager().getLaunchIntentForPackage(notificationPackage) != null) {
                    functionsClass.removeLine(".notificationPackages", notificationPackage);
                    functionsClass.deleteDatabase(notificationPackage);
                    sendBroadcast(new Intent(getString(R.string.notificationDotNo)).putExtra("NotificationPackage", notificationPackage));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
