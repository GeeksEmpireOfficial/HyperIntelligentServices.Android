/*
 * Copyright © 2019 By Geeks Empire.
 *
 * Created by Elias Fazel on 11/13/19 12:54 PM
 * Last modified 11/13/19 12:50 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

package net.geekstools.hyper.intelligentservices.Util.ActivityRecognition;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import net.geekstools.hyper.intelligentservices.Util.Functions.FunctionsClass;

import java.util.ArrayList;
import java.util.Calendar;

public class ActivityRecognitionService extends IntentService {

    protected static final String TAG = "ActivityRecognitionService";
    FunctionsClass functionsClass;

    public ActivityRecognitionService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        functionsClass = new FunctionsClass(getApplicationContext());
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            ActivityRecognitionResult activityRecognitionResult = ActivityRecognitionResult.extractResult(intent);
            ArrayList<DetectedActivity> detectedActivities = (ArrayList) activityRecognitionResult.getProbableActivities();

            int dateMonth = Calendar.getInstance().get(Calendar.MONTH);
            int dateDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            String Date = String.valueOf(dateMonth + 1) + String.valueOf(dateDay);

            int timeHours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            int timeMinutes = Calendar.getInstance().get(Calendar.MINUTE);
            String Time = String.valueOf(timeHours) + String.valueOf(timeMinutes);

            functionsClass.savePreference("ActivityRecognition", "Type", ActivityUtils.getActivityType(getApplicationContext(), detectedActivities.get(0).getType()));
            functionsClass.savePreference("ActivityRecognition", "Date", Date);
            functionsClass.savePreference("ActivityRecognition", "Time", Time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
