/*
 * Copyright © 2020 By Geeks Empire.
 *
 * Created by Elias Fazel on 4/14/20 9:44 PM
 * Last modified 2/10/20 7:53 PM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

/**
 * Automatically generated file. DO NOT MODIFY
 */
package net.geekstools.hyper.intelligentservices;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "net.geekstools.hyper.intelligentservices";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "betaConfig";
  public static final int VERSION_CODE = 46;
  public static final String VERSION_NAME = "1.02.10.2020.46[BETA]";
}
