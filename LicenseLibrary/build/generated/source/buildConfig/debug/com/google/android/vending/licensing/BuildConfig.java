/*
 * Copyright © 2020 By Geeks Empire.
 *
 * Created by Elias Fazel on 4/14/20 9:44 PM
 * Last modified 2/7/20 9:50 AM
 *
 * Licensed Under MIT License.
 * https://opensource.org/licenses/MIT
 */

/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.google.android.vending.licensing;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.google.android.vending.licensing";
  /**
   * @deprecated APPLICATION_ID is misleading in libraries. For the library package name use LIBRARY_PACKAGE_NAME
   */
  @Deprecated
  public static final String APPLICATION_ID = "com.google.android.vending.licensing";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.000.000.1_PRO";
}
